# marketplace-client-java release notes

## 2.0.1

This patch release fixes a slf4j dependency [issue](https://ecosystem.atlassian.net/browse/MPJC-2) that only affects developers who use the all-in-one (dependencies) build of the library. The all-in-one build now bundles slf4j 1.7.21 rather than 1.5.0.

## 2.0.0

This major version release uses the 2.0 version of the Marketplace REST API. Model objects have been updated to reflect the new API schema, and many types and methods have been renamed. Changes include:

* The library now uses the [Fugue](https://bitbucket.org/atlassian/fugue) classes (`com.atlassian.fugue`) rather than `com.atlassian.upm.api.util` for generic types such as `Option` and `Either`.
* All references to "plugins" (the `Plugins` API, `Plugin` model class, etc.) have been changed to "add-ons" (`Addon`, `AddonVersion`, etc.) since not all Marketplace listings are plugins.
* `Addon` and `AddonVersion` now include fields that could not be read or set in the 1.0 API even though they have been supported on the Marketplace site for some time, such as highlight images.
* `MarketplaceType` has been renamed to `PaymentModel`, to match the terminology used on the Marketplace site.
* All queries that support a "hosting type" filter now use a `HostingType` enum that can be `CLOUD` (formerly `onDemand`), `SERVER` (formerly `onPremise`), or `DATA_CENTER`.
* Methods that previously always returned a `Plugin` with an embedded `PluginVersion` now only return the `Addon` by default; the version-level data will be included if you specifically request it in the `AddonQuery` parameters, or you can do a separate query for just the version.
* It is no longer possible to get prices within an add-on query; instead, there is a separate pricing query.
* For queries that can produce multiple pages of results, there is now a single generic `getMore` method in `MarketplaceClient` for reading next or previous pages; this replaces methods like `getMorePlugins`.
* All queries that support `offset` and `limit` parameters now encapsulate these in the `QueryBounds` class.
* `MarketplaceClient` instances can now be created through the factory methods in `MarketplaceClientFactory`, rather than calling the `DefaultMarketplaceClient` constructor directly.
* The `HttpHelper` interface has been renamed `HttpTransport`, and you can now access the `HttpTransport` instance for any instance of `MarketplaceClient` if you want to perform simple HTTP requests against the Marketplace server for some operation that isn't yet supported by the client.

## 1.0.1

Internal changes for UPM support; there is no need for developers to upgrade to this version.

## 1.0.0

This release is identical (except for the Maven artifact name) to the `com.atlassian.upm:mpac-client` artifact in v2.20.6 of UPM.  It is provided only to make it easy to transition any code that was using `mpac-client` to use the new artifact with no code changes.
