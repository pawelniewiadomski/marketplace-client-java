package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.encoding.SchemaViolation;
import com.atlassian.utt.matchers.NamedFunction;
import com.atlassian.utt.reflect.ReflectionFunctions;

import org.hamcrest.Matcher;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.isA;

abstract class BaseJsonTests
{
    protected JsonEntityEncoding encoding = new JsonEntityEncoding();
    
    protected <T> T decode(String json, Class<T> type) throws Exception
    {
        return encoding.decode(new ByteArrayInputStream(json.getBytes()), type);
    }
    
    protected <T> String encode(T t) throws Exception
    {
        return encode(t, false);
    }

    protected <T> String encode(T t, boolean includeReadOnly) throws Exception
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        encoding.encode(os, t, includeReadOnly);
        return new String(os.toByteArray());
    }
    
    @SuppressWarnings("unchecked")
    protected void causeShouldBe(MpacException.InvalidResponseError e, Class<? extends SchemaViolation> cause,
        Option<String> message)
    {
        Matcher<SchemaViolation> classCond = isA((Class<SchemaViolation>)cause);
        Matcher<SchemaViolation> cond = classCond;
        for (String s: message)
        {
            cond = allOf(classCond, svMessage.is(s));
        }
        assertThat(e.getSchemaViolations(), contains(cond));
    }

    private static NamedFunction<SchemaViolation, String> svMessage =
        ReflectionFunctions.accessor(SchemaViolation.class, String.class, "message");
}
