package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.api.VendorQuery;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.TestModelBuilders;
import com.atlassian.marketplace.client.model.Vendor;
import com.atlassian.marketplace.client.model.VendorSummary;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_VENDORS_PATH;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.TestModelBuilders.vendor;
import static com.atlassian.marketplace.client.model.TestModelBuilders.vendorSummary;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class VendorsImplTest extends ApiImplTestBaseWithCreateAndUpdate<Vendor>
{
    private static final URI FAKE_VENDOR_URI = URI.create("/vendor");
    private static final VendorId FAKE_VENDOR_ID = VendorId.fromUri(FAKE_VENDOR_URI);
    private static final Links VENDOR_LINKS = links().put("self", FAKE_VENDOR_URI).build();
    private static final Vendor VENDOR_REP = TestModelBuilders.vendor().links(VENDOR_LINKS).build();
    private static final VendorSummary VENDOR_SUMMARY_REP = vendorSummary().build();
    private static final InternalModel.Vendors VENDORS_REP =
            InternalModel.vendors(links().build(), ImmutableList.of(VENDOR_SUMMARY_REP), 2);
    
    @Test
    public void getByIdUsesUriDirectly() throws Exception
    {
        tester.mockResource(tester.apiBase.resolve(FAKE_VENDOR_URI), VENDOR_REP);
        assertThat(tester.client.vendors().getById(FAKE_VENDOR_ID), contains(VENDOR_REP));
    }

    @Test
    public void findUsesAddonsResource() throws Exception
    {
        tester.mockResource(vendorsApi().build(), VENDORS_REP);
        
        assertThat(tester.client.vendors().find(VendorQuery.any()), contains(VENDOR_SUMMARY_REP));
    }

    @Test
    public void findReturnsPageWithCorrectSize() throws Exception
    {
        tester.mockResource(vendorsApi().build(), VENDORS_REP);
        
        assertThat(tester.client.vendors().find(VendorQuery.any()).size(), equalTo(1));
    }

    @Test
    public void findReturnsPageWithCorrectTotalSize() throws Exception
    {
        tester.mockResource(vendorsApi().build(), VENDORS_REP);
        
        assertThat(tester.client.vendors().find(VendorQuery.any()).totalSize(), equalTo(2));
    }

    private void testFindParams(UriBuilder uriWithParams, VendorQuery.Builder queryWithParams) throws Exception
    {
        tester.mockResource(uriWithParams.build(), VENDORS_REP);
        assertThat(tester.client.vendors().find(queryWithParams.build()), contains(VENDOR_SUMMARY_REP));
    }

    @Test
    public void findPassesOffsetInQueryString() throws Exception
    {
        testFindParams(vendorsApi().queryParam("offset", "5"), VendorQuery.builder().bounds(QueryBounds.offset(5)));
    }
    
    @Test
    public void findPassesLimitInQueryString() throws Exception
    {
        testFindParams(vendorsApi().queryParam("limit", "10"), VendorQuery.builder().bounds(QueryBounds.limit(some(10))));
    }

    @Test
    public void findPassesForThisUserInQueryString() throws Exception
    {
        testFindParams(vendorsApi().queryParam("forThisUser", "true"), VendorQuery.builder().forThisUserOnly(true));
    }

    @Override
    protected URI getCollectionUri()
    {
        return vendorsApi().build();
    }
    
    @Override
    protected Vendor makeEntityToCreate()
    {
        return vendor().build();
    }
    
    @Override
    protected Vendor makeEntityFromServer()
    {
        return vendor().links(VENDOR_LINKS).build();
    }
    
    @Override
    protected Vendor makeEntityWithUpdates(Vendor from)
    {
        return vendor(from).build();
    }
    
    @Override
    protected Vendor createEntity(Vendor entity) throws MpacException
    {
        return tester.client.vendors().createVendor(entity);
    }
    
    @Override
    protected Vendor updateEntity(Vendor original, Vendor updated) throws MpacException
    {
        return tester.client.vendors().updateVendor(original, updated);
    }
    
    private UriBuilder vendorsApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_VENDORS_PATH);
    }
}
