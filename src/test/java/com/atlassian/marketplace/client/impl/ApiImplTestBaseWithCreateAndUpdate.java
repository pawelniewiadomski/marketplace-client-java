package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Entity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.TestObjects.BASE_URI;
import static com.atlassian.marketplace.client.impl.ClientTester.defaultRootResource;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public abstract class ApiImplTestBaseWithCreateAndUpdate<T extends Entity> extends ApiImplTestBase
{
    private static final String FAKE_PATCH = "[patching]";
    
    protected ClientTester tester;
    
    @Before
    public void setUp() throws Exception
    {
        tester = new ClientTester(BASE_URI);
        tester.mockResource(tester.apiBase, defaultRootResource());
    }

    protected abstract URI getCollectionUri();
    protected abstract T makeEntityToCreate();
    protected abstract T makeEntityWithUpdates(T from);
    protected abstract T makeEntityFromServer();
    protected abstract T createEntity(T entity) throws Exception;
    protected abstract T updateEntity(T original, T updated) throws Exception;
    
    protected URI transformEntityUriAfterCreate(URI uri)
    {
        return uri;
    }
    
    @Test
    public void createPostsToCollectionResource() throws Exception
    {
        T entity = makeEntityToCreate();
        tester.mockPostResource(getCollectionUri(), 201,
            ImmutableMap.<String, Iterable<String>>of("Location", ImmutableList.of("/newuri")));
        tester.mockResource(transformEntityUriAfterCreate(tester.apiBase.resolve("/newuri")), entity);
        
        createEntity(entity);
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        tester.encoding.encode(bos, entity, false);
        byte[] data = bos.toByteArray();
        
        verify(tester.httpTransport).post(any(URI.class),
                any(InputStream.class),
                eq((long) data.length),
                eq("application/json"),
                eq("application/json"));
    }

    @Test
    public void createReturnsNewEntity() throws Exception
    {
        T result = makeEntityFromServer();
        
        tester.mockPostResource(getCollectionUri(), 201,
            ImmutableMap.<String, Iterable<String>>of("Location", ImmutableList.of("/newuri")));
        tester.mockResource(transformEntityUriAfterCreate(tester.apiBase.resolve("/newuri")), result);
        
        assertThat(createEntity(makeEntityToCreate()), sameInstance(result));
    }

    @Test
    public void createThrowsExceptionIfServerReturnsError() throws Exception
    {
        tester.mockPostResource(getCollectionUri(), 400);
        
        try
        {
            createEntity(makeEntityToCreate());
            fail("expected error");
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e.getStatus(), equalTo(400));
        }
    }
    
    @Test
    public void updateSendsPatchRequest() throws Exception
    {
        T original = makeEntityFromServer();
        T updated = makeEntityWithUpdates(original);
        URI uri = tester.apiBase.resolve(original.getSelfUri());
        URI newUri = URI.create("/newuri");
        tester.mockPatchResource(uri, 204, some(newUri.toASCIIString()));
        tester.mockResource(tester.apiBase.resolve(newUri), updated);
        tester.mockPatchDocument(original, updated, FAKE_PATCH);
        
        updateEntity(original, updated);
        
        verify(tester.httpTransport).patch(uri, FAKE_PATCH.getBytes());
    }

    @Test
    public void updateReturnsNewEntity() throws Exception
    {
        T original = makeEntityFromServer();
        T updated = makeEntityWithUpdates(original);
        T result = makeEntityFromServer();
        URI uri = tester.apiBase.resolve(original.getSelfUri());
        URI newUri = URI.create("/newuri");
        tester.mockPatchResource(uri, 204, some(newUri.toASCIIString()));
        tester.mockResource(tester.apiBase.resolve(newUri), result);
        
        assertThat(updateEntity(original, updated), sameInstance(result));
    }

    @Test
    public void updateQueriesNewEntityWithNoCaching() throws Exception
    {
        T original = makeEntityFromServer();
        T updated = makeEntityWithUpdates(original);
        T result = makeEntityFromServer();
        URI uri = tester.apiBase.resolve(original.getSelfUri());
        URI newUri = URI.create("/newuri");
        tester.mockPatchResource(uri, 204, some(newUri.toASCIIString()));
        tester.mockResource(tester.apiBase.resolve(newUri), result);
        
        updateEntity(original, updated);
        
        assertThat(tester.getReceivedExtraHeaders(tester.apiBase.resolve(newUri), "GET"),
            equalTo(ImmutableMap.of("Cache-Control", "no-cache")));
    }

    @Test
    public void updateThrowsExceptionIfServerReturnsError() throws Exception
    {
        T original = makeEntityFromServer();
        T updated = makeEntityWithUpdates(original);
        URI uri = tester.apiBase.resolve(original.getSelfUri());
        tester.mockPatchResource(uri, 400, none(String.class)); 
        
        try
        {
            updateEntity(original, updated);
            fail("expected error");
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e.getStatus(), equalTo(400));
        }
    }

    @Test
    public void updateThrowsExceptionIfFinalQueryOfUpdatedEntityFails() throws Exception
    {
        T original = makeEntityFromServer();
        T updated = makeEntityWithUpdates(original);
        URI uri = tester.apiBase.resolve(original.getSelfUri());
        URI newUri = URI.create("/newuri");
        tester.mockPatchResource(uri, 204, some(newUri.toASCIIString()));
        tester.mockResourceError(tester.apiBase.resolve(newUri), 404);
        
        try
        {
            updateEntity(original, updated);
            fail("expected error");
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e.getStatus(), equalTo(404));
        }
    }

    @Test(expected = MpacException.CannotUpdateNonServerSideEntity.class)
    public void updateThrowsExceptionIfOriginalEntityIsNotFromServer() throws Exception
    {
        T original = makeEntityToCreate();
        T updated = makeEntityWithUpdates(original);
        
        updateEntity(original, updated);
    }
}
