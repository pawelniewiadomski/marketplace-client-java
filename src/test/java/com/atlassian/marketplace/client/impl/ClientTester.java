package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.http.RequestDecorator;
import com.atlassian.marketplace.client.http.SimpleHttpResponse;
import com.atlassian.marketplace.client.model.ErrorDetail;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimap;

import org.apache.commons.io.IOUtils;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.atlassian.fugue.Pair.pair;
import static com.atlassian.marketplace.client.TestObjects.API_V2_BASE_PATH;
import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.google.common.base.Preconditions.checkState;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientTester
{
    public static final String FAKE_ADDONS_PATH = "/fake/addons";
    public static final String FAKE_APPLICATIONS_PATH = "/fake/applications";
    public static final String FAKE_ASSETS_PATH = "/fake/assets";
    public static final String FAKE_PRODUCTS_PATH = "/fake/products";
    public static final String FAKE_VENDORS_PATH = "/fake/vendors";

    final DefaultMarketplaceClient client;
    
    public final URI apiBase;
    public final HttpTransport httpTransport;
    protected final EntityEncoding encoding;
    protected final Map<Pair<URI, String>, SimpleHttpResponse> responses;
    protected final Map<Pair<URI, String>, Object> receivedData;
    protected final Map<Pair<URI, String>, ImmutableMap<String, String>> receivedHeaders;
    protected final JsonEntityEncoding jsonEncoding = new JsonEntityEncoding();
    
    public ClientTester(URI baseUri) throws Exception
    {
        this.apiBase = URI.create(HOST_BASE + API_V2_BASE_PATH + "/");
        
        httpTransport = mock(HttpTransport.class);
        encoding = mock(EntityEncoding.class);
        responses = new HashMap<Pair<URI, String>, SimpleHttpResponse>();
        receivedData = new HashMap<Pair<URI, String>, Object>();
        receivedHeaders = new HashMap<Pair<URI, String>, ImmutableMap<String, String>>();
        
        client = new DefaultMarketplaceClient(baseUri, httpTransport, encoding);

        doAnswer(mockEncode()).when(encoding).encode(any(OutputStream.class), any(Object.class), Mockito.anyBoolean());
        setupMockRequests(httpTransport, ImmutableMap.<String, String>of());        
        doAnswer(mockWithRequestDecorator()).when(httpTransport).withRequestDecorator(any(RequestDecorator.class));
    }
    
    @SuppressWarnings("unchecked")
    private void setupMockRequests(HttpTransport ht, ImmutableMap<String, String> headers) throws Exception
    {
        doAnswer(mockHttpAnswer("DELETE", headers)).when(ht).delete(any(URI.class));
        doAnswer(mockHttpAnswer("GET", headers)).when(ht).get(any(URI.class));
        doAnswer(mockHttpAnswer("POST", headers)).when(ht).post(any(URI.class), any(InputStream.class), anyLong(),
                                                                anyString(), anyString());
        doAnswer(mockHttpAnswer("POST", headers)).when(ht).postParams(any(URI.class), any(Multimap.class));
        doAnswer(mockHttpAnswer("PUT", headers)).when(ht).put(any(URI.class), any(byte[].class));
        doAnswer(mockHttpAnswer("PATCH", headers)).when(ht).patch(any(URI.class), any(byte[].class));
    }
    
    private Answer<SimpleHttpResponse> mockHttpAnswer(final String method, final ImmutableMap<String, String> headers)
    {
        return new Answer<SimpleHttpResponse>()
        {
            public SimpleHttpResponse answer(InvocationOnMock invocation) throws Throwable
            {
                URI uri = (URI) invocation.getArguments()[0];
                Pair<URI, String> key = pair(uri, method);
                if (!responses.containsKey(key))
                {
                    throw new IllegalStateException("unexpected " + method + ": " + uri + " (expected: "
                                                    + Joiner.on(", ").join(responses.keySet()) + ")");
                }
                
                receivedHeaders.put(key, headers);
                
                if (invocation.getArguments().length > 1)
                {
                    receivedData.put(key, toReceivedData(invocation.getArguments()[1]));
                }
                
                return responses.get(key);
            }
        };
    }

    private Answer<HttpTransport> mockWithRequestDecorator()
    {
        return new Answer<HttpTransport>()
        {   
            public HttpTransport answer(InvocationOnMock invocation) throws Throwable
            {
                RequestDecorator rd = (RequestDecorator) invocation.getArguments()[0];
                HttpTransport mock = mock(HttpTransport.class);
                setupMockRequests(mock, ImmutableMap.copyOf(rd.getRequestHeaders()));
                return mock;
            }
        };
    }

    private Object toReceivedData(Object o) throws Exception
    {
        if (o instanceof InputStream)
        {
            return IOUtils.toByteArray((InputStream) o);
        }
        return o;
    }
    
    private Answer<Void> mockEncode()
    {
        return new Answer<Void>()
        {
            public Void answer(InvocationOnMock invocation) throws Throwable
            {
                OutputStream os = (OutputStream) invocation.getArguments()[0];
                Object entity = invocation.getArguments()[1];
                boolean includeReadOnly = (Boolean) invocation.getArguments()[2];
                jsonEncoding.encode(os, entity, includeReadOnly);
                return null;
            }
        };
    }

    private Answer<Void> mockEncodeChanges(final String content)
    {
        return new Answer<Void>()
        {
            public Void answer(InvocationOnMock invocation) throws Throwable
            {
                OutputStream os = (OutputStream) invocation.getArguments()[0];
                os.write(content.getBytes());
                return null;
            }
        };
    }

    public static InternalModel.MinimalLinks defaultRootResource()
    {
        return new InternalModel.MinimalLinks(createDefaultRootLinks().build());
    }

    public static InternalModel.MinimalLinks rootResourceMinusLink(String rel)
    {
        return new InternalModel.MinimalLinks(createDefaultRootLinks().remove(rel).build());
    }
    
    public static ModelBuilders.LinksBuilder createDefaultRootLinks()
    {
        return ModelBuilders.links()
            .put("addons", URI.create(FAKE_ADDONS_PATH))
            .put("applications", URI.create(FAKE_APPLICATIONS_PATH))
            .put("assets", URI.create(FAKE_ASSETS_PATH))
            .put("products", URI.create(FAKE_PRODUCTS_PATH))
            .put("vendors", URI.create(FAKE_VENDORS_PATH));
    }
    
    public UriBuilder apiUri(String path)
    {
        return UriBuilder.fromUri(apiBase).path(path);
    }
    
    static String relativeApiPath(URI uri)
    {
        String s = uri.toString();
        checkState(s.startsWith(HOST_BASE));
        return s.substring(HOST_BASE.length());
    }
    
    SimpleHttpResponse mockResponse(int status, boolean isEmpty, InputStream mockStream) throws Exception
    {
        SimpleHttpResponse ret = mock(SimpleHttpResponse.class);
        when(ret.getStatus()).thenReturn(status);
        when(ret.isEmpty()).thenReturn(isEmpty);
        when(ret.getContentStream()).thenReturn(mockStream);
        when(ret.getHeader(Mockito.anyString())).thenReturn(ImmutableList.<String>of());
        return ret;
    }

    @SuppressWarnings("unchecked")
    public <T> void mockResource(URI uri, T rep) throws Exception
    {
        mockResource(uri, rep, (Class<T>)rep.getClass());
    }

    public <T> void mockResource(URI uri, T rep, Class<T> type) throws Exception
    {
        mockResourceInternal(uri, rep, type, "GET");
    }

    public <T> void mockEmptyResponse(URI uri) throws Exception
    {
        InputStream mockStream = mock(InputStream.class);
        responses.put(pair(uri, "GET"), mockResponse(200, true, mockStream));
    }

    public <T> void mockDeleteResource(URI uri, int status) throws Exception
    {
        mockResourceStatusInternal(uri, status, "DELETE");
    }

    public <T> void mockPostResource(URI uri, int status) throws Exception
    {
        mockResourceStatusInternal(uri, status, "POST");
    }

    public <T> void mockPostResource(URI uri, int status, Map<String, Iterable<String>> headers) throws Exception
    {
        mockResourceStatusInternal(uri, status, "POST", headers);
    }

    @SuppressWarnings("unchecked")
    public <T> void mockPostResourceResponse(URI uri, T responseRep) throws Exception
    {
        mockResourceInternal(uri, responseRep, (Class<T>)responseRep.getClass(), "POST");
    }

    public <T> void mockPutResource(URI uri, int status) throws Exception
    {
        mockResourceStatusInternal(uri, status, "PUT");
    }

    public <T> void mockPatchResource(URI uri, int status, Option<String> location) throws Exception
    {
        ImmutableMap.Builder<String, Iterable<String>> headers = ImmutableMap.builder();
        for (String l: location)
        {
            headers.put("Location", ImmutableList.of(l));
        }
        mockResourceStatusInternal(uri, status, "PATCH", headers.build());
    }

    @SuppressWarnings("unchecked")
    <T> void mockPutResourceResponse(URI uri, T responseRep) throws Exception
    {
        mockResourceInternal(uri, responseRep, (Class<T>)responseRep.getClass(), "PUT");
    }
    
    private <T> void mockResourceInternal(URI uri, T rep, Class<T> type, String method)
        throws Exception
    {
        InputStream mockStream = mock(InputStream.class);
        responses.put(pair(uri, method), mockResponse(200, false, mockStream));
        when(encoding.decode(mockStream, type)).thenReturn(rep);
    }

    public void mockResourceErrorBody(URI uri, int status, Iterable<ErrorDetail> details, String method)
        throws Exception
    {
        InternalModel.ErrorDetails ed = InternalModel.errorDetails(details);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        jsonEncoding.encode(baos, ed, true);
        mockResourceErrorBody(uri, status, new String(baos.toByteArray()), method);
        when(encoding.decode(any(ByteArrayInputStream.class), eq(InternalModel.ErrorDetails.class)))
            .thenReturn(ed);
    }

    public void mockResourceErrorBody(URI uri, int status, String body, String method)
        throws Exception
    {
        InputStream stream = new ByteArrayInputStream(body.getBytes());
        responses.put(pair(uri, method), mockResponse(status, false, stream));
    }
    
    public void mockResourceError(URI uri, int status) throws Exception
    {
        mockResourceStatusInternal(uri, status, "GET");
    }
    
    public void mockPostResourceError(URI uri, int status) throws Exception
    {
        mockResourceStatusInternal(uri, status, "POST");
    }

    public void mockResourceStatusInternal(URI uri, int status, String method) throws Exception
    {
        mockResourceStatusInternal(uri, status, method, ImmutableMap.<String, Iterable<String>>of());
    }
    
    @SuppressWarnings("unchecked")
    void mockResourceStatusInternal(URI uri, int status, String method, Map<String, Iterable<String>> headers) throws Exception
    {
        InputStream mockStream = mock(InputStream.class);
        SimpleHttpResponse resp = mockResponse(status, false, mockStream);
        for (Map.Entry<String, Iterable<String>> h: headers.entrySet())
        {
            when(resp.getHeader(h.getKey())).thenReturn(h.getValue());
        }
        responses.put(pair(uri, method), resp);
        when(encoding.decode(same(mockStream), any(Class.class))).thenThrow(
            new IllegalStateException("encoding.decode should not have been called for this response"));
    }

    public void mockPatchDocument(Object original, Object updated, String document) throws Exception
    {
        doAnswer(mockEncodeChanges(document)).when(encoding).encodeChanges(any(OutputStream.class), same(original), same(updated));
    }
    
    public Object getReceivedObject(URI uri, String method) throws Exception
    {
        Pair<URI, String> key = pair(uri, method);
        checkState(receivedData.containsKey(key));
        return receivedData.get(key);
    }

    public String getReceivedData(URI uri, String method) throws Exception
    {
        return new String((byte[]) getReceivedObject(uri, method));
    }
    
    public ImmutableMap<String, String> getReceivedExtraHeaders(URI uri, String method) throws Exception
    {
        if (receivedHeaders.containsKey(pair(uri, method)))
        {
            return receivedHeaders.get(pair(uri, method));
        }
        return ImmutableMap.<String, String>of();
    }
}
