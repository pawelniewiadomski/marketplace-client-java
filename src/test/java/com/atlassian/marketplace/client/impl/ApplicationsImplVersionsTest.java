package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.ApplicationVersionSpecifier;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.atlassian.marketplace.client.util.UriBuilder;

import org.junit.Test;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_APPLICATIONS_PATH;
import static com.atlassian.marketplace.client.model.TestModelBuilders.applicationVersion;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.verify;

public abstract class ApplicationsImplVersionsTest extends ApiImplTestBaseWithCreateAndUpdate<ApplicationVersion>
    implements ApplicationsImplTestBase
{
    @Test
    public void getVersionUsesApplicationsResource() throws Exception
    {
        setupAppVersionResource(JIRA, latestVersionUri(JIRA));
        tester.client.applications().getVersion(JIRA, ApplicationVersionSpecifier.latest());

        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_APPLICATIONS_PATH + "?limit=0"));
    }

    @Test
    public void getLatestVersionUsesLatestVersionResource() throws Exception
    {
        setupAppVersionResource(JIRA, latestVersionUri(JIRA));

        assertThat(tester.client.applications().getVersion(JIRA, ApplicationVersionSpecifier.latest()),
                contains(APP_VER_REP));
    }

    @Test
    public void getVersionByBuildUsesVersionByBuildResource() throws Exception
    {
        setupAppVersionResource(JIRA, versionByBuildUri(JIRA, 100));

        assertThat(tester.client.applications().getVersion(JIRA, ApplicationVersionSpecifier.buildNumber(100)),
                contains(APP_VER_REP));
    }

    @Test
    public void getVersionByNameUsesVersionByNameResource() throws Exception
    {
        setupAppVersionResource(JIRA, versionByNameUri(JIRA, "1.0"));

        assertThat(tester.client.applications().getVersion(JIRA, ApplicationVersionSpecifier.versionName("1.0")),
                contains(APP_VER_REP));
    }
    
    @Override
    protected URI getCollectionUri()
    {
        return URI.create(HOST_BASE + FAKE_APP_VERSIONS_PATH);
    }
    
    @Override
    protected ApplicationVersion makeEntityToCreate()
    {
        return applicationVersion().build();
    }
    
    @Override
    protected ApplicationVersion makeEntityFromServer()
    {
        return applicationVersion().links(APP_LINKS).build();
    }
    
    @Override
    protected ApplicationVersion makeEntityWithUpdates(ApplicationVersion from)
    {
        return applicationVersion(from).build();
    }
    
    @Override
    protected ApplicationVersion createEntity(ApplicationVersion entity) throws Exception
    {
        setupAppByKeyResource(appByKeyUri(JIRA));
        return tester.client.applications().createVersion(JIRA, entity);
    }
    
    @Override
    protected ApplicationVersion updateEntity(ApplicationVersion original, ApplicationVersion updated) throws MpacException
    {
        return tester.client.applications().updateVersion(original, updated);
    }

    protected UriBuilder appsApi()
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APPLICATIONS_PATH);
    }
    
    protected UriBuilder appsApiZeroLengthQuery()
    {
        return appsApi().queryParam("limit", 0);
    }

    protected UriBuilder appByKeyUri(ApplicationKey key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APP_BY_KEY_PATH + key.getKey());
    }

    protected void setupAppsResource(UriBuilder appsUri) throws Exception
    {
        setupAppsResource(appsUri, APPS_REP);
    }
    
    protected void setupAppsResource(UriBuilder appsUri, InternalModel.Applications rep) throws Exception
    {
        tester.mockResource(appsUri.build(), rep);
    }
    
    protected void setupAppByKeyResource(UriBuilder uri) throws Exception
    {
        setupAppsResource(appsApiZeroLengthQuery());
        tester.mockResource(uri.build(), APP_REP);
    }
    
    protected UriBuilder latestVersionUri(ApplicationKey key)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APP_LATEST_PATH + key.getKey());
    }

    protected UriBuilder versionByBuildUri(ApplicationKey key, int buildNumber)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APP_VER_PATH + key.getKey() + "/" + buildNumber);
    }

    protected UriBuilder versionByNameUri(ApplicationKey key, String name)
    {
        return UriBuilder.fromUri(HOST_BASE + FAKE_APP_VER_PATH + "name/" + name);
    }
    
    protected void setupAppVersionResource(ApplicationKey key, UriBuilder uri) throws Exception
    {
        setupAppByKeyResource(appByKeyUri(key));
        tester.mockResource(uri.build(), APP_VER_REP);
    }
}
