package com.atlassian.marketplace.client.impl;

import java.io.File;
import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.api.ImagePurpose.LOGO;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_ASSETS_PATH;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AssetsImplTest extends ApiImplTestBase
{
    private static final URI FAKE_ARTIFACT_URI = URI.create("/artifact");
    private static final URI FAKE_ASSET_URI = URI.create("/asset");
    private static final String FAKE_IMAGE_PATH = "/image/{imageType}";
    private static final Links ASSETS_LINKS = links()
            .put("artifact", FAKE_ARTIFACT_URI)
            .putTemplate("imageByType", FAKE_IMAGE_PATH)
            .build();
    private static final InternalModel.MinimalLinks ASSETS_REP = new InternalModel.MinimalLinks(ASSETS_LINKS);
    private static final Links RESPONSE_LINKS = links()
            .put("self", FAKE_ASSET_URI)
            .build();
    private static final InternalModel.MinimalLinks ASSET_RESPONSE = new InternalModel.MinimalLinks(RESPONSE_LINKS);
    private static final String FILE_DATA = "foo";
    
    private static File file;
    
    @BeforeClass
    public static void makeTempFile() throws Exception
    {
        file = File.createTempFile("AssetsImplTest", "txt");
        writeStringToFile(file, FILE_DATA);
    }
    
    @AfterClass
    public static void cleanupFile()
    {
        file.delete();
    }
    
    @Before
    public void setupAssetsRoot() throws Exception
    {
        tester.mockResource(assetsApi(), ASSETS_REP);
    }
    
    @Test
    public void uploadArtifactPostsData() throws Exception
    {
        tester.mockPostResourceResponse(artifactUploadUri(), ASSET_RESPONSE);
        tester.client.assets().uploadAddonArtifact(file);
        assertThat(tester.getReceivedData(artifactUploadUri(), "POST"), equalTo(FILE_DATA));
    }

    @Test
    public void uploadImagePostsData() throws Exception
    {
        tester.mockPostResourceResponse(imageUploadUri(), ASSET_RESPONSE);
        tester.client.assets().uploadImage(file, LOGO);
        assertThat(tester.getReceivedData(imageUploadUri(), "POST"), equalTo(FILE_DATA));
    }

    @Test
    public void uploadArtifactReturnsAssetId() throws Exception
    {
        tester.mockPostResourceResponse(artifactUploadUri(), ASSET_RESPONSE);
        assertThat(tester.client.assets().uploadAddonArtifact(file), equalTo(ArtifactId.fromUri(FAKE_ASSET_URI)));
    }

    @Test
    public void uploadImageReturnsAssetId() throws Exception
    {
        tester.mockPostResourceResponse(imageUploadUri(), ASSET_RESPONSE);
        assertThat(tester.client.assets().uploadImage(file, LOGO), equalTo(ImageId.fromUri(FAKE_ASSET_URI)));
    }

    @Test
    public void uploadArtifactThrowsExceptionIfServerReturnsError() throws Exception
    {
        tester.mockPostResource(artifactUploadUri(), 400);
        try
        {
            tester.client.assets().uploadAddonArtifact(file);
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e.getStatus(), equalTo(400));
        }
    }
    
    @Test
    public void uploadImageThrowsExceptionIfServerReturnsError() throws Exception
    {
        tester.mockPostResource(imageUploadUri(), 400);
        try
        {
            tester.client.assets().uploadImage(file, LOGO);
        }
        catch (MpacException.ServerError e)
        {
            assertThat(e.getStatus(), equalTo(400));
        }
    }
    
    private URI artifactUploadUri()
    {
        return uploadUri(FAKE_ARTIFACT_URI);
    }

    private URI imageUploadUri()
    {
        return uploadUri(URI.create(FAKE_IMAGE_PATH.replace("{imageType}", "logo")));
    }
    
    private URI uploadUri(URI resourceUri)
    {
        return UriBuilder.fromUri(tester.apiBase.resolve(resourceUri)).queryParam("file", file.getName()).build();
    }
    
    private URI assetsApi()
    {
        return URI.create(HOST_BASE + FAKE_ASSETS_PATH);
    }
}
