package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.util.regex.Pattern;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.AddonCategoryId;
import com.atlassian.marketplace.client.api.AddonExternalLinkType;
import com.atlassian.marketplace.client.api.AddonVersionExternalLinkType;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.api.VendorExternalLinkType;
import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonCategorySummary;
import com.atlassian.marketplace.client.model.AddonDistributionSummary;
import com.atlassian.marketplace.client.model.AddonPricing;
import com.atlassian.marketplace.client.model.AddonPricingItem;
import com.atlassian.marketplace.client.model.AddonReviewsSummary;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionDataCenterStatus;
import com.atlassian.marketplace.client.model.AddonVersionStatus;
import com.atlassian.marketplace.client.model.AddonVersionSummary;
import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationStatus;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.atlassian.marketplace.client.model.ApplicationVersionStatus;
import com.atlassian.marketplace.client.model.ConnectScope;
import com.atlassian.marketplace.client.model.Highlight;
import com.atlassian.marketplace.client.model.HtmlString;
import com.atlassian.marketplace.client.model.ImageInfo;
import com.atlassian.marketplace.client.model.LicenseEditionType;
import com.atlassian.marketplace.client.model.Link;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.PaymentModel;
import com.atlassian.marketplace.client.model.Product;
import com.atlassian.marketplace.client.model.ProductVersion;
import com.atlassian.marketplace.client.model.Screenshot;
import com.atlassian.marketplace.client.model.Vendor;
import com.atlassian.marketplace.client.model.VendorSummary;
import com.atlassian.marketplace.client.model.VersionCompatibility;
import com.atlassian.marketplace.client.model.SupportDetails;
import com.atlassian.marketplace.client.model.SupportOrganization;
import com.atlassian.utt.matchers.NamedFunction;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.apache.commons.io.IOUtils;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.TestObjects.utcDateTime;
import static com.atlassian.marketplace.client.model.AddonVersionDataCenterStatus.COMPATIBLE;
import static com.atlassian.marketplace.client.model.HtmlString.html;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.TestModelBuilders.connectScope;
import static com.atlassian.utt.matchers.NamedFunction.namedFunction;
import static com.atlassian.utt.reflect.ReflectionFunctions.accessor;
import static com.atlassian.utt.reflect.ReflectionFunctions.iterableAccessor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class JsonEntityEncodingTest
{
    private static final AddonCategoryId CATEGORY_ID = AddonCategoryId.fromUri(URI.create("/rest/categories/1"));
    
    private JsonEntityEncoding encoding;

    @Before
    public void setUp()
    {
        encoding = new JsonEntityEncoding();
    }

    @Test
    public void canDecodeSingleLink() throws Exception
    {
        Links links = decode("v2/links", Links.class);
        assertThat(links, hasLink("rel1", "uri1"));
    }

    @Test
    public void canDecodeLinkArray() throws Exception
    {
        Links links = decode("v2/links", Links.class);
        assertThat(links, hasLinkArray("rel2", "uri2a", "uri2b"));
    }

    @Test
    public void canDecodeLinkTemplate() throws Exception
    {
        Links links = decode("v2/links", Links.class);
        assertThat(links, hasLinkTemplate("rel3", "/template/{param}"));
    }

    @Test
    public void canEncodeSingleLink() throws Exception
    {
        Links links = ModelBuilders.links().put("rel1", URI.create("uri1")).build();
        assertThat(encode(links), equalTo("{\"rel1\":{\"href\":\"uri1\"}}"));
    }

    @Test
    public void canEncodeLinkArray() throws Exception
    {
        Links links = ModelBuilders.links().put("rel1", ImmutableList.of(URI.create("uri1"),
                                                                         URI.create("uri2"))).build();
        assertThat(encode(links), equalTo("{\"rel1\":[{\"href\":\"uri1\"},{\"href\":\"uri2\"}]}"));
    }

    @Test
    public void canDecodeAddon() throws Exception
    {
        assertThat(decode("v2/addon", Addon.class), testAddon());
    }

    @Test
    public void canDecodeAddonCategorySummary() throws Exception
    {
        assertThat(decode("v2/addonCategorySummary", AddonCategorySummary.class), testAddonCategorySummary());
    }

    @Test
    public void canDecodeAddonDistributionSummaryWithoutInstalls() throws Exception
    {
        assertThat(decode("v2/addonDistributionSummary1",
                          AddonDistributionSummary.class), testAddonDistributionSummary());
    }

    @Test
    public void canDecodeAddonDistributionSummaryWithInstalls() throws Exception
    {
        assertThat(decode("v2/addonDistributionSummary2", AddonDistributionSummary.class), testAddonDistributionSummary(
            true));
    }

    @Test
    public void canDecodeAddonPricing() throws Exception
    {
        assertThat(decode("v2/addonPricing", AddonPricing.class), testAddonPricing());
    }

    @Test
    public void canDecodeAddonPupPricing() throws Exception
    {
        assertThat(decode("v2/addonPupPricing", AddonPricing.class), testAddonPupPricing());
    }

    @Test
    public void canDecodeAddonRoleBasedPricing() throws Exception
    {
        assertThat(decode("v2/addonRoleBasedPricing", AddonPricing.class), testAddonRoleBasedPricing());
    }

    @Test
    public void canDecodeAddonReviewsSummary() throws Exception
    {
        assertThat(decode("v2/addonReviewsSummary", AddonReviewsSummary.class), testAddonReviewsSummary());
    }

    @Test
    public void canDecodeAddonSummary() throws Exception
    {
        assertThat(decode("v2/addonSummary", AddonSummary.class), testAddonSummary());
    }

    @Test
    public void canDecodeAddonVersion() throws Exception
    {
        assertThat(decode("v2/addonVersion", AddonVersion.class), testAddonVersion());
    }

    @Test
    public void canDecodeAddonVersionSummary() throws Exception
    {
        assertThat(decode("v2/addonVersionSummary", AddonVersionSummary.class), testAddonVersionSummary());
    }

    @Test
    public void canDecodeApplication() throws Exception
    {
        assertThat(decode("v2/application", Application.class), testApplication());
    }
    
    @Test
    public void canDecodeApplicationVersion() throws Exception
    {
        assertThat(decode("v2/applicationVersion", ApplicationVersion.class), testApplicationVersion());
    }
    
    @Test
    public void canDecodeHighlightWithoutExplanation() throws Exception
    {
        assertThat(decode("v2/highlight1", Highlight.class), testHighlight());
    }

    @Test
    public void canDecodeHighlightWithExplanation() throws Exception
    {
        assertThat(decode("v2/highlight2", Highlight.class), testHighlight(true));
    }

    @Test
    public void canDecodeProduct() throws Exception
    {
        assertThat(decode("v2/product", Product.class), testProduct());
    }

    @Test
    public void canDecodeProductVersion() throws Exception
    {
        assertThat(decode("v2/productVersion", ProductVersion.class), testProductVersion());
    }

    @Test
    public void canDecodeScreenshotWithoutCaption() throws Exception
    {
        assertThat(decode("v2/screenshot1", Screenshot.class), testScreenshot());
    }

    @Test
    public void canDecodeScreenshotWithCaption() throws Exception
    {
        assertThat(decode("v2/screenshot2", Screenshot.class), testScreenshot(true));
    }

    @Test
    public void canDecodeVendor() throws Exception
    {
        assertThat(decode("v2/vendor", Vendor.class), testVendor());
    }

    @Test
    public void canDecodeVendorSummary() throws Exception
    {
        assertThat(decode("v2/vendorSummary", VendorSummary.class), testVendorSummary());
    }

    @Test
    public void patchIsEmptyForUnchangedObject() throws Exception
    {
        ConnectScope o = connectScope("key", "name", "desc");
        assertThat(encodeChanges(o, o), equalTo("[]"));
    }
    
    @Test
    public void patchHasReplaceForModifiedTopLevelProperty() throws Exception
    {
        ConnectScope o0 = connectScope("key", "name0", "desc");
        ConnectScope o1 = connectScope("key", "name1", "desc");
        assertThat(encodeChanges(o0, o1),
            equalTo("[{\"op\":\"replace\",\"path\":\"/name\",\"value\":\"name1\"}]"));
    }

    @Test
    public void patchHasAddForNewTopLevelProperty() throws Exception
    {
        Links links = links().put("rel0", URI.create("/url")).put("rel1", some("foo"), URI.create("/url")).build();
        Link link0 = links.getLink("rel0").get();
        Link link1 = links.getLink("rel1").get();
        assertThat(encodeChanges(link0, link1),
            equalTo("[{\"op\":\"add\",\"path\":\"/type\",\"value\":\"foo\"}]"));
    }

    @Test
    public void patchHasRemoveForRemovedTopLevelProperty() throws Exception
    {
        Links links = links().put("rel0", URI.create("/url")).put("rel1", some("foo"), URI.create("/url")).build();
        Link link0 = links.getLink("rel0").get();
        Link link1 = links.getLink("rel1").get();
        assertThat(encodeChanges(link1, link0),
            equalTo("[{\"op\":\"remove\",\"path\":\"/type\"}]"));
    }

    @Test
    public void patchHasReplaceForModifiedNestedProperty() throws Exception
    {
        Links links0 = links().put("rel0", URI.create("/url")).put("rel1", URI.create("/url")).build();
        Links links1 = links().put("rel0", URI.create("/url")).put("rel1", URI.create("/url2")).build();
        assertThat(encodeChanges(links0, links1),
            equalTo("[{\"op\":\"replace\",\"path\":\"/rel1/href\",\"value\":\"/url2\"}]"));
    }

    @Test
    public void patchHasAddForAddedNestedProperty() throws Exception
    {
        Links links0 = links().put("rel0", URI.create("/url")).put("rel1", URI.create("/url")).build();
        Links links1 = links().put("rel0", URI.create("/url")).put("rel1", some("foo"), URI.create("/url")).build();
        assertThat(encodeChanges(links0, links1),
            equalTo("[{\"op\":\"add\",\"path\":\"/rel1/type\",\"value\":\"foo\"}]"));
    }

    @Test
    public void patchHasRemoveForRemovedNestedProperty() throws Exception
    {
        Links links0 = links().put("rel0", URI.create("/url")).put("rel1", URI.create("/url")).build();
        Links links1 = links().put("rel0", URI.create("/url")).put("rel1", some("foo"), URI.create("/url")).build();
        assertThat(encodeChanges(links1, links0),
            equalTo("[{\"op\":\"remove\",\"path\":\"/rel1/type\"}]"));
    }

    @Test
    public void patchHasReplaceForEntireArrayIfPropertyOfElementIsModified() throws Exception
    {
        Links links0 = links().put("rel", ImmutableList.of(URI.create("/url0"), URI.create("/url1"))).build();
        Links links1 = links().put("rel", ImmutableList.of(URI.create("/url0"), URI.create("/url2"))).build();
        assertThat(encodeChanges(links0, links1),
            equalTo("[{\"op\":\"replace\",\"path\":\"/rel\",\"value\":[{\"href\":\"/url0\"},{\"href\":\"/url2\"}]}]"));
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
    private static Matcher<Addon> testAddon()
    {
        return allOf(
            accessor(Addon.class, String.class, "getKey").is("plugin.key"),
            accessor(Addon.class, String.class, "getName").is("Addon Name"),
            iterableAccessor(Addon.class, String.class, "getSummary").is(some("addon summary")),
            accessor(Addon.class, URI.class, "getAlternateUri").is(URI.create("http://marketplace.atlassian.com/addon/plugin.key")),
            accessor(Addon.class, VendorId.class, "getVendorId").is(VendorId.fromUri(URI.create("http://marketplace.atlassian.com/vendors/1"))),
            iterableAccessor(Addon.class, ImageInfo.class, "getLogo").is(contains(testImageAsset())),
            iterableAccessor(Addon.class, ImageInfo.class, "getBanner").is(contains(testImageAsset())),
            iterableAccessor(Addon.class, AddonCategorySummary.class, "getCategories").is(contains(testAddonCategorySummary())),
            iterableAccessor(Addon.class, AddonCategoryId.class, "getCategoryIds").is(contains(CATEGORY_ID)),
            accessor(Addon.class, AddonDistributionSummary.class, "getDistribution").is(testAddonDistributionSummary(true)),
            accessor(Addon.class, AddonReviewsSummary.class, "getReviews").is(testAddonReviewsSummary()),
            iterableAccessor(Addon.class, VendorSummary.class, "getVendor").is(contains(testVendorSummary())),
            iterableAccessor(Addon.class, AddonVersion.class, "getVersion").is(contains(testAddonVersion())),
            iterableAccessor(Addon.class, HtmlString.class, "getDescription").is(some(html("addon description"))),
            addonUri(AddonExternalLinkType.ISSUE_TRACKER).is(some(URI.create("/issueTracker"))),
            addonUri(AddonExternalLinkType.FORUMS).is(some(URI.create("/forums"))),
            addonUri(AddonExternalLinkType.PRIVACY).is(some(URI.create("/privacy"))),
            addonUri(AddonExternalLinkType.WIKI).is(some(URI.create("/wiki"))),
            addonUri(AddonExternalLinkType.SOURCE).is(some(URI.create("/source"))),
            addonUri(AddonExternalLinkType.BUILDS).is(some(URI.create("/builds")))
        );
    }
    private static NamedFunction<Addon, Option<URI>> addonUri(final AddonExternalLinkType type)
    {
        return namedFunction(type.getKey() + "Uri", new Function<Addon, Option<URI>>()
        {
            public Option<URI> apply(Addon a)
            {
                return a.getExternalLinkUri(type);
            }
        });
    }
    
    private static Matcher<AddonCategorySummary> testAddonCategorySummary()
    {
        return allOf(
            accessor(AddonCategorySummary.class, Links.class, "getLinks").is(hasLink("self", CATEGORY_ID.getUri().toString())),
            accessor(AddonCategorySummary.class, AddonCategoryId.class, "getId").is(CATEGORY_ID),
            accessor(AddonCategorySummary.class, String.class, "getName").is("my-name")
        );
    }

    private static Matcher<AddonDistributionSummary> testAddonDistributionSummary()
    {
        return testAddonDistributionSummary(false);
    }

    private static Matcher<AddonDistributionSummary> testAddonDistributionSummary(boolean withInstalls)
    {
        return allOf(
            accessor(AddonDistributionSummary.class, boolean.class, "isBundled").is(true),
            accessor(AddonDistributionSummary.class, int.class, "getDownloads").is(1000),
            iterableAccessor(AddonDistributionSummary.class, Integer.class, "getTotalInstalls").is(withInstalls ? some(200) : none(Integer.class)),
            iterableAccessor(AddonDistributionSummary.class, Integer.class, "getTotalUsers").is(withInstalls ? some(100) : none(Integer.class))
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonPricing> testAddonPricing()
    {
        return allOf(
            accessor(AddonPricing.class, Links.class, "getLinks").is(hasLink("self", "/rest/plugins/plugin.key/pricing")),
            iterableAccessor(AddonPricing.class, AddonPricingItem.class, "getItems").is(contains(testAddonPricingItem())),
            accessor(AddonPricing.class, Option.class, "getPerUnitItemsIfSpecified").is(Option.<AddonPricingItem>none()),
            accessor(AddonPricing.class, boolean.class, "isExpertDiscountOptOut").is(true),
            accessor(AddonPricing.class, boolean.class, "isContactSalesForAdditionalPricing").is(false),
            accessor(AddonPricing.class, boolean.class, "isRoleBased").is(false),
            iterableAccessor(AddonPricing.class, String.class, "getParent").is(some("jira")),
            iterableAccessor(AddonPricing.class, DateTime.class, "getLastModified").is(some(utcDateTime(2015, 2, 10, 1, 2, 3)))
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonPricing> testAddonPupPricing()
    {
        return allOf(
                accessor(AddonPricing.class, Links.class, "getLinks").is(hasLink("self", "/rest/plugins/plugin.key/pricing")),
                iterableAccessor(AddonPricing.class, AddonPricingItem.class, "getItems").is(contains(testAddonPricingItem())),
                iterableAccessor(AddonPricing.class, AddonPricingItem.class, "getPerUnitItems").is(contains(testAddonPricingItem())),
                accessor(AddonPricing.class, boolean.class, "isExpertDiscountOptOut").is(true),
                accessor(AddonPricing.class, boolean.class, "isContactSalesForAdditionalPricing").is(false),
                accessor(AddonPricing.class, boolean.class, "isRoleBased").is(false),
                iterableAccessor(AddonPricing.class, String.class, "getParent").is(some("jira")),
                iterableAccessor(AddonPricing.class, DateTime.class, "getLastModified").is(some(utcDateTime(2015, 2, 10, 1, 2, 3)))
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonPricing> testAddonRoleBasedPricing()
    {
        return allOf(
            accessor(AddonPricing.class, Links.class, "getLinks").is(hasLink("self", "/rest/plugins/plugin.key/pricing")),
            iterableAccessor(AddonPricing.class, AddonPricingItem.class, "getItems").is(contains(testAddonRoleBasedPricingItem())),
            accessor(AddonPricing.class, boolean.class, "isExpertDiscountOptOut").is(true),
            accessor(AddonPricing.class, boolean.class, "isContactSalesForAdditionalPricing").is(true),
            accessor(AddonPricing.class, boolean.class, "isRoleBased").is(true),
            iterableAccessor(AddonPricing.class, String.class, "getParent").is(some("jira")),
            iterableAccessor(AddonPricing.class, DateTime.class, "getLastModified").is(some(utcDateTime(2015, 2, 10, 1, 2, 3))),
            iterableAccessor(AddonPricing.class, AddonPricing.RoleInfo.class, "getRoleInfo").is(contains(testAddonPricingRole()))
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonPricingItem> testAddonPricingItem()
    {
        return allOf(
            accessor(AddonPricingItem.class, String.class, "getDescription").is("Commercial 25 Users"),
            accessor(AddonPricingItem.class, String.class, "getEditionId").is("userTier"),
            accessor(AddonPricingItem.class, String.class, "getEditionDescription").is("Users"),
            accessor(AddonPricingItem.class, LicenseEditionType.class, "getEditionType").is(LicenseEditionType.USER_TIER),
            accessor(AddonPricingItem.class, String.class, "getLicenseType").is("COMMERCIAL"),
            accessor(AddonPricingItem.class, float.class, "getAmount").is(250.00f),
            iterableAccessor(AddonPricingItem.class, Float.class, "getRenewalAmount").is(contains(125.00f)),
            accessor(AddonPricingItem.class, int.class, "getUnitCount").is(25),
            accessor(AddonPricingItem.class, int.class, "getMonthsValid").is(12)
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonPricingItem> testAddonRoleBasedPricingItem()
    {
        return allOf(
            accessor(AddonPricingItem.class, String.class, "getDescription").is("Commercial 25 Users"),
            accessor(AddonPricingItem.class, String.class, "getEditionId").is("userTier"),
            accessor(AddonPricingItem.class, String.class, "getEditionDescription").is("Users"),
            accessor(AddonPricingItem.class, LicenseEditionType.class, "getEditionType").is(LicenseEditionType.ROLE_TIER),
            accessor(AddonPricingItem.class, String.class, "getLicenseType").is("COMMERCIAL"),
            accessor(AddonPricingItem.class, float.class, "getAmount").is(250.00f),
            iterableAccessor(AddonPricingItem.class, Float.class, "getRenewalAmount").is(contains(125.00f)),
            accessor(AddonPricingItem.class, int.class, "getUnitCount").is(25),
            accessor(AddonPricingItem.class, int.class, "getMonthsValid").is(12)
        );
    }

    private static Matcher<AddonPricing.RoleInfo> testAddonPricingRole()
    {
        return allOf(
            accessor(AddonPricing.RoleInfo.class, String.class, "getSingularName").is("Agent"),
            accessor(AddonPricing.RoleInfo.class, String.class, "getPluralName").is("Agents")
        );
    }

    private static Matcher<AddonReviewsSummary> testAddonReviewsSummary()
    {
        return allOf(
            accessor(AddonReviewsSummary.class, float.class, "getAverageStars").is(3.5f),
            accessor(AddonReviewsSummary.class, int.class, "getCount").is(1000)
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<AddonSummary> testAddonSummary()
    {
        return allOf(
            accessor(AddonSummary.class, String.class, "getKey").is("plugin.key"),
            accessor(AddonSummary.class, String.class, "getName").is("Addon Name"),
            iterableAccessor(AddonSummary.class, String.class, "getSummary").is(some("addon summary")),
            accessor(AddonSummary.class, URI.class, "getAlternateUri").is(URI.create("http://marketplace.atlassian.com/addon/plugin.key")),
            accessor(AddonSummary.class, VendorId.class, "getVendorId").is(VendorId.fromUri(URI.create("http://marketplace.atlassian.com/vendors/1"))),
            iterableAccessor(AddonSummary.class, AddonCategorySummary.class, "getCategories").is(contains(testAddonCategorySummary())),
            iterableAccessor(AddonSummary.class, AddonCategoryId.class, "getCategoryIds").is(contains(CATEGORY_ID)),
            accessor(AddonSummary.class, AddonDistributionSummary.class, "getDistribution").is(testAddonDistributionSummary(true)),
            iterableAccessor(AddonSummary.class, ImageInfo.class, "getLogo").is(contains(testImageAsset())),
            accessor(AddonSummary.class, AddonReviewsSummary.class, "getReviews").is(testAddonReviewsSummary()),
            iterableAccessor(AddonSummary.class, VendorSummary.class, "getVendor").is(contains(testVendorSummary())),
            iterableAccessor(AddonSummary.class, AddonVersionSummary.class, "getVersion").is(contains(testAddonVersionSummary()))
        );
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
    private static Matcher<AddonVersion> testAddonVersion()
    {
        return allOf(
            accessor(AddonVersion.class, long.class, "getBuildNumber").is(1000000000001L),
            iterableAccessor(AddonVersion.class, String.class, "getName").is(some("1.0")),
            accessor(AddonVersion.class, AddonVersionStatus.class, "getStatus").is(AddonVersionStatus.PUBLIC),
            accessor(AddonVersion.class, PaymentModel.class, "getPaymentModel").is(PaymentModel.PAID_VIA_ATLASSIAN),
            testAddonVersionReleaseProperties(AddonVersion.class),
            accessor(AddonVersion.class, boolean.class, "isStatic").is(false),
            accessor(AddonVersion.class, boolean.class, "isDeployable").is(true),
            accessor(AddonVersion.class, boolean.class, "isDataCenterStatusCompatible").is(false),
            iterableAccessor(AddonVersion.class, String.class, "getYoutubeId").is(some("abc")),
            addonVersionUri(AddonVersionExternalLinkType.BINARY).is(some(URI.create("/binary"))),
            addonVersionUri(AddonVersionExternalLinkType.DOCUMENTATION).is(some(URI.create("/documentation"))),
            addonVersionUri(AddonVersionExternalLinkType.LICENSE).is(some(URI.create("/license"))),
            addonVersionUri(AddonVersionExternalLinkType.LEARN_MORE).is(some(URI.create("/learnMore"))),
            addonVersionUri(AddonVersionExternalLinkType.EULA).is(some(URI.create("/eula"))),
            addonVersionUri(AddonVersionExternalLinkType.PURCHASE).is(some(URI.create("/purchase"))),
            addonVersionUri(AddonVersionExternalLinkType.RELEASE_NOTES).is(some(URI.create("/releaseNotes"))),
            addonVersionUri(AddonVersionExternalLinkType.JAVADOC).is(some(URI.create("/javadocs"))),
            addonVersionUri(AddonVersionExternalLinkType.SOURCE).is(some(URI.create("/source"))),
            addonVersionUri(AddonVersionExternalLinkType.EVALUATION_LICENSE).is(some(URI.create("/eval"))),
            addonVersionUri(AddonVersionExternalLinkType.DONATE).is(some(URI.create("/donate"))),
            iterableAccessor(AddonVersion.class, VersionCompatibility.class, "getCompatibilities").is(contains(testVersionCompatibility())),
            iterableAccessor(AddonVersion.class, AddonCategorySummary.class, "getFunctionalCategories").is(contains(testAddonCategorySummary())),
            iterableAccessor(AddonVersion.class, AddonCategoryId.class, "getFunctionalCategoryIds").is(contains(CATEGORY_ID)),
            iterableAccessor(AddonVersion.class, Highlight.class, "getHighlights").is(contains(testHighlight(false), testHighlight(true))),
            iterableAccessor(AddonVersion.class, Screenshot.class, "getScreenshots").is(contains(testScreenshot(false), testScreenshot(true))),
            iterableAccessor(AddonVersion.class, String.class, "getReleaseSummary").is(some("rs")),
            iterableAccessor(AddonVersion.class, HtmlString.class, "getMoreDetails").is(some(html("md"))),
            iterableAccessor(AddonVersion.class, HtmlString.class, "getReleaseNotes").is(some(html("rn"))),
            iterableAccessor(AddonVersion.class, ConnectScope.class, "getConnectScopes").is(contains(testConnectScope()))
        );
    }

    private static NamedFunction<AddonVersion, Option<URI>> addonVersionUri(final AddonVersionExternalLinkType type)
    {
        return namedFunction(type.getKey() + "Uri", new Function<AddonVersion, Option<URI>>()
        {
            public Option<URI> apply(AddonVersion v)
            {
                return v.getExternalLinkUri(type);
            }
        });
    }

    private static NamedFunction<AddonVersionSummary, Option<URI>> addonVersionSummaryUri(final AddonVersionExternalLinkType type)
    {
        return namedFunction(type.getKey() + "Uri", new Function<AddonVersionSummary, Option<URI>>()
        {
            public Option<URI> apply(AddonVersionSummary v)
            {
                return v.getExternalLinkUri(type);
            }
        });
    }

    private static <T> Matcher<T> testAddonVersionReleaseProperties(Class<T> entityClass)
    {
        return allOf(
            accessor(entityClass, LocalDate.class, "getReleaseDate").is(new LocalDate(2014, 05, 24)),
            iterableAccessor(entityClass, String.class, "getReleasedBy").is(some("me")),
            accessor(entityClass, boolean.class, "isBeta").is(false),
            accessor(entityClass, boolean.class, "isSupported").is(true)
        );
    }
    
    @SuppressWarnings("unchecked")
    private static Matcher<AddonVersionSummary> testAddonVersionSummary()
    {
        return allOf(
            iterableAccessor(AddonVersionSummary.class, String.class, "getName").is(some("1.0")),
            accessor(AddonVersionSummary.class, AddonVersionStatus.class, "getStatus").is(AddonVersionStatus.PUBLIC),
            accessor(AddonVersionSummary.class, PaymentModel.class, "getPaymentModel").is(PaymentModel.PAID_VIA_ATLASSIAN),
            testAddonVersionReleaseProperties(AddonVersionSummary.class),
            accessor(AddonVersionSummary.class, boolean.class, "isStatic").is(false),
            accessor(AddonVersionSummary.class, boolean.class, "isDeployable").is(true),
            accessor(AddonVersionSummary.class, boolean.class, "isServer").is(true),
            accessor(AddonVersionSummary.class, boolean.class, "isCloud").is(false),
            accessor(AddonVersionSummary.class, boolean.class, "isConnect").is(false),
            accessor(AddonVersionSummary.class, boolean.class, "isAutoUpdateAllowed").is(false),
            accessor(AddonVersionSummary.class, boolean.class, "isDataCenterCompatible").is(true),
            accessor(AddonVersionSummary.class, boolean.class, "isDataCenterStatusCompatible").is(true),
            iterableAccessor(AddonVersionSummary.class, AddonVersionDataCenterStatus.class, "getDataCenterStatus").is(some(COMPATIBLE)),
            iterableAccessor(AddonVersionSummary.class, AddonCategorySummary.class, "getFunctionalCategories").is(contains(testAddonCategorySummary())),
            iterableAccessor(AddonVersionSummary.class, AddonCategoryId.class, "getFunctionalCategoryIds").is(contains(CATEGORY_ID)),
            addonVersionSummaryUri(AddonVersionExternalLinkType.BINARY).is(some(URI.create("/binary"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.DOCUMENTATION).is(some(URI.create("/documentation"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.LICENSE).is(some(URI.create("/license"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.LEARN_MORE).is(some(URI.create("/learnMore"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.EULA).is(some(URI.create("/eula"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.PURCHASE).is(some(URI.create("/purchase"))),
            addonVersionSummaryUri(AddonVersionExternalLinkType.RELEASE_NOTES).is(some(URI.create("/releaseNotes")))
        );
    }
    
    @SuppressWarnings("unchecked")
    private static Matcher<Application> testApplication()
    {
        return allOf(
            accessor(Application.class, ApplicationKey.class, "getKey").is(ApplicationKey.JIRA),
            accessor(Application.class, String.class, "getName").is("JIRA"),
            accessor(Application.class, ApplicationStatus.class, "getStatus").is(ApplicationStatus.PUBLISHED),
            accessor(Application.class, String.class, "getIntroduction").is("yo"),
            accessor(Application.class, Application.CompatibilityUpdateMode.class, "getCompatibilityUpdateMode").is(Application.CompatibilityUpdateMode.MINOR_VERSIONS),
            accessor(Application.class, String.class, "getDescription").is("it's good"),
            accessor(Application.class, URI.class, "getLearnMoreUri").is(URI.create("http://learn/more")),
            iterableAccessor(Application.class, URI.class, "getDownloadPageUri").is(some(URI.create("http://download/it"))),
            iterableAccessor(Application.class, Integer.class, "getCloudFreeUsers").is(some(5))
        );
    }
    
    private static Matcher<ApplicationVersion> testApplicationVersion()
    {
        return allOf(
            accessor(ApplicationVersion.class, int.class, "getBuildNumber").is(100),
            accessor(ApplicationVersion.class, String.class, "getName").is("1.0"),
            accessor(ApplicationVersion.class, LocalDate.class, "getReleaseDate").is(new LocalDate(2014, 05, 24)),
            accessor(ApplicationVersion.class, ApplicationVersionStatus.class, "getStatus").is(ApplicationVersionStatus.PUBLISHED)
        );
    }
    
    private static Matcher<ConnectScope> testConnectScope()
    {
        return allOf(
            accessor(ConnectScope.class, URI.class, "getAlternateUri").is(URI.create("/permissions/read")),
            accessor(ConnectScope.class, String.class, "getKey").is("read"),
            accessor(ConnectScope.class, String.class, "getName").is("Read"),
            accessor(ConnectScope.class, String.class, "getDescription").is("Read things")
        );
    }
    
    private static Matcher<Highlight> testHighlight()
    {
        return testHighlight(false);
    }

    private static Matcher<Highlight> testHighlight(boolean withExplanation)
    {
        return allOf(
            accessor(Highlight.class, ImageInfo.class, "getFullImage").is(testImageAsset("big")),
            accessor(Highlight.class, ImageInfo.class, "getThumbnailImage").is(testImageAsset("small")),
            accessor(Highlight.class, String.class, "getTitle").is("my-title"),
            accessor(Highlight.class, HtmlString.class, "getBody").is(html("my-body")),
            iterableAccessor(Highlight.class, String.class, "getExplanation").is(withExplanation ? some("my-explanation") : none(String.class))
        );
    }

    private static Matcher<ImageInfo> testImageAsset()
    {
        return testImageAsset("image");
    }

    private static Matcher<ImageInfo> testImageAsset(String name)
    {
        return allOf(
            accessor(ImageInfo.class, URI.class, "getImageUri").is(URI.create("/" + name)),
            imageUriWithParams(ImageInfo.Size.SMALL_SIZE, ImageInfo.Resolution.DEFAULT_RESOLUTION).is(none(URI.class)),
            imageUriWithParams(ImageInfo.Size.DEFAULT_SIZE, ImageInfo.Resolution.HIGH_RESOLUTION).is(some(URI.create("/" + name + "/hi"))),
            imageUriWithParams(ImageInfo.Size.SMALL_SIZE, ImageInfo.Resolution.HIGH_RESOLUTION).is(none(URI.class))
        );
    }

    private static Matcher<Screenshot> testScreenshot()
    {
        return testScreenshot(false);
    }

    private static Matcher<Screenshot> testScreenshot(boolean withCaption)
    {
        return allOf(
            accessor(Screenshot.class, ImageInfo.class, "getImage").is(testImageAsset()),
            iterableAccessor(Screenshot.class, String.class, "getCaption").is(withCaption ? some("my-caption") : none(String.class))
        );
    }

    private static NamedFunction<ImageInfo, Option<URI>> imageUriWithParams(final ImageInfo.Size size, final ImageInfo.Resolution resolution)
    {
        return namedFunction("imageUriWithParams(" + size + "," + resolution + ")",
                new Function<ImageInfo, Option<URI>>()
                {
                    public Option<URI> apply(ImageInfo i)
                    {
                        return i.getImageUri(size, resolution);
                    }
                });
    }

    private static Matcher<Product> testProduct()
    {
        return allOf(
            accessor(Product.class, String.class, "getKey").is("my-key"),
            accessor(Product.class, String.class, "getName").is("my-name"),
            accessor(Product.class, String.class, "getSummary").is("my-summary"),
            iterableAccessor(Product.class, ImageInfo.class, "getLogo").is(contains(testImageAsset())),
            iterableAccessor(Product.class, ProductVersion.class, "getVersion").is(contains(testProductVersion()))
        );        
    }
    
    @SuppressWarnings("unchecked")
    private static Matcher<ProductVersion> testProductVersion()
    {
        return allOf(
            accessor(ProductVersion.class, String.class, "getName").is("1.0"),
            accessor(ProductVersion.class, int.class, "getBuildNumber").is(100),
            accessor(ProductVersion.class, PaymentModel.class, "getPaymentModel").is(PaymentModel.PAID_VIA_ATLASSIAN),
            iterableAccessor(ProductVersion.class, URI.class, "getArtifactUri").is(contains(URI.create("/binary"))),
            iterableAccessor(ProductVersion.class, URI.class, "getLearnMoreUri").is(contains(URI.create("/learn-more"))),
            accessor(ProductVersion.class, LocalDate.class, "getReleaseDate").is(new LocalDate(2012, 04, 01)),
            iterableAccessor(ProductVersion.class, VersionCompatibility.class, "getCompatibilities").is(contains(testVersionCompatibility()))
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<Vendor> testVendor()
    {
        return allOf(
            accessor(Vendor.class, URI.class, "getSelfUri").is(URI.create("/rest/vendors/1")),
            accessor(Vendor.class, URI.class, "getAlternateUri").is(URI.create("http://marketplace.atlassian.com/vendors/1")),
            accessor(Vendor.class, String.class, "getName").is("my-name"),
            iterableAccessor(Vendor.class, String.class, "getDescription").is(some("desc")),
            accessor(Vendor.class, String.class, "getEmail").is("test@example.com"),
            iterableAccessor(Vendor.class, String.class, "getPhone").is(some("phone")),
            iterableAccessor(Vendor.class, ImageInfo.class, "getLogo").is(contains(testImageAsset())),
            vendorUri(VendorExternalLinkType.HOME_PAGE).is(some(URI.create("http://home"))),
            vendorUri(VendorExternalLinkType.SLA).is(some(URI.create("http://sla"))),
            iterableAccessor(Vendor.class, String.class, "otherContactDetails").is(some("contact")),
            accessor(Vendor.class, boolean.class, "isVerified").is(true),
            accessor(Vendor.class, SupportDetails.class, "supportDetails").is(testVendorSupportDetails())
        );
    }

    private static Matcher<SupportDetails> testVendorSupportDetails() {
        return allOf(
                iterableAccessor(SupportDetails.class, SupportOrganization.class, "getSupportOrg").is(contains(testVendorSupportOrganization()))
        );
    }

    private static Matcher<SupportOrganization> testVendorSupportOrganization() {
        return allOf(
                accessor(SupportOrganization.class, String.class, "getName").is("support"),
                iterableAccessor(SupportOrganization.class, String.class, "getSupportEmail").is(some("support@foo.com")),
                iterableAccessor(SupportOrganization.class, URI.class, "getSupportUrl").is(some(URI.create("http://foo.com/support"))),
                iterableAccessor(SupportOrganization.class, String.class, "getSupportPhone").is(some("000-111-2222"))
        );
    }

    private static NamedFunction<Vendor, Option<URI>> vendorUri(final VendorExternalLinkType type)
    {
        return NamedFunction.namedFunction(type.getKey() + "Uri", new Function<Vendor, Option<URI>>()
        {
            public Option<URI> apply(Vendor v)
            {
                return v.getExternalLinkUri(type);
            }
        });
    }
    
    private static Matcher<VendorSummary> testVendorSummary()
    {
        return allOf(
            accessor(VendorSummary.class, URI.class, "getSelfUri").is(URI.create("/rest/vendors/1")),
            accessor(VendorSummary.class, URI.class, "getAlternateUri").is(URI.create("http://marketplace.atlassian.com/vendors/1")),
            accessor(VendorSummary.class, String.class, "getName").is("my-name"),
            iterableAccessor(VendorSummary.class, ImageInfo.class, "getLogo").is(contains(testImageAsset()))
        );
    }
    
    private static Matcher<VersionCompatibility> testVersionCompatibility()
    {
        return allOf(
            accessor(VersionCompatibility.class, ApplicationKey.class, "getApplication").is(ApplicationKey.JIRA),
            iterableAccessor(VersionCompatibility.class, Integer.class, "getServerMinBuild").is(contains(100)),
            iterableAccessor(VersionCompatibility.class, Integer.class, "getServerMaxBuild").is(contains(200)),
            accessor(VersionCompatibility.class, boolean.class, "isCloudCompatible").is(true)
        );
    }

    private static Matcher<Links> hasLink(final String rel, String href)
    {
        return NamedFunction.namedFunction("getLink(" + rel + ")", new Function<Links, Option<URI>>()
        {
            public Option<URI> apply(Links input)
            {
                return input.getUri(rel);
            }
        }).is(some(URI.create(href)));
    }

    private static Matcher<Links> hasLinkArray(final String rel, String... href)
    {
        return NamedFunction.namedFunction("getLinks(" + rel + ")", new Function<Links, Iterable<String>>()
        {
            public Iterable<String> apply(Links input)
            {
                return Iterables.transform(input.getLinks(rel), new Function<Link, String>()
                {
                    public String apply(Link input)
                    {
                        return input.stringValue();
                    }
                });
            }
        }).is(contains(href));
    }

    private static Matcher<Links> hasLinkTemplate(final String rel, String template)
    {
        return NamedFunction.namedFunction("getLink(" + rel + ")", new Function<Links, Option<UriTemplate>>()
        {
            public Option<UriTemplate> apply(Links input)
            {
                return input.getUriTemplate(rel);
            }
        }).is(some(UriTemplate.create(template)));
    }

    private <T> T decode(String filename, Class<T> type) throws Exception
    {
        return encoding.decode(new ByteArrayInputStream(readStringResource(filename + ".json").getBytes()), type);
    }
    
    private String readStringResource(String filename) throws Exception
    {
        InputStream is = getClass().getClassLoader().getResourceAsStream(filename);
        if (is == null)
        {
            throw new IllegalStateException("missing test resource file " + filename);
        }
        String s = IOUtils.toString(is);
        Pattern include = Pattern.compile("\"#include\\(([^)]*)\\)\"");
        while (true)
        {
            java.util.regex.Matcher m = include.matcher(s);
            if (!m.find())
            {
                return s;
            }
            s = m.replaceFirst(readStringResource(m.group(1)));
        }
    }
    
    private String encode(Object entity) throws Exception
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        encoding.encode(os, entity, false);
        return os.toString();
    }
    
    private String encodeChanges(Object e1, Object e2) throws Exception
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        encoding.encodeChanges(os, e1, e2);
        return os.toString();
    }
}
