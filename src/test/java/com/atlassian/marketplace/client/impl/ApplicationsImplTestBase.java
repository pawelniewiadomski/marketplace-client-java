package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.atlassian.marketplace.client.model.Links;

import com.google.common.collect.ImmutableList;

import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.TestModelBuilders.application;
import static com.atlassian.marketplace.client.model.TestModelBuilders.applicationVersion;

public interface ApplicationsImplTestBase
{
    static final String FAKE_APP_BY_KEY_PATH = "/fake/applications/";
    static final String FAKE_APP_LATEST_PATH = "/fake/applications/latest/";
    static final String FAKE_APP_VER_PATH = "/fake/applications/version/";
    static final String FAKE_APP_VERSIONS_PATH = "/fake/applications/versions";
    static final Links APP_LINKS =
        links().put("self", URI.create(FAKE_APP_BY_KEY_PATH))
               .putTemplate("versions", FAKE_APP_VERSIONS_PATH)
               .put("latestVersion", URI.create(FAKE_APP_LATEST_PATH))
               .putTemplate("versionByBuild", FAKE_APP_VER_PATH + "build/{buildNumber}")
               .putTemplate("versionByName", FAKE_APP_VER_PATH + "name/{name}")
               .build();
    static final Application APP_REP = application().links(APP_LINKS).build();
    static final ApplicationVersion APP_VER_REP = applicationVersion().build();
    static final Links APPS_LINKS =
        links().putTemplate("byKey", FAKE_APP_BY_KEY_PATH + "{applicationKey}")
               .build();
    static final InternalModel.Applications APPS_REP =
        InternalModel.applications(APPS_LINKS, ImmutableList.of(APP_REP));
}
