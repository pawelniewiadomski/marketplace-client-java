package com.atlassian.marketplace.client.impl;

import java.net.URI;
import java.util.Arrays;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonQuery;
import com.atlassian.marketplace.client.api.AddonQuery.View;
import com.atlassian.marketplace.client.api.Cost;
import com.atlassian.marketplace.client.api.HostingType;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.PricingType;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonPricing;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.TestObjects.HOST_BASE;
import static com.atlassian.marketplace.client.TestObjects.LINK_NEXT_URI;
import static com.atlassian.marketplace.client.TestObjects.LINK_PREV_URI;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.impl.ClientTester.FAKE_ADDONS_PATH;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addon;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonPricing;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.verify;

public class AddonsImplTest extends AddonsImplTestBase<Addon>
{
    @Test
    public void addonsAccessorQueriesRootResource() throws Exception
    {
        tester.client.addons();
        
        verify(tester.httpTransport).get(tester.apiBase);
    }
    
    @Test(expected=MpacException.class)
    public void addonsAccessorThrowsExceptionIfRootResourceNotAvailable() throws Exception
    {
        tester.mockResourceError(tester.apiBase, 404);
        tester.client.addons();
    }

    @Test
    public void addonsAccessorReturnsNonNullObject() throws Exception
    {
        assertThat(tester.client.addons(), not(nullValue()));
    }

    @Test
    public void getByKeyUsesAddonsResource() throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x"));
        tester.client.addons().getByKey("x", AddonQuery.any());
        verify(tester.httpTransport).get(URI.create(HOST_BASE + FAKE_ADDONS_PATH + "?limit=0"));
    }

    @Test
    public void getByKeyUsesAddonByKeyResource() throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x"));
        assertThat(tester.client.addons().getByKey("x", AddonQuery.any()),
                contains(ADDON_REP));
    }

    @Test
    public void getByKeyPassesAppKeyInQueryString() throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x").queryParam("application", "jira"));        
        assertThat(tester.client.addons().getByKey("x", AddonQuery.builder().application(some(JIRA)).build()),
                contains(ADDON_REP));
    }

    @Test
    public void getByKeyPassesAccessTokenInQueryString() throws Exception
    {
        setupAddonByKeyResource(addonByKeyUri("x").queryParam("accessToken", "x"));        
        assertThat(tester.client.addons().getByKey("x", AddonQuery.builder().accessToken(some("x")).build()),
                contains(ADDON_REP));
    }

    @Test
    public void findUsesAddonsResource() throws Exception
    {
        setupAddonsResource(addonsApi());
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()), contains(ADDON_SUMMARY_REP));
    }

    @Test
    public void findReturnsPageWithCorrectSize() throws Exception
    {
        setupAddonsResource(addonsApi());
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()).size(), equalTo(1));
    }

    @Test
    public void findReturnsPageWithCorrectTotalSize() throws Exception
    {
        setupAddonsResource(addonsApi());
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()).totalSize(), equalTo(2));
    }

    @Test
    public void findReturnsPageWithReference() throws Exception
    {
        QueryBounds b = QueryBounds.limit(some(5));
        UriBuilder uri = addonsApi().queryParam("limit", 5);
        setupAddonsResource(uri, ADDONS_REP);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().bounds(b).build()).getReference(),
                   equalTo(some(new PageReference<AddonSummary>(uri.build(), b, PageReader.<AddonSummary>stub()))));
    }
    
    @Test
    public void findReturnsPageWithNoNextReferenceIfNoNextLink() throws Exception
    {
        setupAddonsResource(addonsApi(), ADDONS_REP_WITH_PREV);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()).getNext(),
                   equalTo(Option.<PageReference<AddonSummary>>none()));
    }

    @Test
    public void findReturnsPageWithNextReferenceIfNextLink() throws Exception
    {
        QueryBounds b = QueryBounds.limit(some(5));
        UriBuilder uri = addonsApi().queryParam("limit", 5);
        setupAddonsResource(uri, ADDONS_REP_WITH_NEXT);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().bounds(b).build()).getNext(),
                   equalTo(some(new PageReference<AddonSummary>(LINK_NEXT_URI, b.withOffset(5), PageReader.<AddonSummary>stub()))));
    }

    @Test
    public void findReturnsPageWithNextReferenceUsingSizeAsLimitIfLimitOmitted() throws Exception
    {
        setupAddonsResource(addonsApi(), ADDONS_REP_WITH_NEXT);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()).getNext(),
                   equalTo(some(new PageReference<AddonSummary>(LINK_NEXT_URI, QueryBounds.offset(1).withLimit(some(1)),
                       PageReader.<AddonSummary>stub()))));
    }

    @Test
    public void findReturnsPageWithNoPrevReferenceIfNoPrevLink() throws Exception
    {
        setupAddonsResource(addonsApi(), ADDONS_REP_WITH_NEXT);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().build()).getPrevious(),
                   equalTo(Option.<PageReference<AddonSummary>>none()));
    }

    @Test
    public void findReturnsPageWithPrevReferenceIfPrevLink() throws Exception
    {
        QueryBounds b = QueryBounds.offset(8).withLimit(some(5));
        UriBuilder uri = addonsApi().queryParam("offset", 8).queryParam("limit", 5);
        setupAddonsResource(uri, ADDONS_REP_WITH_PREV);
        
        assertThat(tester.client.addons().find(AddonQuery.builder().bounds(b).build()).getPrevious(),
                   equalTo(some(new PageReference<AddonSummary>(LINK_PREV_URI, b.withOffset(3), PageReader.<AddonSummary>stub()))));
    }

    private void testFindParams(UriBuilder uriWithParams, AddonQuery.Builder queryWithParams) throws Exception
    {
        setupAddonsResource(uriWithParams);
        assertThat(tester.client.addons().find(queryWithParams.build()), contains(ADDON_SUMMARY_REP));
    }

    @Test
    public void findPassesOffsetInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("offset", "5"), AddonQuery.builder().bounds(QueryBounds.offset(5)));
    }
    
    @Test
    public void findPassesLimitInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("limit", "10"), AddonQuery.builder().bounds(QueryBounds.limit(some(10))));
    }

    @Test
    public void findPassesAppKeyInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("application", "jira"), AddonQuery.builder().application(some(JIRA)));
    }

    @Test
    public void findPassesAppBuildNumberInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("application", "jira").queryParam("applicationBuild", "123"),
                AddonQuery.builder().application(some(JIRA)).appBuildNumber(some(123)));
    }

    @Test
    public void findPassesCategoriesInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("category", "foo", "bar"),
                AddonQuery.builder().categoryNames(ImmutableList.of("foo", "bar")));
    }
    
    @Test
    public void findPassesCostInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("cost", "paid"),
                AddonQuery.builder().cost(some(Cost.ALL_PAID)));
    }

    @Test
    public void findPassesViewFilterInQueryString() throws Exception
    {
        for (View v : View.values())
        {
            testFindParams(addonsApi().queryParam("filter", v.getKey()), AddonQuery.builder().view(some(v)));
        }
    }
    
    @Test
    public void findPassesVersionFlagInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("withVersion", "true"), AddonQuery.builder().withVersion(true));
    }

    @Test
    public void findPassesForThisUserFlagInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("forThisUser", "true"), AddonQuery.builder().forThisUserOnly(true));
    }
    
    @Test
    public void findPassesHostingInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("hosting", "cloud"), AddonQuery.builder().hosting(some(HostingType.CLOUD)));
    }

    @Test
    public void findPassesIncludeHiddenInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("includeHidden", "all"), AddonQuery.builder().includeHidden(some(AddonQuery.IncludeHiddenType.ALL)));
    }
    
    @Test
    public void findPassesIncludePrivateFlagInQueryString() throws Exception
    {
        testFindParams(addonsApi().queryParam("includePrivate", "true"), AddonQuery.builder().includePrivate(true));
    }
    
    @Test
    public void getPricingUsesPricingUri() throws Exception
    {
        URI pricingUri = tester.apiBase.resolve(
            FAKE_PRICING_TEMPLATE.replace("{cloudOrServer}", "server").replace("{liveOrPending}", "live"));
        AddonPricing pricing = addonPricing().build();
        setupAddonByKeyResource(addonByKeyUri("x"));
        tester.mockResource(pricingUri, pricing);
        assertThat(tester.client.addons().getPricing("x", PricingType.SERVER), contains(sameInstance(pricing)));
    }
    
    @Test
    public void findBannersUsesAddonsResource() throws Exception
    {
        setupAddonBannersResource(bannersApi());
        
        assertThat(tester.client.addons().findBanners(AddonQuery.any()), contains(ADDON_REF_REP));
    }

    @Test
    public void findBannersReturnsPageWithCorrectSize() throws Exception
    {
        setupAddonBannersResource(bannersApi());
        
        assertThat(tester.client.addons().findBanners(AddonQuery.any()).size(), equalTo(1));
    }

    @Test
    public void findBannersReturnsPageWithCorrectTotalSize() throws Exception
    {
        setupAddonBannersResource(bannersApi());
        
        assertThat(tester.client.addons().findBanners(AddonQuery.any()).totalSize(), equalTo(2));
    }

    @Test
    public void findRecommendationsReturnsAddons() throws Exception
    {
        setupRecommendationsResource("x", recommendationsUri());
        
        assertThat(tester.client.addons().findRecommendations("x", AddonQuery.any()),
            contains(ADDON_REF_REP));
    }

    @Test
    public void findRecommendationsReturnsPageWithCorrectTotalSize() throws Exception
    {
        setupRecommendationsResource("x", recommendationsUri());
        
        assertThat(tester.client.addons().findRecommendations("x", AddonQuery.any()).totalSize(),
            equalTo(2));
    }

    @Test
    public void findRecommendationsPassesQueryParameters() throws Exception
    {
        // Since we use the same code for encoding all AddonQuery parameters in general, we'll just verify that this
        // resource can pass any of those parameters.
        setupRecommendationsResource("x", recommendationsUri().queryParam("application", "jira"));
        
        assertThat(tester.client.addons().findRecommendations("x", AddonQuery.builder().application(some(JIRA)).build()),
            contains(ADDON_REF_REP));
    }
    
    @Test
    public void claimAccessTokenReturnsTrueIfPostIsSuccessful() throws Exception
    {
        setupClaimTokenResource("x", "t", 204);
        
        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(true));
    }

    @Test
    public void claimAccessTokenReturnsFalseIfPostReturns400() throws Exception
    {
        setupClaimTokenResource("x", "t", 400);
        
        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Test
    public void claimAccessTokenReturnsFalseIfPostReturns403() throws Exception
    {
        setupClaimTokenResource("x", "t", 403);
        
        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Test
    public void claimAccessTokenReturnsFalseIfPostReturns404() throws Exception
    {
        setupClaimTokenResource("x", "t", 404);
        
        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Test(expected = MpacException.ServerError.class)
    public void claimAccessTokenThrowsExceptionIfPostReturns500() throws Exception
    {
        setupClaimTokenResource("x", "t", 500);
        tester.client.addons().claimAccessToken("x", "t");
    }

    @Test(expected = MpacException.ServerError.class)
    public void claimAccessTokenThrowsExceptionIfGetAddonByKeyReturns500() throws Exception
    {
        setupClaimTokenAddonByKeyResourceError("x", "t", 500);
        tester.client.addons().claimAccessToken("x", "t");
    }

    @Test
    public void claimAccessTokenReturnsFalseIfGetAddonByKeyReturns400() throws Exception
    {
        setupClaimTokenAddonByKeyResourceError("x", "t", 400);

        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Test
    public void claimAccessTokenReturnsFalseIfGetAddonByKeyReturns403() throws Exception
    {
        setupClaimTokenAddonByKeyResourceError("x", "t", 403);

        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Test
    public void claimAccessTokenReturnsFalseIfGetAddonByKeyReturns404() throws Exception
    {
        setupClaimTokenAddonByKeyResourceError("x", "t", 404);

        assertThat(tester.client.addons().claimAccessToken("x", "t"), equalTo(false));
    }

    @Override
    protected URI getCollectionUri()
    {
        return addonsApi().build();
    }
    
    @Override
    protected Addon makeEntityToCreate()
    {
        return addon().build();
    }
    
    @Override
    protected Addon makeEntityFromServer()
    {
        return addon().links(ADDON_LINKS).build();
    }
    
    @Override
    protected Addon makeEntityWithUpdates(Addon from)
    {
        return addon(from).build();
    }
    
    @Override
    protected Addon createEntity(Addon entity) throws MpacException
    {
        return tester.client.addons().createAddon(entity);
    }
    
    @Override
    protected Addon updateEntity(Addon original, Addon updated) throws MpacException
    {
        return tester.client.addons().updateAddon(original, updated);
    }
    
    @Override
    protected URI transformEntityUriAfterCreate(URI uri)
    {
        return UriBuilder.fromUri(uri).queryParam("withVersion", true).build();
    }
}
