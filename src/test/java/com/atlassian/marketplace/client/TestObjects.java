package com.atlassian.marketplace.client;

import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.model.Links;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import static com.atlassian.marketplace.client.model.ModelBuilders.links;

public abstract class TestObjects
{
    public static final String HOST_BASE = "http://fake.marketplace";
    public static final String API_V1_BASE_PATH = "/rest/1.0";
    public static final String API_V2_BASE_PATH = "/rest/2";
    
    public static final URI BASE_URI = URI.create(HOST_BASE + "/");
    public static final URI BASE_URI_WITHOUT_SLASH = URI.create(HOST_BASE);

    public static final Links EMPTY_LINKS = links().build();
    public static final URI LINK_NEXT_URI = URI.create("/next/uri");
    public static final URI LINK_PREV_URI = URI.create("/prev/uri");
    public static final Links LINKS_WITH_NEXT = links().put("next", LINK_NEXT_URI).build();
    public static final Links LINKS_WITH_PREV = links().put("prev", LINK_PREV_URI).build();

    public static final String PLUGIN_KEY = "a-plugin-key";
    public static final VendorId VENDOR_ID = VendorId.fromUri(URI.create("/vendor"));
        
    public static Date utcDate(int year, int month, int day, int hour, int minute, int second)
    {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, second);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static DateTime utcDateTime(int year, int month, int day, int hour, int minute, int second)
    {
        return new DateTime(year, month, day, hour, minute, second, 0, DateTimeZone.UTC);
    }
}
