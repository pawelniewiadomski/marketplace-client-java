package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.AddonCategoryId;
import com.atlassian.marketplace.client.api.AddonExternalLinkType;
import com.atlassian.marketplace.client.api.AddonVersionExternalLinkType;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.LicenseTypeId;
import com.atlassian.marketplace.client.api.VendorExternalLinkType;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.TestObjects.VENDOR_ID;
import static com.atlassian.marketplace.client.api.AddonExternalLinkType.ISSUE_TRACKER;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.model.HtmlString.html;
import static com.atlassian.marketplace.client.model.ModelBuilders.addon;
import static com.atlassian.marketplace.client.model.ModelBuilders.addonVersion;
import static com.atlassian.marketplace.client.model.ModelBuilders.address;
import static com.atlassian.marketplace.client.model.ModelBuilders.applicationVersion;
import static com.atlassian.marketplace.client.model.ModelBuilders.highlight;
import static com.atlassian.marketplace.client.model.ModelBuilders.links;
import static com.atlassian.marketplace.client.model.ModelBuilders.screenshot;
import static com.atlassian.marketplace.client.model.ModelBuilders.vendor;
import static com.atlassian.marketplace.client.model.ModelBuilders.versionCompatibilityForServer;
import static com.atlassian.marketplace.client.model.PaymentModel.FREE;
import static com.atlassian.marketplace.client.model.TestModelBuilders.DEFAULT_LOCAL_DATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class ModelBuildersTest
{
    public static final ImageId DEFAULT_IMAGE = ImageId.fromUri(URI.create("/image"));
    public static final URI TEST_URI = URI.create("xyz");
    
    private static ModelBuilders.AddonBuilder minimalAddon()
    {
        return addon().key("key").name("name").status(AddonStatus.PUBLIC).vendor(VENDOR_ID);
    }
    
    private static ModelBuilders.AddonVersionBuilder minimalAddonVersion()
    {
        return addonVersion().buildNumber(1).paymentModel(PaymentModel.FREE)
                .releaseDate(DEFAULT_LOCAL_DATE)
                .status(AddonVersionStatus.PUBLIC);
    }

    private static ModelBuilders.ApplicationVersionBuilder minimalApplicationVersion()
    {
        return applicationVersion().name("1.0").buildNumber(1).releaseDate(DEFAULT_LOCAL_DATE).status(ApplicationVersionStatus.PUBLISHED);
    }

    private static ModelBuilders.VendorBuilder minimalVendor()
    {
        return vendor().name("name").email("email");
    }
    
    // The following tests verify that the defaults provided by these builders don't
    // violate any preconditions in the model object constructors which would throw an
    // IllegalArgumentException or NullPointerException - and that the builders for objects
    // we can send to the server do *not* provide defaults for required properties.

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonRequiresKey() throws Exception
    {
        ModelBuilders.addon()
            .name("name")
            .status(AddonStatus.PUBLIC)
            .vendor(VENDOR_ID)
            .build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonRequiresName() throws Exception
    {
        ModelBuilders.addon()
            .key("key")
            .status(AddonStatus.PUBLIC)
            .vendor(VENDOR_ID)
            .build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonRequiresStatus() throws Exception
    {
        ModelBuilders.addon()
            .key("key")
            .name("name")
            .vendor(VENDOR_ID)
            .build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonRequiresVendor() throws Exception
    {
        ModelBuilders.addon()
            .key("key")
            .name("name")
            .status(AddonStatus.PUBLIC)
            .build();
    }
    
    @Test
    public void canSetAddonBanner() throws Exception
    {
        Addon a = minimalAddon().banner(some(DEFAULT_IMAGE)).build();
        assertThat(a.getLinks().getUri("banner"), equalTo(some(DEFAULT_IMAGE.getUri())));
    }

    @Test
    public void canSetAddonCategories() throws Exception
    {
        AddonCategoryId c1 = AddonCategoryId.fromUri(URI.create("/c1"));
        AddonCategoryId c2 = AddonCategoryId.fromUri(URI.create("/c2"));
        Addon a = minimalAddon().categories(ImmutableList.of(c1, c2)).build();
        assertThat(a.getCategoryIds(), contains(c1, c2));
    }
    
    @Test
    public void canSetAddonEnableAtlassianAnswers() throws Exception
    {
        Addon a = minimalAddon().enableAtlassianAnswers(false).build();
        assertThat(a.isEnableAtlassianAnswers(), equalTo(some(false)));
    }
    
    @SuppressWarnings("deprecation")
    @Test
    public void canSetAddonExternalLinks() throws Exception
    {
        ImmutableList.Builder<URI> uris = ImmutableList.builder();
        ImmutableList.Builder<AddonExternalLinkType> notAllowedTypes = ImmutableList.builder();
        ModelBuilders.AddonBuilder ab = minimalAddon();
        for (AddonExternalLinkType type: AddonExternalLinkType.values())
        {
            try
            {
                URI uri = URI.create("/link-" + type.getKey());
                ab.externalLinkUri(type, some(uri));
                uris.add(uri);
            }
            catch (IllegalArgumentException e)
            {
                notAllowedTypes.add(type);
            }
        }
        Addon a = ab.build();
        ImmutableList.Builder<URI> uris1 = ImmutableList.builder();
        for (AddonExternalLinkType type: AddonExternalLinkType.values())
        {
            uris1.addAll(a.getExternalLinkUri(type));
        }
        assertThat(uris1.build(), equalTo(uris.build()));
        assertThat(notAllowedTypes.build(),
                contains(AddonExternalLinkType.BUILDS, AddonExternalLinkType.SOURCE, AddonExternalLinkType.WIKI));
    }

    @Test
    public void canSetAddonKey() throws Exception
    {
        Addon a = minimalAddon().key("abc").build();
        assertThat(a.getKey(), equalTo("abc"));
    }
    
    @Test
    public void canSetAddonLinkForIssueTracker() throws Exception
    {
        Addon a = minimalAddon().externalLinkUri(ISSUE_TRACKER, some(TEST_URI)).build();
        assertThat(a.getExternalLinkUri(ISSUE_TRACKER), equalTo(some(TEST_URI)));
    }
    
    @Test
    public void canSetAddonLogo() throws Exception
    {
        Addon a = minimalAddon().logo(some(DEFAULT_IMAGE)).build();
        assertThat(a.getLinks().getUri("logo"), equalTo(some(DEFAULT_IMAGE.getUri())));
    }

    @Test
    public void canSetAddonName() throws Exception
    {
        Addon a = minimalAddon().name("abc").build();
        assertThat(a.getName(), equalTo("abc"));
    }

    @Test
    public void canSetAddonStatus() throws Exception
    {
        Addon a = minimalAddon().status(AddonStatus.PUBLIC).build();
        assertThat(a.getStatus(), equalTo(AddonStatus.PUBLIC));
    }
    
    @Test
    public void canSetAddonSummary() throws Exception
    {
        Addon a = minimalAddon().summary(some("abc")).build();
        assertThat(a.getSummary(), equalTo(some("abc")));
    }
    
    @Test
    public void canSetAddonVersion() throws Exception
    {
        AddonVersion v = minimalAddonVersion().build();
        Addon a = minimalAddon().version(some(v)).build();
        assertThat(a.getVersion(), equalTo(some(v)));
    }
    
    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonVersionRequiresBuildNumber() throws Exception
    {
        addonVersion().paymentModel(FREE).releaseDate(DEFAULT_LOCAL_DATE).status(AddonVersionStatus.PRIVATE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonVersionRequiresPaymentModel() throws Exception
    {
        addonVersion().buildNumber(1).releaseDate(DEFAULT_LOCAL_DATE).status(AddonVersionStatus.PRIVATE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonVersionRequiresReleaseDate() throws Exception
    {
        addonVersion().buildNumber(1).paymentModel(FREE).status(AddonVersionStatus.PRIVATE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void addonVersionRequiresStatus() throws Exception
    {
        addonVersion().buildNumber(1).paymentModel(FREE).releaseDate(DEFAULT_LOCAL_DATE).build();
    }

    @Test
    public void canSetAddonVersionArtifact() throws Exception
    {
        AddonVersion v = minimalAddonVersion().artifact(some(ArtifactId.fromUri(TEST_URI))).build();
        assertThat(v.getLinks().getUri("artifact"), equalTo(some(TEST_URI)));
    }
    
    @Test
    public void canSetAddonVersionBeta() throws Exception
    {
        assertThat(minimalAddonVersion().beta(true).build().isBeta(), equalTo(true));
    }
    
    @Test
    public void canSetAddonVersionBuildNumber() throws Exception
    {
        AddonVersion v = minimalAddonVersion().buildNumber(1000).build();
        assertThat(v.getBuildNumber(), equalTo(1000L));
    }

    @Test
    public void canSetAddonVersionCompatibilities() throws Exception
    {
        VersionCompatibility vc = versionCompatibilityForServer(JIRA, 1, 2);
        AddonVersion v = minimalAddonVersion().compatibilities(ImmutableList.of(vc)).build();
        assertThat(v.getCompatibilities(), contains(vc));
    }
    
    @Test
    public void addonVersionCompatibilitiesCompletelyOmittedIfNotSet() throws Exception
    {
        AddonVersion v = minimalAddonVersion().build();
        assertThat(v.getCompatibilitiesIfSpecified(), equalTo(Option.<Iterable<VersionCompatibility>>none()));
    }
    
    @Test
    public void canSetAddonVersionDeployable() throws Exception
    {
        AddonVersion v = minimalAddonVersion().deployable(true).build();
        assertThat(v.isDeployable(), equalTo(true));
    }

    @SuppressWarnings("deprecation")
    @Test
    public void canSetAddonVersionExternalLinks() throws Exception
    {
        ImmutableList.Builder<URI> uris = ImmutableList.builder();
        ImmutableList.Builder<AddonVersionExternalLinkType> notAllowedTypes = ImmutableList.builder();
        ModelBuilders.AddonVersionBuilder vb = minimalAddonVersion();
        for (AddonVersionExternalLinkType type: AddonVersionExternalLinkType.values())
        {
            try
            {
                URI uri = URI.create("/link-" + type.getKey());
                vb.externalLinkUri(type, some(uri));
                uris.add(uri);
            }
            catch (IllegalArgumentException e)
            {
                notAllowedTypes.add(type);
            }
        }
        AddonVersion v = vb.build();
        ImmutableList.Builder<URI> uris1 = ImmutableList.builder();
        for (AddonVersionExternalLinkType type: AddonVersionExternalLinkType.values())
        {
            uris1.addAll(v.getExternalLinkUri(type));
        }
        assertThat(uris1.build(), equalTo(uris.build()));
        assertThat(notAllowedTypes.build(),
                contains(AddonVersionExternalLinkType.DONATE, AddonVersionExternalLinkType.EVALUATION_LICENSE,
                        AddonVersionExternalLinkType.JAVADOC, AddonVersionExternalLinkType.SOURCE));
    }

    @Test
    public void canSetAddonVersionHighlights() throws Exception
    {
        Highlight h1 = highlight().fullImage(DEFAULT_IMAGE).thumbnailImage(DEFAULT_IMAGE).title("h1").body(html("b1")).build();
        Highlight h2 = highlight().fullImage(DEFAULT_IMAGE).thumbnailImage(DEFAULT_IMAGE).title("h2").body(html("b2")).build();
        AddonVersion v = minimalAddonVersion().highlights(ImmutableList.of(h1, h2)).build();
        assertThat(v.getHighlights(), contains(h1, h2));
    }

    @Test
    public void addonVersionHighlightsCompletelyOmittedIfNotSet() throws Exception
    {
        AddonVersion v = minimalAddonVersion().build();
        assertThat(v.getHighlightsIfSpecified(), equalTo(Option.<Iterable<Highlight>>none()));
    }

    @Test
    public void canSetAddonVersionLicenseTypeId() throws Exception
    {
        AddonVersion v = minimalAddonVersion().licenseTypeId(some(LicenseTypeId.fromUri(TEST_URI))).build();
        assertThat(v.getLicenseTypeId(), equalTo(some(LicenseTypeId.fromUri(TEST_URI))));
    }
    
    @Test
    public void canSetAddonVersionMoreDetails() throws Exception
    {
        AddonVersion v = minimalAddonVersion().moreDetails(some(html("x"))).build();
        assertThat(v.getMoreDetails(), equalTo(some(html("x"))));
    }
    
    @Test
    public void canSetAddonVersionName() throws Exception
    {
        AddonVersion v = minimalAddonVersion().name("1.0").build();
        assertThat(v.getName(), equalTo(some("1.0")));
    }

    @Test
    public void canSetAddonVersionReleaseDate() throws Exception
    {
        assertThat(minimalAddonVersion().releaseDate(DEFAULT_LOCAL_DATE).build().getReleaseDate(), equalTo(DEFAULT_LOCAL_DATE));
    }

    @Test
    public void canSetAddonVersionReleasedBy() throws Exception
    {
        assertThat(minimalAddonVersion().releasedBy(some("x")).build().getReleasedBy(),
                equalTo(some("x")));
    }
    
    @Test
    public void canSetAddonVersionReleaseNotes() throws Exception
    {
        AddonVersion v = minimalAddonVersion().releaseNotes(some(html("x"))).build();
        assertThat(v.getReleaseNotes(), equalTo(some(html("x"))));
    }

    @Test
    public void canSetAddonVersionReleaseSummary() throws Exception
    {
        AddonVersion v = minimalAddonVersion().releaseSummary(some("x")).build();
        assertThat(v.getReleaseSummary(), equalTo(some("x")));
    }

    @Test
    public void canSetAddonVersionScreenshots() throws Exception
    {
        Screenshot s1 = screenshot().image(DEFAULT_IMAGE).build();
        Screenshot s2 = screenshot().image(DEFAULT_IMAGE).build();
        AddonVersion v = minimalAddonVersion().screenshots(ImmutableList.of(s1, s2)).build();
        assertThat(v.getScreenshots(), contains(s1, s2));
    }

    @Test
    public void addonVersionScreenshotsCompletelyOmittedIfNotSet() throws Exception
    {
        AddonVersion v = minimalAddonVersion().build();
        assertThat(v.getScreenshotsIfSpecified(), equalTo(Option.<Iterable<Screenshot>>none()));
    }
    
    @Test
    public void canSetAddonVersionStatus() throws Exception
    {
        AddonVersion v = minimalAddonVersion().status(AddonVersionStatus.PUBLIC).build();
        assertThat(v.getStatus(), equalTo(AddonVersionStatus.PUBLIC));
    }

    @Test
    public void canSetAddonVersionAgreement() throws Exception
    {
        final URI agreementURI = URI.create("http://www.atlassian.com/licensing/marketplace/publisheragreement");
        AddonVersion v = minimalAddonVersion().agreement(agreementURI).build();
        assertThat(v.getLinks().getUri("agreement"), equalTo(Option.some(agreementURI)));
    }

    @Test
    public void canSetAddonVersionSupported() throws Exception
    {
        assertThat(minimalAddonVersion().supported(true).build().isSupported(), equalTo(true));
    }
    
    @Test
    public void canSetAddonVersionYoutubeId() throws Exception
    {
        AddonVersion v = minimalAddonVersion().youtubeId(some("x")).build();
        assertThat(v.getYoutubeId(), equalTo(some("x")));
    }
    
    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void applicationVersionRequiresName() throws Exception
    {
        applicationVersion().buildNumber(1).releaseDate(DEFAULT_LOCAL_DATE).status(ApplicationVersionStatus.PUBLISHED).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void applicationVersionRequiresBuildNumber() throws Exception
    {
        applicationVersion().name("1.0").releaseDate(DEFAULT_LOCAL_DATE).status(ApplicationVersionStatus.PUBLISHED).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void applicationVersionRequiresReleaseDate() throws Exception
    {
        applicationVersion().name("1.0").buildNumber(1).status(ApplicationVersionStatus.PUBLISHED).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void applicationVersionRequiresStatus() throws Exception
    {
        applicationVersion().name("1.0").buildNumber(1).releaseDate(DEFAULT_LOCAL_DATE).build();
    }
    
    @Test
    public void canSetApplicationVersionBuildNumber() throws Exception
    {
        assertThat(minimalApplicationVersion().buildNumber(2).build().getBuildNumber(), equalTo(2));
    }

    @Test
    public void canSetApplicationVersionName() throws Exception
    {
        assertThat(minimalApplicationVersion().name("x").build().getName(), equalTo("x"));
    }

    @Test
    public void canSetApplicationVersionReleaseDate() throws Exception
    {
        assertThat(minimalApplicationVersion().releaseDate(DEFAULT_LOCAL_DATE).build().getReleaseDate(), equalTo(DEFAULT_LOCAL_DATE));
    }

    @Test
    public void canSetApplicationVersionStatus() throws Exception
    {
        assertThat(minimalApplicationVersion().status(ApplicationVersionStatus.PUBLISHED).build().getStatus(),
                equalTo(ApplicationVersionStatus.PUBLISHED));
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void highlightRequiresTitle() throws Exception
    {
        highlight().body(html("b")).fullImage(DEFAULT_IMAGE).thumbnailImage(DEFAULT_IMAGE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void highlightRequiresBody() throws Exception
    {
        highlight().title("t").fullImage(DEFAULT_IMAGE).thumbnailImage(DEFAULT_IMAGE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void highlightRequiresFullImage() throws Exception
    {
        highlight().title("t").body(html("b")).thumbnailImage(DEFAULT_IMAGE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void highlightRequiresThumbnailImage() throws Exception
    {
        highlight().title("t").body(html("b")).fullImage(DEFAULT_IMAGE).build();
    }

    @Test
    public void canBuildHighlightWithAllRequiredProperties() throws Exception
    {
        highlight().title("t").body(html("b")).fullImage(DEFAULT_IMAGE).thumbnailImage(DEFAULT_IMAGE).build();
    }

    @Test
    public void canBuildLinksWithDefaults()
    {
        assertThat(links().build().getItems().size(), equalTo(0));
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void screenshotRequiresImage() throws Exception
    {
        screenshot().build();
    }
    
    @Test
    public void canBuildScreenshotWithAllRequiredProperties() throws Exception
    {
        screenshot().image(DEFAULT_IMAGE).build();
    }

    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void vendorRequiresEmail() throws Exception
    {
        vendor().name("name").build();
    }
    
    @Test(expected = ModelBuilders.InvalidModelException.class)
    public void vendorRequiresName() throws Exception
    {
        vendor().email("test@example.com").build();
    }

    @Test
    public void canSetVendorAddress() throws Exception
    {
        Address a = address().line1("x").build();
        assertThat(minimalVendor().address(some(a)).build().getAddress(), equalTo(some(a)));
    }
    
    @Test
    public void canSetVendorDescription() throws Exception
    {
        assertThat(minimalVendor().description(some("x")).build().getDescription(), equalTo(some("x")));
    }

    @Test
    public void canSetVendorEmail() throws Exception
    {
        assertThat(minimalVendor().email("x").build().getEmail(), equalTo("x"));
    }

    @Test
    public void canSetVendorLogo() throws Exception
    {
        assertThat(minimalVendor().logo(some(DEFAULT_IMAGE)).build().getLinks().getUri("logo"), equalTo(some(DEFAULT_IMAGE.getUri())));
    }

    @Test
    public void canSetVendorName() throws Exception
    {
        assertThat(minimalVendor().name("x").build().getName(), equalTo("x"));
    }

    @Test
    public void canSetVendorOtherContactDetails() throws Exception
    {
        assertThat(minimalVendor().otherContactDetails(some("x")).build().getOtherContactDetails(), equalTo(some("x")));
    }

    @Test
    public void canSetVendorPhone() throws Exception
    {
        assertThat(minimalVendor().phone(some("x")).build().getPhone(), equalTo(some("x")));
    }

    @Test
    public void canSetVendorExternalLinks() throws Exception
    {
        ImmutableList.Builder<URI> uris = ImmutableList.builder();
        ModelBuilders.VendorBuilder vb = minimalVendor();
        for (VendorExternalLinkType type: VendorExternalLinkType.values())
        {
            URI uri = URI.create("/link-" + type.getKey());
            vb.externalLinkUri(type, some(uri));
            uris.add(uri);
        }
        Vendor v = vb.build();
        ImmutableList.Builder<URI> uris1 = ImmutableList.builder();
        for (VendorExternalLinkType type: VendorExternalLinkType.values())
        {
            uris1.addAll(v.getExternalLinkUri(type));
        }
        assertThat(uris1.build(), equalTo(uris.build()));
    }
}
