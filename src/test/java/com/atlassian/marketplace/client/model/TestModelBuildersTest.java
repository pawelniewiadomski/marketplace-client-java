package com.atlassian.marketplace.client.model;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addon;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonCategorySummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonDistributionSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonReference;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonVersion;
import static com.atlassian.marketplace.client.model.TestModelBuilders.addonVersionSummary;
import static com.atlassian.marketplace.client.model.TestModelBuilders.application;
import static com.atlassian.marketplace.client.model.TestModelBuilders.applicationVersion;
import static com.atlassian.marketplace.client.model.TestModelBuilders.artifactInfo;
import static com.atlassian.marketplace.client.model.TestModelBuilders.imageInfo;
import static com.atlassian.marketplace.client.model.TestModelBuilders.product;
import static com.atlassian.marketplace.client.model.TestModelBuilders.productVersion;
import static com.atlassian.marketplace.client.model.TestModelBuilders.vendorSummary;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestModelBuildersTest
{
    // The following tests verify that the defaults provided by these builders don't
    // violate any preconditions in the model object constructors which would throw an
    // IllegalArgumentException or NullPointerException - and that the builders in
    // TestModelBuilders *do* provide defaults for required properties.

    @Test
    public void canBuildAddonWithDefaults()
    {
        addon().build();
    }
    
    @Test
    public void canBuildAddonCategorySummaryWithDefaults()
    {
        addonCategorySummary().build();
    }
    
    @Test
    public void canBuildAddonDistributionSummaryWithDefaults()
    {
        addonDistributionSummary().build();
    }

    @Test
    public void canBuildAddonReferenceWithDefaults()
    {
        addonReference().build();
    }
    
    @Test
    public void canBuildAddonSummaryWithDefaults()
    {
        addonSummary().build();
    }

    @Test
    public void canBuildAddonVersionWithDefaults()
    {
        addonVersion().build();
    }

    @Test
    public void addonVersionWithArtifactUriHasArtifactInfo()
    {
        AddonVersion v = addonVersion().artifactUri(some(ModelBuildersTest.TEST_URI)).build();
        assertThat(v.getArtifactInfo().isDefined(), equalTo(true));
    }

    @Test
    public void addonVersionWithArtifactUriHasNoRemoteLinkIfConnectIsFalse()
    {
        AddonVersion v = addonVersion().artifactUri(some(ModelBuildersTest.TEST_URI)).build();
        assertThat(v.getArtifactInfo().get().getRemoteDescriptorUri().isDefined(), equalTo(false));
    }

    @Test
    public void addonVersionWithArtifactUriHasRemoteLinkIfConnectIsTrue()
    {
        AddonVersion v = addonVersion().artifactUri(some(ModelBuildersTest.TEST_URI)).connect(true).build();
        assertThat(v.getArtifactInfo().get().getRemoteDescriptorUri().isDefined(), equalTo(true));
    }

    @Test
    public void canSetArtifactInfo()
    {
        AddonVersion v = addonVersion().artifact(some(artifactInfo(ModelBuildersTest.TEST_URI, true))).build();
        assertThat(v.getArtifactInfo().isDefined(), equalTo(true));
    }

    @Test
    public void canSetAddonVersionAutoUpdate()
    {
        AddonVersion v = addonVersion().autoUpdateAllowed(true).build();
        assertThat(v.isAutoUpdateAllowed(), equalTo(true));
    }
    
    @Test
    public void canBuildAddonVersionSummaryWithDefaults()
    {
        addonVersionSummary().build();
    }

    @Test
    public void canBuildApplicationWithDefaults()
    {
        application().build();
    }

    @Test
    public void canSetApplicationCloudFreeUsers() throws Exception
    {
        assertThat(application().cloudFreeUsers(some(2)).build().getCloudFreeUsers(), equalTo(some(2)));
    }

    @Test
    public void canSetApplicationIntroduction() throws Exception
    {
        assertThat(application().introduction("x").build().getIntroduction(), equalTo("x"));
    }

    @Test
    public void canSetApplicationKey() throws Exception
    {
        assertThat(application().key(JIRA).build().getKey(), equalTo(JIRA));
    }
    
    @Test
    public void canSetApplicationName() throws Exception
    {
        assertThat(application().name("x").build().getName(), equalTo("x"));
    }

    @Test
    public void canSetApplicationStatus() throws Exception
    {
        assertThat(application().status(ApplicationStatus.PUBLISHED).build().getStatus(),
                equalTo(ApplicationStatus.PUBLISHED));
    }

    @Test
    public void canBuildApplicationVersionWithDefaults()
    {
        applicationVersion().build();
    }
    
    @Test
    public void canBuildImageInfoWithDefaults()
    {
        imageInfo().build();
    }
    
    @Test
    public void canBuildProductWithDefaults()
    {
        product().build();
    }

    @Test
    public void canBuildProductVersionWithDefaults()
    {
        productVersion().build();
    }

    @Test
    public void productVersionWithArtifactUriHasArtifactInfo()
    {
        ProductVersion v = productVersion().artifactUri(some(ModelBuildersTest.TEST_URI)).build();
        assertThat(v.getArtifactUri().isDefined(), equalTo(true));
    }

    @Test
    public void canBuildVendorSummaryWithDefaults()
    {
        vendorSummary().build();
    }

    @Test
    public void canSetAddonVersionArtifactRemoteLink()
    {
        AddonVersion v = addonVersion().artifact(some(artifactInfo(ModelBuildersTest.TEST_URI, true))).build();
        assertThat(v.getRemoteDescriptorUri(), equalTo(some(ModelBuildersTest.TEST_URI)));
    }

    @Test
    public void canSetAddonVersionSummaryArtifactRemoteLink()
    {
        AddonVersionSummary vs = addonVersionSummary().artifactUri(some(ModelBuildersTest.TEST_URI)).connect(true).build();
        assertThat(vs.getRemoteDescriptorUri(), equalTo(some(ModelBuildersTest.TEST_URI)));
    }
}
