package com.atlassian.marketplace.client.model;

import org.junit.Test;

import static com.atlassian.marketplace.client.api.ApplicationKey.CONFLUENCE;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.api.HostingType.CLOUD;
import static com.atlassian.marketplace.client.api.HostingType.SERVER;
import static com.atlassian.marketplace.client.model.ModelBuilders.versionCompatibilityForCloud;
import static com.atlassian.marketplace.client.model.ModelBuilders.versionCompatibilityForServer;
import static com.atlassian.marketplace.client.model.ModelBuilders.versionCompatibilityForServerAndCloud;
import static com.google.common.base.Predicates.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class VersionCompatibilityTest
{
    @Test
    public void compatibleWithServerInServerWithBuildEqualToMinimum()
    {
        assertTrue(versionCompatibilityForServer(JIRA, 100, 200).isCompatibleWith(equalTo(JIRA), SERVER, 100));
    }

    @Test
    public void compatibleWithServerInServerWithBuildEqualToMaximum()
    {
        assertTrue(versionCompatibilityForServer(JIRA, 100, 200).isCompatibleWith(equalTo(JIRA), SERVER, 200));
    }
    
    @Test
    public void compatibleWithServerInServerWithBuildInsideRange()
    {
        assertTrue(versionCompatibilityForServer(JIRA, 100, 200).isCompatibleWith(equalTo(JIRA), SERVER, 150));
    }
    
    @Test
    public void compatibleWithServerAndCloudInServerWithBuildEqualToMinimum()
    {
        assertTrue(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 100));
    }

    @Test
    public void compatibleWithServerAndCloudInServerWithBuildEqualToMaximum()
    {
        assertTrue(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 200));
    }
    
    @Test
    public void compatibleWithServerAndCloudInServerWithBuildInsideRange()
    {
        assertTrue(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 150));
    }

    @Test
    public void compatibleWithCloudInCloud()
    {
        assertTrue(versionCompatibilityForCloud(JIRA).isCompatibleWith(equalTo(JIRA), CLOUD, 150));
    }

    @Test
    public void compatibleWithCloudAndServerInCloudRegardlessOfBuild()
    {
        assertTrue(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), CLOUD, 450));
    }

    @Test
    public void incompatibleWithServerInCloud()
    {
        assertFalse(versionCompatibilityForServer(JIRA, 100, 200).isCompatibleWith(equalTo(JIRA), CLOUD, 150));
    }
    
    @Test
    public void incompatibleWithCloudInServer()
    {
        assertFalse(versionCompatibilityForCloud(JIRA).isCompatibleWith(equalTo(JIRA), SERVER, 150));
    }

    @Test
    public void incompatibleWithServerInServerWithBuildLessThanMinimum()
    {
        assertFalse(versionCompatibilityForServer(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 50));
    }

    @Test
    public void incompatibleWithServerInServerWithBuildGreaterThanMaximum()
    {
        assertFalse(versionCompatibilityForServer(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 2550));
    }

    @Test
    public void incompatibleWithServerInServerWithWrongApplication()
    {
        assertFalse(versionCompatibilityForServer(JIRA, 100, 200).
                isCompatibleWith(equalTo(CONFLUENCE), SERVER, 150));
    }
    
    @Test
    public void incompatibleWithServerAndCloudInServerWithBuildLessThanMinimum()
    {
        assertFalse(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 50));
    }

    @Test
    public void incompatibleWithServerAndCloudInServerWithBuildGreaterThanMaximum()
    {
        assertFalse(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(JIRA), SERVER, 2550));
    }

    @Test
    public void incompatibleWithServerAndCloudInServerWithWrongApplication()
    {
        assertFalse(versionCompatibilityForServerAndCloud(JIRA, 100, 200).
                isCompatibleWith(equalTo(CONFLUENCE), SERVER, 150));
    }
}
