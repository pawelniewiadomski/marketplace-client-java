package com.atlassian.marketplace.client.http;

import java.io.ByteArrayInputStream;
import java.net.URI;

import com.atlassian.marketplace.client.http.HttpConfiguration.Credentials;
import com.atlassian.marketplace.client.impl.CommonsHttpTransport;
import com.atlassian.utt.http.TestHttpServer.RequestProperties;

import com.google.common.collect.ImmutableMap;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.utt.http.TestHttpServer.requests;
import static com.atlassian.utt.http.TestHttpServer.status;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.header;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.method;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestBody;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.uri;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class CommonsHttpTransportTest extends CommonsHttpTransportTestBase
{
    @Test
    public void simpleGetRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.get(absoluteTestUri());
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(
                allOf(
                    method().is(equalTo("GET")),
                    uri().is(equalTo(TEST_URI))
                )
            )
        ));
    }

    @Test
    public void simpleGetResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("GET")), uri().is(equalTo(TEST_URI))),
            status(200).body("foo")
        );
        resp = http.get(absoluteTestUri());
        assertThat(resp,
            allOf(
                responseStatus().is(equalTo(200)),
                responseBody().is(equalTo("foo"))
            )
        );
    }

    @Test
    public void simplePostRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.post(absoluteTestUri(),
            new ByteArrayInputStream(makeContent()),
            makeContent().length,
            "application/thing",
            "application/reply"
        );
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("POST")),
                    uri().is(equalTo(TEST_URI)),
                    header("Content-Type").is(contains("application/thing")),
                    header("Accept").is(contains("application/reply")),
                    requestBody().is(equalTo(CONTENT_STRING))
                )
            )
        ));
    }

    @Test
    public void simplePostResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("POST")), uri().is(equalTo(TEST_URI))),
            status(200).body("foo")
        );
        resp = http.post(absoluteTestUri(),
            new ByteArrayInputStream(makeContent()),
            makeContent().length,
            "application/thing",
            "application/reply"
        );
        assertThat(resp,
            allOf(
                responseStatus().is(equalTo(200)),
                responseBody().is(equalTo("foo"))
            )
        );
    }

    @Test
    public void postParamsRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.postParams(absoluteTestUri(), makeParams());
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("POST")),
                    uri().is(equalTo(TEST_URI)),
                    hasPostParams()
                )
            )
        ));
    }
    
    @Test
    public void postParamsResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("POST")), uri().is(equalTo(TEST_URI))),
            status(200).body("foo")
        );
        resp = http.postParams(absoluteTestUri(), makeParams());
        assertThat(resp,
            allOf(
                responseStatus().is(equalTo(200)),
                responseBody().is(equalTo("foo"))
            )
        );
    }
    
    @Test
    public void simplePutRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.put(absoluteTestUri(), makeContent());
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("PUT")),
                    uri().is(equalTo(TEST_URI)),
                    header("Content-Type").is(contains("application/json; charset=UTF-8")),
                    requestBody().is(equalTo(CONTENT_STRING))
                )
            )
        ));
    }

    @Test
    public void simplePutResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("PUT")), uri().is(equalTo(TEST_URI))),
            status(200).body("foo")
        );
        resp = http.put(absoluteTestUri(), makeContent());
        assertThat(resp,
            allOf(
                responseStatus().is(equalTo(200)),
                responseBody().is(equalTo("foo"))
            )
        );
    }

    @Test
    public void simplePatchRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.patch(absoluteTestUri(), makeContent());
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("PATCH")),
                    uri().is(equalTo(TEST_URI)),
                    header("Content-Type").is(contains("application/json-patch+json; charset=UTF-8")),
                    requestBody().is(equalTo(CONTENT_STRING))
                )
            )
        ));
    }

    @Test
    public void simplePatchResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("PATCH")), uri().is(equalTo(TEST_URI))),
            status(200).body("foo")
        );
        resp = http.patch(absoluteTestUri(), makeContent());
        assertThat(resp,
            allOf(
                responseStatus().is(equalTo(200)),
                responseBody().is(equalTo("foo"))
            )
        );
    }

    @Test
    public void simpleDeleteRequest() throws Exception
    {
        http = makeHttp(baseConfig());
        resp = http.delete(absoluteTestUri());
        assertThat(server, requests().is(
            contains(
                allOf(
                    method().is(equalTo("DELETE")),
                    uri().is(equalTo(TEST_URI))
                )
            )
        ));
    }
    
    @Test
    public void simpleDeleteResponse() throws Exception
    {
        http = makeHttp(baseConfig());
        server.respondIf(
            allOf(method().is(equalTo("DELETE")), uri().is(equalTo(TEST_URI))),
            status(204)
        );
        resp = http.delete(absoluteTestUri());
        assertThat(resp,
            responseStatus().is(equalTo(204))
        );
    }
    
    @Test
    public void requestDecoratorCanAddHeadersForGet() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        resp = http.get(absoluteTestUri());
        
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(hasRequestDecoratorHeaders())
        ));
    }

    @Test
    public void requestDecoratorCanAddHeadersForSimplePost() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        resp = http.post(absoluteTestUri(),
            new ByteArrayInputStream(makeContent()),
            makeContent().length,
            "application/thing",
            "application/reply"
        );
        
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(hasRequestDecoratorHeaders())
        ));
    }

    @Test
    public void requestDecoratorCanAddHeadersForPostParams() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        resp = http.postParams(absoluteTestUri(), makeParams());
        
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(hasRequestDecoratorHeaders())
        ));
    }

    @Test
    public void requestDecoratorCanAddHeadersForPut() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        resp = http.put(absoluteTestUri(), makeContent());
        
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(hasRequestDecoratorHeaders())
        ));
    }

    @Test
    public void requestDecoratorCanAddHeadersForDelete() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        resp = http.delete(absoluteTestUri());
        
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(hasRequestDecoratorHeaders())
        ));
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void canAddAdditionalRequestDecoratorInDerivedHttpOperations() throws Exception
    {
        http = makeHttp(withSimpleRequestDecorator(baseConfig()));
        HttpTransport http2 = http.withRequestDecorator(simpleRequestDecorator(ImmutableMap.of("bar", "baz")));

        Matcher<RequestProperties> fooHeader = header("foo").is(contains("bar"));
        Matcher<RequestProperties> barHeader = header("bar").is(contains("baz"));

        resp = http.get(absoluteTestUri());
        SimpleHttpResponse resp2 = http2.get(absoluteTestUri());

        try
        {
            assertThat(server, requests().is(
                Matchers.<RequestProperties>contains(
                    allOf(fooHeader, not(barHeader)),
                    allOf(fooHeader, barHeader)
                )));
        }
        finally
        {
            resp2.close();
        }
    }

    @Test
    public void authenticatedRequestUsesPreemptiveBasicAuth() throws Exception
    {
        Credentials creds = new Credentials("user", "pass");
        http = makeHttp(baseConfig().credentials(some(creds)));
        resp = http.get(absoluteTestUri());
        assertThat(server, requests().is(
            Matchers.<RequestProperties>contains(
                allOf(
                    method().is(equalTo("GET")),
                    uri().is(equalTo(TEST_URI)),
                    header("Authorization").is(contains("Basic dXNlcjpwYXNz"))
                )
            )
        ));
    }
    
    private CommonsHttpTransport makeHttp(HttpConfiguration.Builder builder)
    {
        return new CommonsHttpTransport(builder.build(), serverUri);
    }

    private URI absoluteTestUri()
    {
        return serverUri.resolve(TEST_URI);
    }
}
