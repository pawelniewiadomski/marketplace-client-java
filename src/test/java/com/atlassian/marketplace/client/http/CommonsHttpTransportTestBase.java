package com.atlassian.marketplace.client.http;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.marketplace.client.impl.CommonsHttpTransport;
import com.atlassian.utt.http.TestHttpServer;
import com.atlassian.utt.http.TestHttpServer.RequestProperties;
import com.atlassian.utt.matchers.NamedFunction;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Multimap;

import org.apache.commons.io.IOUtils;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.header;
import static com.atlassian.utt.http.TestHttpServer.RequestProperties.requestBody;
import static com.atlassian.utt.matchers.NamedFunction.namedFunction;
import static com.google.common.collect.Multimaps.newListMultimap;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

public class CommonsHttpTransportTestBase
{
    private static AtomicInteger nextServerPort = new AtomicInteger(8173);
    
    protected static final URI TEST_URI = URI.create("/test/request");
    
    protected static final String CONTENT_STRING = "a thing";
    
    protected int serverPort;
    protected URI serverUri;
    protected TestHttpServer server;
    protected CommonsHttpTransport http;
    protected SimpleHttpResponse resp;
    
    @Before
    public void setup() throws Exception
    {
        serverPort = nextServerPort.getAndIncrement();
        serverUri = URI.create("http://localhost:" + serverPort);
        
        server = TestHttpServer.create(serverPort);
        server.start();
    }
    
    @After
    public void teardown()
    {
        server.stop();
        if (http != null)
        {
            http.close();
        }
        if (resp != null)
        {
            resp.close();
        }
    }
    
    protected byte[] makeContent() throws Exception
    {
        return CONTENT_STRING.getBytes("UTF-8");
    }
    
    protected Multimap<String, String> makeParams() throws Exception
    {
        Multimap<String, String> params = newListMultimap(new HashMap<String, Collection<String>>(),
            new Supplier<ArrayList<String>>()
            {
                @Override
                public ArrayList<String> get()
                {
                    return new ArrayList<String>();
                }
            });
        params.put("param", "value");
        params.put("foo", "bar");
        params.put("foo", "baz");
        return params;
    }
    
    protected HttpConfiguration.Builder baseConfig()
    {
        return HttpConfiguration.builder();
    }
    
    protected HttpConfiguration.Builder withSimpleRequestDecorator(HttpConfiguration.Builder builder)
    {
        RequestDecorator rd = new RequestDecorator()
            {
                @Override
                public Map<String, String> getRequestHeaders()
                {
                    return ImmutableMap.of("foo", "bar");
                }
            };
        return builder.requestDecorator(some(rd));
    }
    
    protected RequestDecorator simpleRequestDecorator(final Map<String, String> headers)
    {
        return new RequestDecorator()
        {
            @Override
            public Map<String, String> getRequestHeaders()
            {
                return headers;
            }
        };
    }
    
    protected Matcher<RequestProperties> hasPostParams()
    {
        return allOf(
            header("Content-Type").is(contains("application/x-www-form-urlencoded")),
            requestBody().is(equalTo("param=value&foo=bar&foo=baz"))
        );
    }
    
    protected Matcher<RequestProperties> hasRequestDecoratorHeaders()
    {
        return header("foo").is(contains("bar"));
    }
    
    protected static NamedFunction<SimpleHttpResponse, Integer> responseStatus()
    {
        return namedFunction("responseStatus", new Function<SimpleHttpResponse, Integer>()
        {
            @Override
            public Integer apply(SimpleHttpResponse r)
            {
                return r.getStatus();
            }
        });
    }
    
    protected static NamedFunction<SimpleHttpResponse, String> responseBody()
    {
        return namedFunction("responseBody", new Function<SimpleHttpResponse, String>()
        {
            @Override
            public String apply(SimpleHttpResponse r)
            {
                try
                {
                    return new String(IOUtils.toByteArray(r.getContentStream()), "UTF-8");
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
