package com.atlassian.marketplace.client.http;

import java.util.Map;

import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.http.RequestDecorator;
import com.atlassian.marketplace.client.http.HttpConfiguration.Credentials;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthParams;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyConfiguration;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyHost;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthMethod.BASIC;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestHttpConfiguration
{
    private static String HOST = "foo";
    private static RequestDecorator DECORATOR = new RequestDecorator()
    {
        @Override
        public Map<String, String> getRequestHeaders()
        {
            return ImmutableMap.of();
        }
    };

    @Test
    public void hasNoCredentialsByDefault()
    {
        assertThat(HttpConfiguration.builder().build().getCredentials(), equalTo(none(Credentials.class)));
    }

    @Test
    public void canSetCredentials()
    {
        assertThat(HttpConfiguration.builder().credentials(some(new Credentials("u", "p"))).build().getCredentials(),
                equalTo(some(new Credentials("u", "p"))));
    }

    @Test
    public void canSetConnectTimeoutMillis()
    {
        assertThat(HttpConfiguration.builder().connectTimeoutMillis(999).build().getConnectTimeoutMillis(),
                equalTo(999));
    }
    
    @Test
    public void canSetReadTimeoutMillis()
    {
        assertThat(HttpConfiguration.builder().readTimeoutMillis(999).build().getReadTimeoutMillis(),
                equalTo(999));
    }
    
    @Test
    public void canSetMaxConnections()
    {
        assertThat(HttpConfiguration.builder().maxConnections(999).build().getMaxConnections(),
                equalTo(999));
    }

    @Test
    public void canSetMaxCacheEntries()
    {
        assertThat(HttpConfiguration.builder().maxCacheEntries(999).build().getMaxCacheEntries(),
                equalTo(999));
    }

    @Test
    public void canSetMaxCacheObjectSize()
    {
        assertThat(HttpConfiguration.builder().maxCacheObjectSize(999L).build().getMaxCacheObjectSize(),
                equalTo(999L));
    }
    
    @Test
    public void canSetProxyConfig()
    {
        ProxyConfiguration proxy = ProxyConfiguration.builder().proxyHost(some(new ProxyHost(HOST))).build();
        assertThat(HttpConfiguration.builder().proxyConfiguration(some(proxy)).build().getProxyConfiguration(),
                equalTo(some(proxy)));
    }
    
    @Test
    public void canSetRequestDecorator()
    {
        assertThat(HttpConfiguration.builder().requestDecorator(some(DECORATOR)).build().getRequestDecorator(),
                equalTo(some(DECORATOR)));
    }
    
    @Test
    public void proxyCanSetHost()
    {
        ProxyHost ph = new ProxyHost(HOST);
        assertThat(ProxyConfiguration.builder().proxyHost(some(ph)).build().getProxyHost(), equalTo(some(ph)));
    }

    @Test
    public void proxyCanSetAuthParams()
    {
        ProxyAuthParams ap = new ProxyAuthParams(new Credentials("u", "p"), BASIC);
        assertThat(ProxyConfiguration.builder().authParams(some(ap)).build().getAuthParams(), equalTo(some(ap)));
    }
}
