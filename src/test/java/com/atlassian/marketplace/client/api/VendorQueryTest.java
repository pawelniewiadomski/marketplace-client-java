package com.atlassian.marketplace.client.api;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.api.VendorQuery.any;
import static com.atlassian.marketplace.client.api.VendorQuery.builder;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class VendorQueryTest
{
    @Test
    public void defaultQueryIsSameAsAny()
    {
        assertThat(builder().build(), equalTo(any()));
    }
    
    @Test
    public void defaultQueryHasForThisUserFalse()
    {
        assertThat(any().isForThisUserOnly(), equalTo(false));
    }
    
    @Test
    public void forThisUserCanBeSetToTrue()
    {
        assertThat(builder().forThisUserOnly(true).build().isForThisUserOnly(), equalTo(true));
    }

    @Test
    public void forThisUserCanBeSetToFalser()
    {
        assertThat(builder().forThisUserOnly(false).build().isForThisUserOnly(), equalTo(false));
    }


    @Test
    public void defaultQueryHasDefaultBounds()
    {
        assertThat(any().getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(some(3));
        VendorQuery q = builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
