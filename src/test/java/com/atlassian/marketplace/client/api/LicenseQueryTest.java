package com.atlassian.marketplace.client.api;

import org.junit.Test;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.api.LicenseQuery.any;
import static com.atlassian.marketplace.client.api.LicenseQuery.builder;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

public class LicenseQueryTest
{
    @Test
    public void defaultQueryIsSameAsAny()
    {
        assertThat(builder().build(), equalTo(any()));
    }
    
    @Test
    public void addonCanBeSet()
    {
        assertThat(builder().addon(of("test")).build().getAddon(), contains("test"));
    }

    @Test
    public void defaultQueryHasDefaultBounds()
    {
        assertThat(any().getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(some(3));
        LicenseQuery q = builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
