package com.atlassian.marketplace.client.api;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.api.AddonQuery.any;
import static com.atlassian.marketplace.client.api.AddonQuery.builder;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.api.Cost.ALL_PAID;
import static com.atlassian.marketplace.client.api.HostingType.CLOUD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AddonQueryTest
{
    @Test
    public void defaultQueryIsSameAsAny()
    {
        assertThat(builder().build(), equalTo(any()));
    }
    
    @Test
    public void defaultQueryHasNoAccessToken()
    {
        assertThat(any().getAccessToken(), equalTo(none(String.class)));
    }
    
    @Test
    public void accessTokenCanBeSet()
    {
        assertThat(builder().accessToken(some("x")).build().getAccessToken(), equalTo(some("x")));
    }
    
    @Test
    public void defaultQueryHasNoApplication()
    {
        assertThat(any().getApplication(), equalTo(none(ApplicationKey.class)));
    }
    
    @Test
    public void applicationCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().application(some(JIRA)).build();
        assertThat(q.getApplication(), equalTo(some(JIRA)));
    }
    
    @Test
    public void defaultQueryHasNoAppBuildNumber()
    {
        assertThat(any().getAppBuildNumber(), equalTo(none(Integer.class)));
    }
    
    @Test
    public void appBuildNumberCanBeSet()
    {
        AddonQuery q = builder().appBuildNumber(some(1000)).build();
        assertThat(q.getAppBuildNumber(), equalTo(some(1000)));
    }

    @Test
    public void defaultQueryHasNoCategories()
    {
        assertThat(any().getCategoryNames(), Matchers.<String>emptyIterable());
    }
    
    @Test
    public void categoriesCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().categoryNames(ImmutableList.of("a", "b")).build();
        assertThat(q.getCategoryNames(), Matchers.<String>contains("a", "b"));
    }

    @Test
    public void defaultQueryHasNoCost()
    {
        assertThat(any().getCost(), Matchers.<Cost>emptyIterable());
    }
    
    @Test
    public void costCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().cost(some(ALL_PAID)).build();
        assertThat(q.getCost(), equalTo(some(ALL_PAID)));
    }

    @Test
    public void defaultQueryHasNoHosting()
    {
        assertThat(any().getHosting(), equalTo(none(HostingType.class)));
    }
    
    @Test
    public void hostingCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().hosting(some(CLOUD)).build();
        assertThat(q.getHosting(), equalTo(some(CLOUD)));
    }
    
    @Test
    public void defaultQueryHasNoSearchText()
    {
        assertThat(any().getSearchText(), equalTo(none(String.class)));
    }
    
    @Test
    public void searchTextCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().searchText(some("yo")).build();
        assertThat(q.getSearchText(), equalTo(some("yo")));
    }
    
    @Test
    public void defaultQueryHasWithVersionFalse()
    {
        assertThat(any().isWithVersion(), equalTo(false));
    }
    
    @Test
    public void withVersionCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().withVersion(true).build();
        assertThat(q.isWithVersion(), equalTo(true));
    }

    @Test
    public void defaultQueryHasDefaultBounds()
    {
        assertThat(any().getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(some(3));
        AddonQuery q = AddonQuery.builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
