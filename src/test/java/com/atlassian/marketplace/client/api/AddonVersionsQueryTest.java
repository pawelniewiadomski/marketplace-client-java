package com.atlassian.marketplace.client.api;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AddonVersionsQueryTest {

    @Test
    public void queryCanIncludePrivateVersions() {
        assertIncludePrivate(true);
    }

    @Test
    public void queryCanExcludePrivateVersions() {
        assertIncludePrivate(false);
    }

    private void assertIncludePrivate(final boolean requestedIncludePrivate) {
        // Set up
        final AddonVersionsQuery query = AddonVersionsQuery.builder()
                .includePrivate(requestedIncludePrivate)
                .build();

        // Invoke
        final boolean actualIncludePrivate = query.isIncludePrivate();

        // Check
        assertThat(actualIncludePrivate, is(requestedIncludePrivate));
    }
}