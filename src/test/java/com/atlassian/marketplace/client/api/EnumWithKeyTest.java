package com.atlassian.marketplace.client.api;

import com.atlassian.marketplace.client.api.EnumWithKey;

import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EnumWithKeyTest
{
    @Test
    public void canGetAllValues()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).getValues(),
            equalTo(new MyEnum[] { MyEnum.LITTLE, MyEnum.BIG }));
    }

    @Test
    public void valueForKeyMatchesKey()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).valueForKey("big"),
            equalTo(some(MyEnum.BIG)));
    }

    @Test
    public void valueForKeyMatchesKeyCaseInsensitively()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).valueForKey("BiG"),
            equalTo(some(MyEnum.BIG)));
    }
    
    @Test
    public void valueForKeyReturnsNoneIfNotFound()
    {
        assertThat(EnumWithKey.Parser.forType(MyEnum.class).valueForKey("medium"),
            equalTo(none(MyEnum.class)));
    }
    
    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void cannotGetParserForUnsupportedEnum()
    {
        Class<?> badClassObscuredByTypeErasure = EnumWithoutKey.class;
        EnumWithKey.Parser.forType((Class<? extends EnumWithKey>) badClassObscuredByTypeErasure);
    }
    
    private static enum MyEnum implements EnumWithKey
    {
        LITTLE("little"),
        BIG("big");
        
        private final String key;
        
        private MyEnum(String key)
        {
            this.key = key;
        }
        
        public String getKey()
        {
            return key;
        }
    }
    
    private static enum EnumWithoutKey
    {
        RED,
        GREEN;
    }
}
