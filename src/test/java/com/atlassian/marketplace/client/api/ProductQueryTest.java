package com.atlassian.marketplace.client.api;

import org.hamcrest.Matchers;
import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.api.ApplicationKey.JIRA;
import static com.atlassian.marketplace.client.api.Cost.ALL_PAID;
import static com.atlassian.marketplace.client.api.HostingType.CLOUD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ProductQueryTest
{
    @Test
    public void defaultQueryHasNoApplication()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.getApplication(), equalTo(none(ApplicationKey.class)));
    }
    
    @Test
    public void applicationCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().application(some(JIRA)).build();
        assertThat(q.getApplication(), equalTo(some(JIRA)));
    }
    
    @Test
    public void defaultQueryHasNoAppBuildNumber()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.getAppBuildNumber(), equalTo(none(Integer.class)));
    }
    
    @Test
    public void appBuildNumberCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().appBuildNumber(some(1000)).build();
        assertThat(q.getAppBuildNumber(), equalTo(some(1000)));
    }

    @Test
    public void defaultQueryHasNoCost()
    {
        AddonQuery q = AddonQuery.builder().build();
        assertThat(q.getCost(), Matchers.<Cost>emptyIterable());
    }
    
    @Test
    public void costCanBeSet()
    {
        AddonQuery q = AddonQuery.builder().cost(some(ALL_PAID)).build();
        assertThat(q.getCost(), equalTo(some(ALL_PAID)));
    }

    @Test
    public void defaultQueryHasNoHosting()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.getHosting(), equalTo(none(HostingType.class)));
    }
    
    @Test
    public void hostingCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().hosting(some(CLOUD)).build();
        assertThat(q.getHosting(), equalTo(some(CLOUD)));
    }
    
    @Test
    public void defaultQueryHasWithVersionFalse()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.isWithVersion(), equalTo(false));
    }
    
    @Test
    public void withVersionCanBeSet()
    {
        ProductQuery q = ProductQuery.builder().withVersion(true).build();
        assertThat(q.isWithVersion(), equalTo(true));
    }

    @Test
    public void defaultQueryHasDefaultBounds()
    {
        ProductQuery q = ProductQuery.builder().build();
        assertThat(q.getBounds(), equalTo(QueryBounds.defaultBounds()));
    }

    @Test
    public void boundsCanBeSet()
    {
        QueryBounds b = QueryBounds.offset(2).withLimit(some(3));
        ProductQuery q = ProductQuery.builder().bounds(b).build();
        assertThat(q.getBounds(), equalTo(b));
    }
}
