package com.atlassian.marketplace.client.api;

import java.net.URI;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class UriTemplateTest
{
    @Test
    public void templateWithPlaceholdersIsValid()
    {
        String s = "/this/is/{good}";
        assertThat(UriTemplate.create(s).getValue(), equalTo(s));
    }
    
    @Test
    public void simplePlaceholdersCanBeSubstituted()
    {
        UriTemplate t = UriTemplate.create("/my/{a}/path/is/my/{b}/{a}/path/{c}");
        assertThat(t.resolve(ImmutableMap.of("a", "favorite", "b", "very")),
                equalTo(URI.create("/my/favorite/path/is/my/very/favorite/path/")));
    }
    
    @Test
    public void queryParamsCanBeSubstituted()
    {
        UriTemplate t = UriTemplate.create("/my/{a}/path{?b,c,d}");
        assertThat(t.resolve(ImmutableMap.of("a", "special", "b", "bee", "d", "do thing")),
                equalTo(URI.create("/my/special/path?b=bee&d=do+thing")));
    }
}
