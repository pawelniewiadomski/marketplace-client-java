package com.atlassian.marketplace.client;

import java.io.Closeable;

import com.atlassian.marketplace.client.api.AddonCategories;
import com.atlassian.marketplace.client.api.Addons;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.api.Assets;
import com.atlassian.marketplace.client.api.LicenseTypes;
import com.atlassian.marketplace.client.api.Licenses;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.Products;
import com.atlassian.marketplace.client.api.Vendors;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.VendorBase;

/**
 * Client interface for the Atlassian Marketplace 2.0 API.
 * <p>
 * Construct a concrete implementation of the client ({@link com.atlassian.marketplace.client.impl.DefaultMarketplaceClient})
 * and use that instance for all API requests.
 */
public interface MarketplaceClient extends Closeable
{
    /**
     * Checks whether the MPAC service is available, by attempting to get the root resource.
     * 
     * @return  true if the request was successful; false if it failed for any reason
     */
    boolean isReachable();
    
    /**
     * Returns an API object that provides access to add-on listings.
     */
    Addons addons() throws MpacException;
    
    /**
     * Returns an API object that provides access to add-on categories.
     */
    AddonCategories addonCategories() throws MpacException;
    
    /**
     * Returns an API object that provides access to application listings.
     */
    Applications applications() throws MpacException;
    
    /**
     * Returns an API object that provides file uploading capabilities.
     */
    Assets assets() throws MpacException;
    
    /**
     * Returns an API object that provides access to software license types.
     */
    LicenseTypes licenseTypes() throws MpacException;
    
    /**
     * Returns an API object that provides access to product listings (which are called "applications" in the administrative UI).
     */
    Products products() throws MpacException;

    /**
     * Returns an API object that provides access to vendors.
     */
    Vendors vendors() throws MpacException;

    /**
     * Returns an API object that provides access to licenses.
     */
    Licenses licenses(VendorBase vendor) throws MpacException;

    /**
     * Queries a page of a result set from a previous query, for any resource that provides paginated
     * results (such as {@link Addons#find}).  The {@link PageReference} is obtained by calling
     * {@link Page#getPrevious()}, {@link Page#getNext()}, or {@link Page#getReference()} on an existing
     * {@link Page} of results.
     * @return  a {@link Page} containing the results
     * @throws MpacException  if the server was unavailable or returned an error
     */
    <T> Page<T> getMore(PageReference<T> ref) throws MpacException;

    /**
     * Returns the top-level API resource links, such as "addons".  <tt>MarketplaceClient</tt>
     * normally traverses these links automatically; you should only need to use this method if
     * you are passing a URL outside of Java code, for instance to a front-end script. 
     * @return  a {@link Links} object
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Links getRootLinks() throws MpacException;
    
    /**
     * Provides access to the underlying HTTP transport mechanism.  This allows for
     * implementation of extension modules for Marketplace APIs that are not supported
     * by the base library.
     */
    HttpTransport getHttp();

    /**
     * Attempts to convert the specified model object to a JSON string.  This should be used only for
     * testing purposes; normally, JSON serialization is done automatically within client methods.
     * @param entity  a model object
     * @return  a JSON string
     * @throws MpacException if the object could not be serialized as JSON (for instance, because it
     *   is not an instance of any of this library's model classes)
     */
    <T> String toJson(T entity) throws MpacException;
    
    /**
     * Should be called when finished with client to release resources.
     */
    void close();
}
