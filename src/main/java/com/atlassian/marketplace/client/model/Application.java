package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.EnumWithKey;

import static com.atlassian.fugue.Option.none;

/**
 * Information about an Atlassian application/platform, such as JIRA.
 * @see com.atlassian.marketplace.client.api.Applications
 */
public class Application implements Entity
{
    Links _links;
    @RequiredLink(rel = "self") URI selfUri;
    
    String name;
    ApplicationKey key;
    String introduction;
    ApplicationStatus status;
    ConnectSupport atlassianConnectSupport;
    CompatibilityUpdateMode compatibilityMode;
    Details details;
    HostingSupport hostingSupport;
    @Deprecated @ReadOnly Option<Integer> cloudFreeUsers;
    
    // not supported: keyAliases

    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }
    
    /**
     * The application name.
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * The unique string key representing the application.
     */
    public ApplicationKey getKey()
    {
        return key;
    }

    public ApplicationStatus getStatus()
    {
        return status;
    }

    public String getIntroduction()
    {
        return introduction;
    }

    public CompatibilityUpdateMode getCompatibilityUpdateMode()
    {
        return compatibilityMode;
    }
    
    public String getDescription()
    {
        return details.description;
    }
    
    public URI getLearnMoreUri()
    {
        return details.learnMore;
    }
    
    public Option<URI> getDownloadPageUri()
    {
        return details.downloadPage;
    }

    /**
     * @deprecated will be removed in next major version. Will always return a none as there are never any cloud free users.
     */
    @Deprecated
    public Option<Integer> getCloudFreeUsers()
    {
        return cloudFreeUsers;
    }
    
    /**
     * Indicates how Marketplace will update add-on compatibilities when a new application version is
     * released. 
     */
    public enum CompatibilityUpdateMode implements EnumWithKey
    {
        /**
         * Indicates that Marketplace will automatically mark an add-on version as compatible
         * with the next micro/patch release version of an application, if it was compatible
         * with the previous one (for instance, going from 3.1.2 to 3.1.3). 
         */
        MICRO_VERSIONS("micro"),
        /**
         * Indicates that Marketplace will automatically mark an add-on version as compatible
         * with the next minor release version of an application, if it was compatible
         * with the previous one (for instance, going from 3.1.x to 3.2). 
         */
        MINOR_VERSIONS("minor");
        
        private String key;
        
        private CompatibilityUpdateMode(String key)
        {
            this.key = key;
        }
        
        @Override
        public String getKey()
        {
            return key;
        }
    }
    
    static class ConnectSupport
    {
        Boolean cloud;
        Boolean server;
    }
    
    static class Details
    {
        String description;
        URI learnMore;
        Option<URI> downloadPage;
    }
    
    static class HostingModelSupport
    {
        Boolean enabled;
        // not supported: customHamsKey
    }
    
    static class HostingSupport
    {
        HostingModelSupport cloud;
        HostingModelSupport server;
    }
}
