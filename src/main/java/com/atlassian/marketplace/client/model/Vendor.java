package com.atlassian.marketplace.client.model;

import java.net.URI;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.VendorExternalLinkType;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;

/**
 * Details about a vendor. Includes all the properties of {@link VendorBase}, but adds
 * further details; to get these details, you will need to query the vendor individually
 * with {@link com.atlassian.marketplace.client.api.Vendors#getById}.
 * @see VendorSummary
 * @since 2.0.0
 */
public final class Vendor extends VendorBase
{
    @ReadOnly Embedded _embedded;
    Option<String> description;
    Option<Address> address;
    String email;
    Option<String> phone;
    Map<String, URI> vendorLinks;
    Option<String> otherContactDetails;
    SupportDetails supportDetails = new SupportDetails();  // required in schema, but not supported in client API

    /**
     * Short introductory text that appears on the vendor's Marketplace page.
     * @see ModelBuilders.VendorBuilder#description(Option)
     */
    public Option<String> getDescription()
    {
        return description;
    }
    
    /**
     * The vendor's public contact email.
     * @see ModelBuilders.VendorBuilder#email(String)
     */
    public String getEmail()
    {
        return email;
    }
    
    /**
     * The vendor's public contact address.
     * @see ModelBuilders.VendorBuilder#address(Option)
     */
    public Option<Address> getAddress()
    {
        return address;
    }
    
    /**
     * The vendor's public phone number.
     * @see ModelBuilders.VendorBuilder#phone(Option)
     */
    public Option<String> getPhone()
    {
        return phone;
    }
    
    /**
     * Any other public contact information the vendor wishes to provide.
     * @see ModelBuilders.VendorBuilder#otherContactDetails(Option)
     */
    public Option<String> getOtherContactDetails()
    {
        return otherContactDetails;
    }

    /**
     * Public support information for a vendor.
     * @see ModelBuilders.VendorBuilder#supportDetails(SupportDetails)
     */
    public SupportDetails getSupportDetails()
    {
        return supportDetails;
    }

    /**
     * Returns one of the optional vendor-specified external links for the vendor.
     * @param type specifies which type of link to get
     * @return the link URI, or {@link Option#none()} if there is no such link
     */
    public Option<URI> getExternalLinkUri(VendorExternalLinkType type)
    {
        return option(vendorLinks.get(type.getKey()));
    }
    
    @Override
    public Option<ImageInfo> getLogo()
    {
        return _embedded.logo;
    }

    static final class Embedded
    {
        Option<ImageInfo> logo;
    }
    
}
