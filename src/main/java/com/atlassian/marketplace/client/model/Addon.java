package com.atlassian.marketplace.client.model;

import java.net.URI;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.AddonExternalLinkType;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.marketplace.client.model.Links.WEB_TYPE;

/**
 * Information about an add-on. Includes all the properties of {@link AddonBase}, but adds
 * further details; to get these details, you will need to query the add-on individually
 * with {@link com.atlassian.marketplace.client.api.Addons#getByKey}. 
 * <p>
 * To construct a new instance of this class in order to create or update an add-on, use
 * {@link ModelBuilders#addon()}.
 * 
 * @see com.atlassian.marketplace.client.api.Addons
 * @see AddonSummary
 * @since 2.0.0
 */
public class Addon extends AddonBase
{
    Embedded _embedded;
    Option<Boolean> enableAtlassianAnswers;
    Map<String, URI> vendorLinks;
    @ReadOnly Option<LegacyProperties> legacy;
    
    /**
     * The add-on banner image. This is not displayed on Marketplace, but may appear in UPM.
     * @see ModelBuilders.AddonBuilder#banner
     */
    public Option<ImageInfo> getBanner()
    {
        return _embedded.banner;
    }

    @Override
    public Option<ImageInfo> getLogo()
    {
        return _embedded.logo;
    }
    
    @Override
    public Iterable<AddonCategorySummary> getCategories()
    {
        return _embedded.categories;
    }

    @Override
    public AddonDistributionSummary getDistribution()
    {
        return _embedded.distribution;
    }

    @Override
    public AddonReviewsSummary getReviews()
    {
        return _embedded.reviews;
    }

    @Override
    public Option<VendorSummary> getVendor()
    {
        return _embedded.vendor;
    }

    /**
     * Details of one of the add-on's versions. This will be present if 1. you have requested
     * it in an add-on query (see {@link com.atlassian.marketplace.client.api.AddonQuery.Builder#withVersion(boolean)}),
     * in which case it will be the latest available version that matches whatever criteria you have specified;
     * or 2. you have constructed it yourself because you are creating a new add-on
     * (see {@link ModelBuilders.AddonBuilder#version}).
     */
    public Option<AddonVersion> getVersion()
    {
        return _embedded.version;
    }

    /**
     * The address of a Marketplace page that describes the add-on's support details.
     */
    public Option<URI> getSupportDetailsPageUri()
    {
        return getLinks().getUri("support", WEB_TYPE);
    }

    /**
     * A description of the add-on that can be longer than {@link AddonBase#getSummary()}
     * and can include some limited HTML markup. This is no longer supported for new add-on
     * listings, but may be present for older ones. 
     */
    public Option<HtmlString> getDescription()
    {
        for (LegacyProperties l: legacy)
        {
            return l.description;
        }
        return none();
    }
    
    /**
     * Returns one of the optional vendor-specified external links for the add-on.
     * @param type specifies which type of link to get
     * @return the link URI, or {@link Option#none()} if there is no such link
     */
    public Option<URI> getExternalLinkUri(AddonExternalLinkType type)
    {
        if (type.canSetForNewAddons())
        {
            return option(vendorLinks.get(type.getKey()));
        }
        else
        {
            for (LegacyProperties l: legacy)
            {
                return option(l.vendorLinks.get(type.getKey()));
            }
            return none();
        }
    }
    
    /**
     * True if Marketplace should provide a link to an Atlassian Answers tag devoted to this add-on.
     * Can be omitted for private add-ons.
     */
    public Option<Boolean> isEnableAtlassianAnswers()
    {
        return enableAtlassianAnswers;
    }
    
    static final class Embedded
    {
        Option<ImageInfo> banner;
        Option<ImageInfo> logo;
        ImmutableList<AddonCategorySummary> categories;
        AddonDistributionSummary distribution;
        AddonReviewsSummary reviews;
        Option<VendorSummary> vendor;
        Option<AddonVersion> version;
    }

    static final class LegacyProperties
    {
        Option<HtmlString> description;
        Map<String, URI> vendorLinks;
    }
}
