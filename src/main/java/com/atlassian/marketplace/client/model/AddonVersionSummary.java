package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;

/**
 * A more concise representation of an {@link AddonVersion} when it is embedded in another response.
 * @see com.atlassian.marketplace.client.api.Addons
 * @since 2.0.0
 */
public final class AddonVersionSummary extends AddonVersionBase
{
    Embedded _embedded;
    
    public Option<ArtifactInfo> getArtifactInfo()
    {
        return _embedded.artifact;
    }
    
    public Option<URI> getArtifactUri()
    {
        for (ArtifactInfo a: _embedded.artifact)
        {
            return some(a.getBinaryUri());
        }
        return none();
    }

    @Override
    public Iterable<AddonCategorySummary> getFunctionalCategories()
    {
        return _embedded.functionalCategories;
    }

    public Option<URI> getRemoteDescriptorUri()
    {
        for (ArtifactInfo a: _embedded.artifact)
        {
            return a.getRemoteDescriptorUri();
        }
        return none();
    }

    static final class Embedded
    {
        Option<ArtifactInfo> artifact;
        ImmutableList<AddonCategorySummary> functionalCategories;
    }
}
