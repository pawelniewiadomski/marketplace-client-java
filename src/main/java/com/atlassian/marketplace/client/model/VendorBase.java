package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.VendorId;

/**
 * Properties that exist in both {@link Vendor} and {@link VendorSummary}.
 * @since 2.0.0
 */
public abstract class VendorBase implements Entity
{
    Links _links;
    String name;
    @ReadOnly Option<String> verifiedStatus;
    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "alternate") URI alternateUri;
    
    @Override
    public Links getLinks()
    {
        return _links;
    }

    /**
     * The unique resource identifier for the vendor.
     * @see AddonBase#getVendorId()
     * @see ModelBuilders.AddonBuilder#vendor(VendorId)
     * @see com.atlassian.marketplace.client.api.Vendors#getById(VendorId)
     */
    public VendorId getId()
    {
        return VendorId.fromUri(selfUri);
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }
    
    /**
     * The address of a Marketplace page that shows details of the vendor.
     */
    public URI getAlternateUri()
    {
        return alternateUri;
    }
    
    /**
     * The vendor name.
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * The vendor logo.
     */
    public abstract Option<ImageInfo> getLogo();

    /**
     * True if the vendor has Atlassian Verified status.
     */
    public boolean isVerified()
    {
        return "verified".equalsIgnoreCase(verifiedStatus.getOrElse(""));
    }
}
