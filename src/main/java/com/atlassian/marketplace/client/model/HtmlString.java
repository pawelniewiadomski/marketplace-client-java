package com.atlassian.marketplace.client.model;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

/**
 * Simple value wrapper for a string that can contain HTML markup.  Marketplace allows only a
 * limited subset of HTML tags and will remove any unsupported tags.  Plain text newlines will
 * be treated as whitespace rather than line breaks (as usual in HTML), so to add a paragraph
 * break you should use {@code <p>}.
 * @since 2.0.0
 */
public class HtmlString
{
    private final String value;
    
    private HtmlString(String value)
    {
        this.value = checkNotNull(value);
    }
    
    public static HtmlString html(String value)
    {
        return new HtmlString(value);
    }
    
    public String getHtml()
    {
        return value;
    }
    
    @Override
    public String toString()
    {
        return "Html(" + escapeHtml(value) + ")";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof HtmlString) && ((HtmlString) other).value.equals(value);
    }
    
    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
    
    public static Function<HtmlString, String> htmlToString()
    {
        return new Function<HtmlString, String>()
        {
            @Override
            public String apply(HtmlString input)
            {
                return input.getHtml();
            }
        };
    }

    public static Function<String, HtmlString> stringToHtml()
    {
        return new Function<String, HtmlString>()
        {
            @Override
            public HtmlString apply(String input)
            {
                return html(input);
            }
        };
    }
}
