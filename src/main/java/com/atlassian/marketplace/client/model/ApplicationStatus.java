package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates whether an {@link Application} is publicly visible or not.
 * @since 2.0.0
 */
public enum ApplicationStatus implements EnumWithKey
{
    /**
     * The application is visible to everyone.
     */
    PUBLISHED("published"),
    /**
     * The application is visible only to Atlassian administrators.
     */
    UNPUBLISHED("unpublished");
    
    private final String key;
    
    private ApplicationStatus(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
