package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * One of the "highlight" image/text sections that can be part of {@link AddonVersion} details.
 * @since 2.0.0
 */
public final class Highlight
{
    Links _links;
    @ReadOnly Embedded _embedded;
    String title;
    HtmlString body;
    Option<String> explanation;

    public Links getLinks()
    {
        return _links;
    }

    /**
     * Descriptive heading to describe a highlight image.
     */
    public String getTitle()
    {
       return title;
    }

    /**
     * Longer description with additional information about a highlight image.
     */
    public HtmlString getBody()
    {
        return body;
    }
    
    /**
     * Caption that will be displayed in the image gallery when viewing the image.
     */
    public Option<String> getExplanation()
    {
        return explanation;
    }

    /**
     * The full-sized image for the highlight.
     */
    public ImageInfo getFullImage()
    {
        return _embedded.screenshot;
    }

    /**
     * The reduced-size image for the highlight.
     */
    public ImageInfo getThumbnailImage()
    {
        return _embedded.thumbnail;
    }
    
    static final class Embedded
    {
        ImageInfo screenshot;
        ImageInfo thumbnail;
    }
}
