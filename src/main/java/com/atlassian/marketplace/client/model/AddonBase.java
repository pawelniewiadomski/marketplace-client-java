package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.AddonCategoryId;
import com.atlassian.marketplace.client.api.PricingType;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.api.VendorId;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.model.Links.REST_TYPE;
import static com.atlassian.marketplace.client.model.Links.WEB_TYPE;
import static com.google.common.collect.Iterables.transform;

/**
 * Properties that exist in both {@link Addon} and {@link AddonSummary}.
 * @since 2.0.0
 */
public abstract class AddonBase implements Entity
{
    Links _links;
    String name;
    String key;
    AddonStatus status;
    Option<String> summary;
    Option<String> tagLine;
    @Deprecated @ReadOnly Option<Integer> cloudFreeUsers;

    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "alternate") URI alternateUri;
    @RequiredLink(rel = "vendor") URI vendorUri;

    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }

    /**
     * The address of a Marketplace page that shows details of the add-on.
     */
    public URI getAlternateUri()
    {
        return alternateUri;
    }
    
    /**
     * A unique resource identifier for the add-on's vendor.
     * @see #getVendor()
     * @see com.atlassian.marketplace.client.api.Vendors#getById(VendorId)
     * @see ModelBuilders.AddonBuilder#vendor(VendorId)
     */
    public VendorId getVendorId()
    {
        return VendorId.fromUri(vendorUri);
    }

    /**
     * The name of the add-on.
     * @see ModelBuilders.AddonBuilder#name(String)
     */
    public String getName()
    {
        return name;
    }

    /**
     * The unique key of the add-on.
     * @see com.atlassian.marketplace.client.api.Addons#getByKey
     * @see ModelBuilders.AddonBuilder#key(String)
     */
    public String getKey()
    {
        return key;
    }

    /**
     * Indicates whether the add-on is public, private, or pending approval.
     * @see ModelBuilders.AddonBuilder#status(AddonStatus) 
     */
    public AddonStatus getStatus()
    {
        return status;
    }
    
    /**
     * Summary of the add-on's functionality. This is optional for private add-ons.
     * @see ModelBuilders.AddonBuilder#summary(Option)
     */
    public Option<String> getSummary()
    {
        return summary;
    }

    /**
     * A short phrase that summarizes what the add-on does. This is optional for private add-ons.
     * @see ModelBuilders.AddonBuilder#tagLine(Option)
     */
    public Option<String> getTagLine()
    {
        return tagLine;
    }

    /**
     * @deprecated will be removed in next major version. Will always return a none as there are never any cloud free users.
     */
    @Deprecated
    public Option<Integer> getCloudFreeUsers()
    {
        return cloudFreeUsers;
    }

    /**
     * Returns a resource URI for retrieving the add-on's current pricing. You can instead access
     * pricing directly with {@link com.atlassian.marketplace.client.api.Addons#getPricing(String, PricingType)};
     * use {@code getPricingUri} only if you need to pass the resource link to other code, for instance in order
     * to get pricing asynchronously from the front end.
     * @param pricingType  specifies whether to query Cloud or Server pricing
     * @return  link to the pricing resource, if pricing is available for the specified hosting type; otherwise
     *   {@link Option#none()}
     */
    public Option<URI> getPricingUri(final PricingType pricingType)
    {
        return getLinks().getUriTemplate("pricing", REST_TYPE).map(new Function<UriTemplate, URI>()
        {
            public URI apply(UriTemplate t)
            {
                return t.resolve(ImmutableMap.of("cloudOrServer", pricingType.getKey(),
                    "liveOrPending", "live"));
            }
        });
    }
    
    /**
     * The address of a Marketplace page that shows details of the add-on's pricing.
     */
    public Option<URI> getPricingDetailsPageUri()
    {
        return getLinks().getUri("pricing", WEB_TYPE);
    }

    /**
     * The address of a Marketplace page that shows details of the add-on's reviews.
     */
    public Option<URI> getReviewDetailsPageUri()
    {
        return getLinks().getUri("reviews", WEB_TYPE);
    }
    
    /**
     * Same as {@link #getCategories()}, but returns only the {@link AddonCategoryId}
     * rather than the rest of the category description.
     * @see ModelBuilders.AddonBuilder#categories(Iterable)
     */
    public Iterable<AddonCategoryId> getCategoryIds()
    {
        return transform(getLinks().getLinks("categories"), new Function<Link, AddonCategoryId>()
            {
                public AddonCategoryId apply(Link l)
                {
                    return AddonCategoryId.fromUri(l.getUri());
                }
            });
    }
    
    /**
     * Returns summaries of all logical categories that this add-on belongs to. These are assigned
     * by the vendor.
     * @see AddonVersionBase#getFunctionalCategories()
     * @see #getCategoryIds()
     */
    public abstract Iterable<AddonCategorySummary> getCategories();

    /**
     * Summary information about the add-on's download/install counts.
     */
    public abstract AddonDistributionSummary getDistribution();

    /**
     * The add-on logo.
     * @see ModelBuilders.AddonBuilder#logo
     */
    public abstract Option<ImageInfo> getLogo();

    /**
     * Summary information about the add-on's reviews.
     */
    public abstract AddonReviewsSummary getReviews();

    /**
     * Summary information about the add-on's vendor.
     * @see #getVendorId()
     */
    public abstract Option<VendorSummary> getVendor();
}
