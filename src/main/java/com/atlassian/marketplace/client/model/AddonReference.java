package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.VendorId;

/**
 * Minimal data about an add-on listing, even less detailed than {@link AddonSummary}.
 * @see com.atlassian.marketplace.client.api.Addons#findBanners
 * @see com.atlassian.marketplace.client.api.Addons#findRecommendations
 * @since 2.0.0
 */
public class AddonReference implements Entity
{
    Links _links;
    Embedded _embedded;
    String name;
    String key;

    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "alternate") URI alternateUri;
    @RequiredLink(rel = "vendor") URI vendorUri;
    
    @Override
    public Links getLinks()
    {
        return _links;
    }

    /**
     * @see AddonBase#getName()
     */
    public String getName()
    {
        return name;
    }

    /**
     * @see AddonBase#getKey()
     */
    public String getKey()
    {
        return key;
    }

    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }

    /**
     * @see AddonBase#getAlternateUri()
     */
    public URI getAlternateUri()
    {
        return alternateUri;
    }

    /**
     * @see AddonBase#getVendorId()
     */
    public VendorId getVendorId()
    {
        return VendorId.fromUri(vendorUri);
    }

    /**
     * @see AddonBase#getLogo()
     */
    public Option<ImageInfo> getImage()
    {
        return _embedded.image;
    }

    /**
     * @see AddonBase#getReviews()
     */
    public Option<AddonReviewsSummary> getReviews()
    {
        return _embedded.reviews;
    }

    static final class Embedded
    {
        Option<ImageInfo> image;
        Option<AddonReviewsSummary> reviews;
    }
}
