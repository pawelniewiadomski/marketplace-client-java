package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * Standard postal address properties.  Only the first line of the address is required.
 * @since 2.0.0
 */
public class Address
{
    String line1;
    Option<String> line2;
    Option<String> city;
    Option<String> state;
    Option<String> postCode;
    Option<String> country;

    /**
     * The first line of the address.
     * @see ModelBuilders.AddressBuilder#line1(String)
     */
    public String getLine1()
    {
        return line1;
    }

    /**
     * The second line of the address, if any.
     * @see ModelBuilders.AddressBuilder#line2(Option)
     */
    public Option<String> getLine2()
    {
        return line2;
    }

    /**
     * The city name.
     * @see ModelBuilders.AddressBuilder#city(Option)
     */
    public Option<String> getCity()
    {
        return city;
    }

    /**
     * The state or province name.
     * @see ModelBuilders.AddressBuilder#state(Option)
     */
    public Option<String> getState()
    {
        return state;
    }

    /**
     * The postal code, if applicable.
     * @see ModelBuilders.AddressBuilder#postCode(Option)
     */
    public Option<String> getPostCode()
    {
        return postCode;
    }

    /**
     * The ISO-3166 country code.
     * @see ModelBuilders.AddressBuilder#country(Option)
     */
    public Option<String> getCountry()
    {
        return country;
    }
}
