package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * An individual pricing tier within {@link AddonPricing}.
 * @since 2.0.0
 */
public class AddonPricingItem
{
    String description;
    String editionId;
    @ReadOnly String editionDescription;
    LicenseEditionType editionType;
    String licenseType;
    float amount;
    @ReadOnly Option<Float> renewalAmount;
    int unitCount;
    int monthsValid;

    /**
     * Long description of this tier, such as "Addon Name 10 Users: Commercial License".
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Unique identifier for this tier, used internally.
     */
    public String getEditionId()
    {
        return editionId;
    }

    /**
     * Short description of this tier, such as "10 Users".
     */
    public String getEditionDescription()
    {
        return editionDescription;
    }

    /**
     * The type of tier that this is (per-user versus per-role).
     */
    public LicenseEditionType getEditionType()
    {
        return editionType;
    }

    /**
     * The license type that this pricing is for: "commercial", "academic", etc.
     */
    public String getLicenseType()
    {
        return licenseType;
    }

    /**
     * The amount (in USD) that a customer pays for a license at this tier.
     */
    public float getAmount()
    {
        return amount;
    }

    /**
     * The amount (in USD) that a customer with an existing license at this tier pays for a renewal
     * (calculated by Marketplace, cannot be set).
     */
    public Option<Float> getRenewalAmount()
    {
        return renewalAmount;
    }

    /**
     * The user count (or other unit if appropriate, such as remote agents in Bamboo) defining
     * this pricing tier; -1 for unlimited.
     */
    public int getUnitCount()
    {
        return unitCount;
    }

    /**
     * The time period that this pricing is for: 1 for monthly, 12 for annual.
     */
    public int getMonthsValid()
    {
        return monthsValid;
    }
}
