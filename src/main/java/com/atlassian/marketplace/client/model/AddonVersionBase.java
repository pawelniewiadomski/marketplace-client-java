package com.atlassian.marketplace.client.model;

import java.net.URI;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.AddonCategoryId;
import com.atlassian.marketplace.client.api.AddonVersionExternalLinkType;
import com.atlassian.marketplace.client.api.LicenseTypeId;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.joda.time.LocalDate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.model.AddonVersionDataCenterStatus.COMPATIBLE;
import static com.google.common.collect.Iterables.transform;

/**
 * Properties that exist in both {@link AddonVersion} and {@link AddonVersionSummary}.
 * @since 2.0.0
 */
public abstract class AddonVersionBase implements Entity
{
    Links _links;
    Option<String> name;
    AddonVersionStatus status;
    PaymentModel paymentModel;
    boolean staticAddon;
    boolean deployable;
    ReleaseProperties release;
    @ReadOnly DeploymentProperties deployment;
    Map<String, URI> vendorLinks;

    @RequiredLink(rel = "self") URI selfUri;

    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }
    
    /**
     * The version string, e.g. "1.0".  This will always have a value in versions queried from
     * Marketplace, but can usually be omitted when creating a new version.
     * @see ModelBuilders.AddonVersionBuilder#name(String)
     */
    public Option<String> getName()
    {
        return name;
    }
    
    /**
     * Indicates whether the version is free, paid via Atlassian, or paid via Vendor.
     * @see ModelBuilders.AddonVersionBuilder#paymentModel(PaymentModel)
     */
    public PaymentModel getPaymentModel()
    {
        return paymentModel;
    }

    /**
     * Indicates whether the version is public, private, or pending approval.
     * @see ModelBuilders.AddonVersionBuilder#status(AddonVersionStatus)
     */
    public AddonVersionStatus getStatus()
    {
        return status;
    }

    /**
     * True if this is a beta version.
     * @see ModelBuilders.AddonVersionBuilder#beta(boolean)
     */
    public boolean isBeta()
    {
        return release.beta;
    }

    /**
     * True if this is a static add-on (Plugins 1, a deprecated add-on framework).
     */
    public boolean isStatic()
    {
        return staticAddon;
    }
        
    public boolean isSupported()
    {
        return release.supported;
    }

    /**
     * True if this version can be installed directly via the Plugin Manager.
     */
    public boolean isDeployable()
    {
        return deployable;
    }

    /**
     * List of Atlassian Connect scopes used by the version, if it is an Atlassian Connect add-on;
     * otherwise an empty list.
     */
    public Iterable<ConnectScope> getConnectScopes()
    {
        return deployment.permissions.getOrElse(ImmutableList.<ConnectScope>of());
    }

    /**
     * True if this version is compatible with Server instances.
     */
    public boolean isServer()
    {
        return deployment.server;
    }

    /**
     * True if this version is compatible with Cloud instances.
     */
    public boolean isCloud()
    {
        return deployment.cloud;
    }

    /**
     * True if automatic updates are allowed for the version. This is always true for Atlassian
     * Connect add-ons, and is enabled selectively by Atlassian for a small subset of other add-ons.
     */
    public boolean isAutoUpdateAllowed()
    {
        return deployment.autoUpdateAllowed;
    }
    
    /**
     * True if this is an Atlassian Connect add-on.
     */
    public boolean isConnect()
    {
        return deployment.connect;
    }

    /**
     * True if this version is compatible with Data Center instances.
     */
    @Deprecated
    public boolean isDataCenterCompatible()
    {
        return deployment.dataCenter;
    }

    /**
     * Indicates the Data Center approval status
     * @since 2.1.0
     */
    public Option<AddonVersionDataCenterStatus> getDataCenterStatus()
    {
        return deployment.dataCenterStatus;
    }

    /**
     * True if this version is compatible with Data Center instances.
     * @since 2.1.0
     */
    public boolean isDataCenterStatusCompatible()
    {
        return deployment.dataCenterStatus.exists(COMPATIBLE::equals);
    }
    
    /**
     * The unique resource identifier for the software license type used by this version,
     * if any. This is required for public versions, optional for private versions.
     * @see ModelBuilders.AddonVersionBuilder#licenseTypeId(Option)
     */
    public Option<LicenseTypeId> getLicenseTypeId()
    {
        for (URI uri: getLinks().getUri("license"))
        {
            return some(LicenseTypeId.fromUri(uri));
        }
        return none();
    }
    
    /**
     * The date on which this version was released.
     * @see ModelBuilders.AddonVersionBuilder#releaseDate(LocalDate)
     */
    public LocalDate getReleaseDate()
    {
        return release.date;
    }

    /**
     * The name of the person who released this version.
     * @see ModelBuilders.AddonVersionBuilder#releasedBy(Option)
     */
    public Option<String> getReleasedBy()
    {
        return release.releasedBy;
    }
    
    /**
     * Details about the installable file for this add-on, if it has one that is hosted by
     * Marketplace.
     * @see ModelBuilders.AddonVersionBuilder#artifact(Option)
     */
    public abstract Option<ArtifactInfo> getArtifactInfo();
    
    /**
     * Shortcut for getting the download URI for the add-on's installable file, if any.
     * @see #getArtifactInfo()
     */
    public abstract Option<URI> getArtifactUri();

    /**
     * Shortcut for getting the remote Atlassian Connect descriptor URI for the add-on, if any.
     * @see #getArtifactInfo()
     */
    public abstract Option<URI> getRemoteDescriptorUri();

    /**
     * Same as {@link #getFunctionalCategories()}, but returns only the {@link AddonCategoryId}
     * rather than the rest of the category description.
     */
    public Iterable<AddonCategoryId> getFunctionalCategoryIds()
    {
        return transform(getLinks().getLinks("functionalCategories"), new Function<Link, AddonCategoryId>()
            {
                public AddonCategoryId apply(Link l)
                {
                    return AddonCategoryId.fromUri(l.getUri());
                }
            });
    }

    /**
     * Returns one of the optional vendor-specified external links for the add-on version.
     * @param type specifies which type of link to get
     * @return the link URI, or {@link Option#none()} if there is no such link
     */
    public Option<URI> getExternalLinkUri(AddonVersionExternalLinkType type)
    {
        if (type.canSetForNewAddonVersions())
        {
            return option(vendorLinks.get(type.getKey()));
        }
        return none();
    }

    /**
     * Returns summaries of all functional categories that this add-on version belongs to. A
     * functional category - unlike the logical categories returned by
     * {@link AddonBase#getCategories()} - describes a specific kind of feature integration
     * based on the software structure of the add-on (for instance, the types of modules it
     * provides); this is computed by Marketplace, not set by the vendor. Examples of
     * functional categories include macros in Confluence and workflows in JIRA.
     */
    public abstract Iterable<AddonCategorySummary> getFunctionalCategories();

    static final class DeploymentProperties
    {
        Boolean server;
        Boolean cloud;
        Boolean connect;
        Boolean autoUpdateAllowed;
        Option<ImmutableList<ConnectScope>> permissions;
        // dataCenter will be replaced by dataCenterStatus
        @Deprecated
        Boolean dataCenter; 
        Option<AddonVersionDataCenterStatus> dataCenterStatus;
    }

    static final class ReleaseProperties
    {
        LocalDate date;
        Option<String> releasedBy;
        Boolean beta;
        Boolean supported;
    }
}
