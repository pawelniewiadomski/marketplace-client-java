package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates whether an {@link ApplicationVersion} is publicly visible or not.
 * @since 2.0.0
 */
public enum ApplicationVersionStatus implements EnumWithKey
{
    /**
     * The version is visible to everyone.
     */
    PUBLISHED("published"),
    /**
     * The version is visible only to Atlassian administrators.
     */
    UNPUBLISHED("unpublished");
    
    private final String key;
    
    private ApplicationVersionStatus(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
