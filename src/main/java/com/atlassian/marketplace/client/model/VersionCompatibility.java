package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.HostingType;

import com.google.common.base.Predicate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.fugue.Pair.pair;

/**
 * A range of application versions compatible with an add-on version or product version.
 * @since 2.0.0
 */
public final class VersionCompatibility
{
    ApplicationKey application;
    CompatibilityHosting hosting;
    
    VersionCompatibility(ApplicationKey application, CompatibilityHosting hosting)
    {
        this.application = application;
        this.hosting = hosting;
    }
    
    static final class CompatibilityHosting
    {
        Option<CompatibilityHostingBounds> server;
        Option<Boolean> cloud;

        CompatibilityHosting(Option<CompatibilityHostingBounds> server, Option<Boolean> cloud)
        {
            this.server = server;
            this.cloud = cloud;
        }

        boolean isServerCompatible()
        {
            return server.isDefined();
        }
        
        boolean isCloudCompatible()
        {
            return cloud.getOrElse(false);
        }
    }
    
    static final class CompatibilityHostingBounds
    {
        VersionPoint min;
        VersionPoint max;
        
        CompatibilityHostingBounds(VersionPoint min, VersionPoint max)
        {
            this.min = min;
            this.max = max;
        }
    }
    
    static final class VersionPoint
    {
        int build;
        Option<String> version;
        
        VersionPoint(int build, Option<String> version)
        {
            this.build = build;
            this.version = version;
        }
    }

    /**
     * The application key.
     */
    public ApplicationKey getApplication()
    {
        return application;
    }

    /**
     * Returns true if the version is compatible with Atlassian Cloud instances.
     */
    public boolean isCloudCompatible()
    {
        return hosting.isCloudCompatible();
    }

    /**
     * Returns true if the version is compatible with Atlassian Server instances.
     */
    public boolean isServerCompatible()
    {
        return hosting.isServerCompatible();
    }

    /**
     * The minimum and maximum application build numbers with which the version is compatible
     * in a Server installation. 
     */
    public Option<Pair<Integer, Integer>> getServerBuildRange()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(pair(b.min.build, b.max.build));
        }
        return none();
    }
    
    public boolean isInServerBuildRange(int build)
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return build >= b.min.build && build <= b.max.build;
        }
        return false;
    }
    
    public Option<Integer> getServerMinBuild()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(b.min.build);
        }
        return none();
    }
    
    public Option<Integer> getServerMaxBuild()
    {
        for (CompatibilityHostingBounds b: hosting.server)
        {
            return some(b.max.build);
        }
        return none();
    }
    
    public boolean isCompatibleWith(Predicate<ApplicationKey> applicationCriteria,
                                    HostingType hostingType, int build)
    {
        return applicationCriteria.apply(this.getApplication()) &&
                ((hostingType == HostingType.CLOUD && hosting.isCloudCompatible()) ||
                 (hostingType != HostingType.CLOUD && hosting.isServerCompatible() &&
                    isInServerBuildRange(build)));
    }

    public static Predicate<VersionCompatibility> compatibleWith(final Predicate<ApplicationKey> applicationCriteria,
        final HostingType hostingType, final int build)
    {
        return new Predicate<VersionCompatibility>()
        {
            @Override
            public boolean apply(VersionCompatibility c)
            {
                return c.isCompatibleWith(applicationCriteria, hostingType, build);
            }
        };
    }
}
