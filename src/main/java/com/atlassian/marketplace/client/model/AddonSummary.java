package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;

/**
 * A more concise representation of an {@link Addon} when it is embedded in another response.
 * @since 2.0.0
 */
public class AddonSummary extends AddonBase
{
    Embedded _embedded;

    @Override
    public Iterable<AddonCategorySummary> getCategories()
    {
        return _embedded.categories;
    }

    @Override
    public AddonDistributionSummary getDistribution()
    {
        return _embedded.distribution;
    }

    @Override
    public Option<ImageInfo> getLogo()
    {
        return _embedded.logo;
    }
    
    @Override
    public AddonReviewsSummary getReviews()
    {
        return _embedded.reviews;
    }

    @Override
    public Option<VendorSummary> getVendor()
    {
        return _embedded.vendor;
    }

    /**
     * Details of one of the add-on's versions. This will be present if you have requested
     * it in an add-on query (see {@link com.atlassian.marketplace.client.api.AddonQuery.Builder#withVersion(boolean)}),
     * in which case it will be the latest available version that matches whatever criteria you have specified.
     */
    public Option<AddonVersionSummary> getVersion()
    {
        return _embedded.version;
    }

    static final class Embedded
    {
        ImmutableList<AddonCategorySummary> categories;
        AddonDistributionSummary distribution;
        Option<ImageInfo> logo;
        AddonReviewsSummary reviews;
        Option<VendorSummary> vendor;
        Option<AddonVersionSummary> version;
    }
}
