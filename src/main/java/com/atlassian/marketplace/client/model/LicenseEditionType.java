package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Describes the type of license edition for an {@link AddonPricingItem}.
 * @since 2.0.0
 */
public enum LicenseEditionType implements EnumWithKey
{
    USER_TIER("user-tier"),
    ROLE_TIER("role-tier"),
    REMOTE_AGENT_COUNT("remote-agent-count");

    private final String key;

    private LicenseEditionType(String key)
    {
        this.key = key;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    public static LicenseEditionType getDefault()
    {
        return USER_TIER;
    }
}
