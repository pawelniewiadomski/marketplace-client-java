package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates which payment system, if any, is used for buying licenses for an add-on.
 * @since 2.0.0
 */
public enum PaymentModel implements EnumWithKey
{
    /**
     * The add-on does not require purchase.
     */
    FREE("free"),
    /**
     * The add-on can be purchased through the vendor.
     */
    PAID_VIA_VENDOR("vendor"),
    /**
     * The add-on can be purchased through the Atlassian Marketplace.
     */
    PAID_VIA_ATLASSIAN("atlassian");
    
    private final String key;
    
    private PaymentModel(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
