package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * Details of an error returned by the server.
 * @since 2.0.0
 */
public class ErrorDetail
{
    String message;
    Option<String> code;
    Option<String> path;
    
    /**
     * The error message. For errors related to a specific request property, the message will
     * normally be a fragment such as "required" or "cannot be zero" rather than a complete
     * sentence; refer to {@link #getPath()} to see which property was involved. 
     */
    public String getMessage()
    {
        return message;
    }
    
    /**
     * The error code. If present, this string is a stable identifier for some specific type
     * of error, which will not change even if the message text changes.
     */
    public Option<String> getCode()
    {
        return code;
    }
    
    /**
     * The path to a request property that was involved in the error, if any.  This uses
     * JSON Pointer format, and refers to the JSON representation of the request: so, for
     * instance, a top-level property called "name" would have a path of "/name", and the
     * link URI of a "vendor" link would have a path of "/_links/vendor/href".
     */
    public Option<String> getPath()
    {
        return path;
    }
    
    @Override
    public String toString()
    {
        for (String p: path)
        {
            return p + ": " + message;
        }
        return message;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof ErrorDetail)
        {
            ErrorDetail o = (ErrorDetail) other;
            return message.equals(o.message) && code.equals(o.code) && path.equals(o.path);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return message.hashCode() + code.hashCode() + path.hashCode();
    }
}
