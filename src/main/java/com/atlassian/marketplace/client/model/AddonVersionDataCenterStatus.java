package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.EnumWithKey;

/**
 * Indicates whether an {@link AddonVersion} is Data Center (DC) compatible, legacy DC compatible,
 *      or pending DC approval.
 * @since 2.1.0
 */
public enum AddonVersionDataCenterStatus implements EnumWithKey
{
    /**
     * The version is Data Center compatible.
     */
    COMPATIBLE("compatible"),
    /**
     * The version has the legacy Data Center compatibility flag
     */
    LEGACY("legacy"),
    /**
     * The version's Data Center compatibility status is pending approval.
     */
    PENDING("pending"),
    /**
     * The version's Data Center compatibility status has been rejected.
     */
    REJECTED("rejected");

    private final String key;

    private AddonVersionDataCenterStatus(String key)
    {
        this.key = key;
    }

    @Override
    public String getKey()
    {
        return key;
    }
}
