package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * A concise representation of a {@link Vendor} when it is embedded in another response.
 * @see com.atlassian.marketplace.client.api.Vendors#find
 * @since 2.0.0
 */
public final class VendorSummary extends VendorBase
{
    Embedded _embedded;
    
    @Override
    public Option<ImageInfo> getLogo()
    {
        return _embedded.logo;
    }

    static final class Embedded
    {
        Option<ImageInfo> logo;
    }
}
