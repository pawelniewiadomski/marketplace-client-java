package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.LicenseTypeId;

/**
 * Information about a type of software license.
 * @since 2.0.0
 */
public final class LicenseType implements Entity
{
    Links _links;
    String key;
    String name;

    @RequiredLink(rel = "self") URI selfUri;
    
    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }

    /**
     * The unique resource identifier for the license type.
     * @see AddonVersionBase#getLicenseTypeId()
     * @see ModelBuilders.AddonVersionBuilder#licenseTypeId(Option)
     */
    public LicenseTypeId getId()
    {
        return LicenseTypeId.fromUri(selfUri);
    }
    
    /**
     * Address of a web page that describes the license type.
     */
    public Option<URI> getAlternateUri()
    {
        return _links.getUri("alternate");
    }
    
    /**
     * A short string identifier that Marketplace uses to describe this license type, such as "gpl".
     * @see com.atlassian.marketplace.client.api.LicenseTypes#getByKey(String)
     */
    public String getKey()
    {
        return key;
    }
    
    /**
     * The name of the license type, such as "GNU Public License (GPL)".
     */
    public String getName()
    {
        return name;
    }
}
