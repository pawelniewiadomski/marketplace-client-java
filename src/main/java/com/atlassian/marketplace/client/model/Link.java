package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.UriTemplate;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang.ObjectUtils;

import static com.atlassian.marketplace.client.model.Links.REST_TYPE;

/**
 * A resource/web hyperlink in the HAL-JSON format used by the Marketplace version 2 API.
 * A HAL link can be either a URI or a a {@link UriTemplate}.
 */
public final class Link
{
    private final Option<String> type;
    final Either<UriTemplate, URI> templateOrUri;

    public Link(Either<UriTemplate, URI> templateOrUri, Option<String> type)
    {
        this.templateOrUri = templateOrUri;
        this.type = type;
    }

    public static Link fromUri(URI uri, Option<String> type)
    {
        return new Link(Either.<UriTemplate, URI>right(uri), type);
    }
    
    public static Link fromUriTemplate(UriTemplate ut, Option<String> type)
    {
        return new Link(Either.<UriTemplate, URI>left(ut), type);
    }
    
    /**
     * Returns the link URI (the "href" attribute) if it is a URI; if it is a template,
     * converts it to a URI by removing all variable parameters.
     */
    public URI getUri()
    {
        return templateOrUri.fold(
            new Function<UriTemplate, URI>()
            {
                public URI apply(UriTemplate t)
                {
                    return t.resolve(ImmutableMap.<String, String>of());
                }
            },
            Functions.<URI>identity()
        );
    }
    
    /**
     * Returns the link URI template if it is a template, or {@link Option#none()} if it is a
     * normal URI.
     */
    public Option<UriTemplate> getUriTemplate()
    {
        return templateOrUri.left().toOption();
    }
    
    public Either<UriTemplate, URI> getTemplateOrUri()
    {
        return templateOrUri;
    }
    
    /**
     * Returns the optional "type" attribute, indicating the media type of the linked resource.
     * If this is {@link Option#none()}, assume that the resource contains JSON.
     */
    public Option<String> getType()
    {
        return type;
    }
    
    public boolean matchType(Option<String> desiredType)
    {
        for (String dt: desiredType)
        {
            return type == null ? dt.equals(REST_TYPE) : dt.equals(type); 
        }
        return true;
    }
    
    /**
     * Returns the link URI or link template as a string.
     */
    public String stringValue()
    {
        for (UriTemplate ut: getUriTemplate())
        {
            return ut.getValue();
        }
        return getUri().toASCIIString();
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof Link)
        {
            Link o = (Link) other;
            return templateOrUri.equals(o.templateOrUri) &&
                ObjectUtils.equals(type, o.type);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return templateOrUri.hashCode() + ((type == null) ? 0 : type.hashCode());
    }
}