package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.HostingType;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import org.joda.time.LocalDate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.any;

/**
 * Information about a specific version of a {@link Product}.
 * @since 2.0.0
 */
public final class ProductVersion
{
    Links _links;
    Embedded _embedded;
    String name;
    int buildNumber;
    PaymentModel paymentModel;
    LocalDate releaseDate;
    ImmutableList<VersionCompatibility> compatibilities;

    public Option<URI> getArtifactUri()
    {
        for (ArtifactInfo a: _embedded.artifact)
        {
            return some(a.getBinaryUri());
        }
        return none();
    }
    
    public Option<URI> getLearnMoreUri()
    {
        return _links.getUri("view");
    }

    public Option<URI> getReleaseNotesUri()
    {
        return _links.getUri("releaseNotes");
    }
    
    /**
     * The version string, e.g. "1.0".
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * The unique integer that identifies this version and defines its ordering relative to other versions.
     */
    public int getBuildNumber()
    {
        return buildNumber;
    }
    
    /**
     * Indicates whether the version is free or paid via Atlassian.
     */
    public PaymentModel getPaymentModel()
    {
        return paymentModel;
    }
    
    public LocalDate getReleaseDate()
    {
        return releaseDate;
    }
    
    /**
     * Returns a list of {@link VersionCompatibility} objects representing each of the
     * {@link Application}s the product version is compatible with, and the compatible
     * application version range.
     */
    public Iterable<VersionCompatibility> getCompatibilities()
    {
        return compatibilities;
    }
    
    public boolean isCompatibleWith(final ApplicationKey application, final HostingType hosting, final int buildNumber)
    {
        return any(compatibilities, new Predicate<VersionCompatibility>()
        {
            public boolean apply(VersionCompatibility vc)
            {
                return vc.isCompatibleWith(equalTo(application), hosting, buildNumber);
            }
        });
    }
    
    static final class Embedded
    {
        Option<ArtifactInfo> artifact;
    }
}
