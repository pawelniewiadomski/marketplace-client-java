package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

import java.net.URI;

public class SupportOrganization {
    String name;
    Option<String> supportEmail;
    Option<URI> supportUrl;
    Option<String> supportPhone;

    public String getName()
    {
        return name;
    }

    public Option<String> getSupportEmail()
    {
        return supportEmail;
    }

    public Option<URI> getSupportUrl()
    {
        return supportUrl;
    }

    public Option<String> getSupportPhone()
    {
        return supportPhone;
    }
}
