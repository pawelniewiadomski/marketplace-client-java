package com.atlassian.marketplace.client.model;

import java.net.URI;

import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.none;

/**
 * Represents an image hosted by Marketplace, such as an add-on logo.
 * @since 2.0.0
 */
public final class ImageInfo implements Entity
{
    Links _links;
    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "image") URI imageUri;
    
    /**
     * Image size constants for use with {@link ImageInfo#getImageUri(Size, Resolution)}).
     */
    public static enum Size
    {
        /**
         * Represents the default size for whatever type of image this is.  For instance, for an add-on
         * logo the default size is 72x72px at standard resolution.
         */
        DEFAULT_SIZE,
        /**
         * Represents a standard smaller size that may be defined for some types of images.  For instance,
         * for an add-on logo the small size is 16x16px at standard resolution.
         */
        SMALL_SIZE
    }
    
    /**
     * Image resolution constants for use with {@link ImageInfo#getImageUri(Size, Resolution)}).
     */
    public static enum Resolution
    {
        /**
         * Represents the default resolution for the image.
         */
        DEFAULT_RESOLUTION,
        /**
         * Represents a higher-resolution version of the image that may or may not be available.  This
         * normally is twice as wide and twice as high in pixel count.
         */
        HIGH_RESOLUTION
    }
    
    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    /**
     * Returns the URI of the image at its default size, for displaying at standard resolution.
     */
    public URI getImageUri()
    {
        return imageUri;
    }
    
    /**
     * Returns a URI for the image at a different size, or for displaying at higher resolution.
     * This may not exist for all images.
     */
    public Option<URI> getImageUri(Size size, Resolution resolution)
    {
        return _links.getUri(getImageLinkRel(size, resolution));
    }
    
    /**
     * Returns the content type for the image, if known.
     */
    public Option<String> getImageContentType(Size size, Resolution resolution)
    {
        for (Link link: _links.getLink(getImageLinkRel(size, resolution)))
        {
            return link.getType();
        }
        return none();
    }
    
    /**
     * Returns the self link to the image resource in the {@link com.atlassian.marketplace.client.api.Assets}
     * API.  This is not the URI of the image itself.
     */
    public URI getSelfUri()
    {
        return selfUri;
    }
    
    static String getImageLinkRel(Size size, Resolution resolution)
    {
        if (resolution == Resolution.HIGH_RESOLUTION)
        {
            return (size == Size.SMALL_SIZE) ? "smallHighResImage" : "highRes";
        }
        else
        {
            return (size == Size.SMALL_SIZE) ? "smallImage" : "image";
        }
    }
}
