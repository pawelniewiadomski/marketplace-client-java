package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;

/**
 * Information about the download/install counts for an add-on.
 * @since 2.0.0
 */
public final class AddonDistributionSummary
{
    Boolean bundled;
    Integer downloads;
    Option<Integer> totalInstalls;
    Option<Integer> totalUsers;
    
    /**
     * True if the add-on is a preinstalled component that is always present in applications.
     */
    public boolean isBundled()
    {
        return bundled;
    }
    
    /**
     * The number of times the add-on has been downloaded from Marketplace.
     */
    public int getDownloads()
    {
        return downloads;
    }
    
    public Option<Integer> getTotalInstalls()
    {
        return totalInstalls;
    }
    
    public Option<Integer> getTotalUsers()
    {
        return totalUsers;
    }
}
