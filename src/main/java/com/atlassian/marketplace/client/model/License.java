package com.atlassian.marketplace.client.model;

import com.atlassian.fugue.Option;
import org.joda.time.LocalDate;

/**
 * Details about a license.
 *
 * @since 2.0.0
 */
public final class License
{
    String addonLicenseId;
    Option<String> hostLicenseId;
    String licenseId;
    String addonKey;
    String addonName;
    String hosting;
    LocalDate lastUpdated;
    String licenseType;
    LocalDate maintenanceStartDate;
    LocalDate maintenanceEndDate;
    String status;
    String tier;
    ContactDetails contactDetails;
    Option<PartnerDetails> partnerDetails;

    public String getAddonLicenseId() {
        return addonLicenseId;
    }

    public Option<String> getHostLicenseId() {
        return hostLicenseId;
    }

    public String getLicenseId() {
        return licenseId;
    }

    public String getAddonKey() {
        return addonKey;
    }

    public String getAddonName() {
        return addonName;
    }

    public String getHosting() {
        return hosting;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public LocalDate getMaintenanceStartDate() {
        return maintenanceStartDate;
    }

    public LocalDate getMaintenanceEndDate() {
        return maintenanceEndDate;
    }

    public String getStatus() {
        return status;
    }

    public String getTier() {
        return tier;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public Option<PartnerDetails> getPartnerDetails() {
        return partnerDetails;
    }

    public static final class Contact {
        Option<String> email;
        Option<String> name;
        Option<String> phone;
        Option<String> address1;
        Option<String> address2;
        Option<String> city;
        Option<String> state;
        Option<String> postcode;

        public Option<String> getEmail() {
            return email;
        }

        public Option<String> getName() {
            return name;
        }

        public Option<String> getPhone() {
            return phone;
        }

        public Option<String> getAddress1() {
            return address1;
        }

        public Option<String> getAddress2() {
            return address2;
        }

        public Option<String> getCity() {
            return city;
        }

        public Option<String> getState() {
            return state;
        }

        public Option<String> getPostcode() {
            return postcode;
        }
    }

    public static final class ContactDetails
    {
        Option<String> company;
        String country;
        String region;
        Option<Contact> technicalContact;
        Option<Contact> billingContact;

        public Option<String> getCompany() {
            return company;
        }

        public String getCountry() {
            return country;
        }

        public String getRegion() {
            return region;
        }

        public Option<Contact> getTechnicalContact() {
            return technicalContact;
        }

        public Option<Contact> getBillingContact() {
            return billingContact;
        }
    }

    public static final class PartnerDetails
    {
        String partnerName;
        Option<String> partnerType;
        Option<Contact> billingContact;

        public String getPartnerName() {
            return partnerName;
        }

        public Option<String> getPartnerType() {
            return partnerType;
        }

        public Option<Contact> getBillingContact() {
            return billingContact;
        }
    }
}
