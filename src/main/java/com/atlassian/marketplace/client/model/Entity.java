package com.atlassian.marketplace.client.model;

import java.net.URI;

/**
 * Common properties defined for resource entities in the Marketplace API.
 * @since 2.0.0
 */
public interface Entity
{
    /**
     * The top-level links for the resource.
     */
    public Links getLinks();
    
    /**
     * The "self" link for the resource.  This is the canonical identifier for the entity in the API,
     * and is used whenever linking to this resource from another one.
     */
    public URI getSelfUri();
}
