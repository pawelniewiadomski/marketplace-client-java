package com.atlassian.marketplace.client.model;


import com.atlassian.fugue.Option;

public final class SupportDetails {
    Option<SupportOrganization> supportOrg;
    Option<String> emergencyContact;

    public Option<SupportOrganization> getSupportOrg()
    {
        return supportOrg;
    }

    public Option<String> getEmergencyContact()
    {
        return emergencyContact;
    }
}
