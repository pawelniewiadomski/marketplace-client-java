package com.atlassian.marketplace.client.http;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

/**
 * Allows the caller to attach custom request headers to all client requests.
 * @since 2.0.0
 */
public interface RequestDecorator
{
    /**
     * Returns a map of header names and values that should be added to outgoing
     * Marketplace requests.
     */
    public Map<String, String> getRequestHeaders();
    
    public static abstract class Instances
    {
        /**
         * Returns a {@link RequestDecorator} instance that always adds the same constant header values.
         * @param headers  a map of header names and values
         */
        public static RequestDecorator forHeaders(Map<String, String> headers)
        {
            final ImmutableMap<String, String> immutableHeaders = ImmutableMap.copyOf(headers);
            return new RequestDecorator()
            {
                @Override
                public Map<String,String> getRequestHeaders()
                {
                    return immutableHeaders;
                }
            };
        }
    }
}
