package com.atlassian.marketplace.client.http;

import java.io.Closeable;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.marketplace.client.MpacException;

import com.google.common.collect.Multimap;

/**
 * Abstraction of how to execute HTTP requests against the API.
 * @since 2.0.0
 */
public interface HttpTransport extends Closeable
{
    /**
     * Performs an HTTP GET.
     * @param uri  URI of resource to get
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse get(URI uri) throws MpacException;

    /**
     * Performs an HTTP POST with form parameters.
     * @param uri  URI of resource to post to
     * @param params  the unencoded parameters
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse postParams(URI uri, Multimap<String, String> params) throws MpacException;

    /**
     * Performs an HTTP POST of an API entity.
     * @param uri  URI of resource to post to
     * @param content  input stream containing the request body
     * @param length  length of the input stream
     * @param contentType  content type of the entity
     * @param acceptContentType  expected content type of response
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse post(URI uri, InputStream content, long length, String contentType, String acceptContentType) throws MpacException;

    /**
     * Performs an HTTP PUT of an API entity.  The content type is assumed to be JSON.
     * @param uri  URI of resource to put
     * @param content  request body
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse put(URI uri, byte[] content) throws MpacException;

    /**
     * Performs an HTTP PATCH.  The content type is assumed to be application/json-patch+json.
     * @param uri  URI of resource to put
     * @param content  request body
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse patch(URI uri, byte[] content) throws MpacException;
    
    /**
     * Performs an HTTP DELETE.
     * @param uri  URI of resource to delete
     * @return  a {@link SimpleHttpResponse} wrapping the HTTP response
     * @throws MpacException  if the server was unavailable or returned no response
     */
    SimpleHttpResponse delete(URI uri) throws MpacException;
    
    /**
     * Returns another {@code HttpTransport} instance that behaves the same as this one except that
     * it adds headers from the specified {@link RequestDecorator} to every request (in
     * addition to any headers previously specified in the defaults).  The new instance still
     * uses the same underlying HTTP transport; closing one will close the other.
     * @param decorator  will generate additional headers for requests made via the new instance
     * @return  a new {@link HttpTransport} instance
     */
    HttpTransport withRequestDecorator(RequestDecorator decorator);
}
