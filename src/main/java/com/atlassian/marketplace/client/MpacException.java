package com.atlassian.marketplace.client;

import com.atlassian.marketplace.client.encoding.SchemaViolation;
import com.atlassian.marketplace.client.model.ErrorDetail;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

import org.apache.commons.lang.StringUtils;

/**
 * Base class for all exceptions thrown by {@link MarketplaceClient}.
 */
@SuppressWarnings("serial")
public class MpacException extends Exception
{
    public MpacException()
    {
        super();
    }
    
    public MpacException(String message)
    {
        super(message);
    }
    
    public MpacException(Throwable cause)
    {
        super(cause);
    }
    
    public MpacException(String message, Throwable cause)
    {
        super(message, cause);
    }
    
    /**
     * Indicates that the client was unable to make an HTTP connection to the server, or that
     * a request timed out before a response was received.
     */
    public static class ConnectionFailure extends MpacException
    {
        public ConnectionFailure(Throwable cause)
        {
            super(cause);
        }
    }

    /**
     * Indicates that the server returned an HTTP error status code.
     */
    public static class ServerError extends MpacException
    {
        private final int status;
        private final ImmutableList<ErrorDetail> errorDetails;
        
        public ServerError(int status)
        {
            super("error " + status);
            this.status = status;
            this.errorDetails = ImmutableList.of();
        }
        
        public ServerError(int status, String message)
        {
            super(status + (StringUtils.isBlank(message) ? "" : (": " + message)));
            this.status = status;
            this.errorDetails = ImmutableList.of();
        }
        
        public ServerError(int status, Iterable<ErrorDetail> errorDetails)
        {
            super(status + ": " + Joiner.on(", ").join(errorDetails));
            this.status = status;
            this.errorDetails = ImmutableList.copyOf(errorDetails);
        }
        
        /**
         * The HTTP status code from the response.
         */
        public int getStatus()
        {
            return status;
        }
        
        /**
         * Error details included in the response, if any.
         */
        public Iterable<ErrorDetail> getErrorDetails()
        {
            return errorDetails;
        }
    }

    /**
     * Indicates that the server returned a response that could not be parsed.  This should not
     * normally happen, and may indicate either a bug in the client library or a server-side bug.
     */
    public static class InvalidResponseError extends MpacException
    {
        private ImmutableList<SchemaViolation> schemaViolations;
        
        public InvalidResponseError(Iterable<SchemaViolation> schemaViolations)
        {
            this.schemaViolations = ImmutableList.copyOf(schemaViolations);
        }
        
        public InvalidResponseError(String message)
        {
            super(message);
            this.schemaViolations = ImmutableList.of();
        }

        public InvalidResponseError(String message, Throwable cause)
        {
            super(message, cause);
            this.schemaViolations = ImmutableList.of();
        }
        
        public Iterable<SchemaViolation> getSchemaViolations()
        {
            return schemaViolations;
        }
        
        @Override
        public String getMessage()
        {
            if (super.getMessage() == null)
            {
                return Joiner.on(", ").join(schemaViolations);
            }
            else
            {
                return super.getMessage();
            }
        }
    }
    
    /**
     * Indicates that an {@code update} method was called for an object that did not come from
     * the Marketplace server.
     * <p>
     * This could happen if, for example, you construct an
     * {@link com.atlassian.marketplace.client.model.Addon} instance {@code a0}, pass it to
     * {@link com.atlassian.marketplace.client.api.Addons#createAddon} to create the add-on,
     * and then try to use {@code a0} again with {@link com.atlassian.marketplace.client.api.Addons#updateAddon}
     * to update some property of the add-on; that will not work because the {@code a0} instance
     * is still in a not-yet-created state and does not have a resource URI.  The correct thing
     * to do in that case would be to use the {@code Addon} instance that is <i>returned from</i>
     * {@code createAddon}, which represents the persistent state of the resource after it has
     * been created.  (You could also query the add-on's current state again at any time with
     * {@link com.atlassian.marketplace.client.api.Addons#getByKey}, and use the resulting
     * {@code Addon} instance for your update request.)
     *  
     * @since 2.0.0
     */
    public static class CannotUpdateNonServerSideEntity extends MpacException
    {
        public CannotUpdateNonServerSideEntity()
        {
        }
    }
}
