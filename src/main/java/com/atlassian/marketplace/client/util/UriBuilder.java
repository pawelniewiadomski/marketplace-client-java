package com.atlassian.marketplace.client.util;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simplified wrapper for Apache URIBuilder that gives it the nicer interface of the jax-rs UriBuilder,
 * without having to bring in that dependency. 
 */
public class UriBuilder
{
    private final URIBuilder builder;
    
    private UriBuilder(URIBuilder builder)
    {
        this.builder = checkNotNull(builder);
    }
    
    public static UriBuilder fromUri(URI uri)
    {
        return new UriBuilder(new URIBuilder(checkNotNull(uri)));
    }
    
    public static UriBuilder fromUri(String uri)
    {
        try
        {
            return new UriBuilder(new URIBuilder(checkNotNull(uri)));
        }
        catch (URISyntaxException e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    
    public URI build()
    {
        try
        {
            return builder.build();
        }
        catch (URISyntaxException e)
        {
            throw new IllegalArgumentException(e);
        }
    }
    
    public UriBuilder host(String host)
    {
        builder.setHost(checkNotNull(host));
        return this;
    }
    
    public UriBuilder path(String addPath)
    {
        checkNotNull(addPath);
        String old = builder.getPath();
        if (!old.endsWith("/") && !addPath.startsWith("/"))
        {
            old = old + "/";
        }
        builder.setPath(old + addPath);
        return this;
    }
    
    public UriBuilder queryParam(String name, Object... value)
    {
        for (Object v: checkNotNull(value))
        {
            builder.addParameter(checkNotNull(name), String.valueOf(checkNotNull(v)));
        }
        return this;
    }
}
