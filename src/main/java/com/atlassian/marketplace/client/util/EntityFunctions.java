package com.atlassian.marketplace.client.util;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.model.Entity;
import com.atlassian.marketplace.client.model.Links;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

/**
 * Helper methods for resource entities in the Marketplace v2 API.
 * @since 2.0.0
 */
public abstract class EntityFunctions
{
    private EntityFunctions()
    {
    }
    
    public static <T extends Entity> Function<T, Links> links()
    {
        return new Function<T, Links>()
        {
            public Links apply(T t)
            {
                return t.getLinks();
            }
        };
    }

    public static <T extends Entity> Option<URI> selfUri(T entity)
    {
        return entity.getLinks().getUri("self");
    }
    
    public static <T extends Entity> Function<T, Option<URI>> selfUri()
    {
        return new Function<T, Option<URI>>()
        {
            public Option<URI> apply(T e)
            {
                return selfUri(e);
            }
        };
    }
    
    public static <T extends Entity> Iterable<URI> entityLinks(Iterable<T> entities)
    {
        ImmutableList.Builder<URI> ret = ImmutableList.builder();
        for (T e: entities)
        {
            for (URI u: selfUri(e))
            {
                ret.add(u);
            }
        }
        return ret.build();
    }
}
