package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of pricing for Atlassian applications.
 * @since 2.0.0
 */
public enum PricingType implements EnumWithKey
{
    SERVER("server"),
    
    CLOUD("cloud");
    
    private final String key;
    
    private PricingType(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
