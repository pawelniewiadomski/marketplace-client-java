package com.atlassian.marketplace.client.api;

import java.net.URI;

/**
 * Specifies an image as a property of a model object.  This can be obtained by uploading
 * an image file through the {@link Assets} API, or by providing the URI of an image that
 * is available online. 
 * @since 2.0.0
 */
public final class ImageId extends ResourceId
{
    private ImageId(URI value)
    {
        super(value);
    }

    /**
     * Specifies the URI of an image that is available online.  The Marketplace server
     * will download the image when it creates or updates the object that uses it.
     */
    public static ImageId fromUri(URI uri)
    {
        return new ImageId(uri);
    }
}
