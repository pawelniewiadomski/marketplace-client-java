package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of vendor-specified external links for add-ons.
 * Deprecated types are available only for some old add-on listings and cannot be set for new ones.
 * @since 2.0.0
 */
public enum AddonExternalLinkType
{
    /**
     * A link to a continuous integration server/build system.
     * @deprecated This link cannot be set for new add-on listings, but may be present in old listings.
     */
    @Deprecated
    BUILDS("builds", false),
    
    /**
     * A link to a discussion forum.
     */
    FORUMS("forums", true),
    
    /**
     * A link to an issue tracker.
     */
    ISSUE_TRACKER("issueTracker", true),
    
    /**
     * A link to a privacy policy. This is required for all Atlassian Connect add-ons.
     */
    PRIVACY("privacy", true),
    
    /**
     * A link to a source control repository.
     * @deprecated This link cannot be set for new add-on listings, but may be present in old listings.
     */
    @Deprecated
    SOURCE("source", false),
    
    /**
     * A link to a wiki.
     * @deprecated This link cannot be set for new add-on listings, but may be present in old listings.
     */
    @Deprecated
    WIKI("wiki", false);
    
    private final String key;
    private final boolean canSetForNewAddons;
    
    private AddonExternalLinkType(String key, boolean canSetForNewAddons)
    {
        this.key = key;
        this.canSetForNewAddons = canSetForNewAddons;
    }
    
    /**
     * Returns true if it is possible to set this type of link for new add-on listings.
     */
    public boolean canSetForNewAddons()
    {
        return canSetForNewAddons;
    }
    
    /**
     * The property name that corresponds to this link type within the {@code vendorLinks} or
     * {@code legacy.vendorLinks} property of the add-on's JSON representation.
     */
    public String getKey()
    {
        return key;
    }
}
