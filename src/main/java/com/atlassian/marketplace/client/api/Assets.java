package com.atlassian.marketplace.client.api;

import java.io.File;

import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MarketplaceClientFactory;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.model.ModelBuilders;

/**
 * Allows you to manage file-like data that can be attached to resources, such as a logo
 * for an add-on or vendor, or an artifact for an installable add-on.  Use
 * {@link MarketplaceClient#assets()} to access this API.
 * <p>
 * Asset uploads are managed as follows:
 * <ul>
 * <li> To upload an asset, you must be authenticated as a Marketplace user (that is, when
 * you created your client instance with {@link MarketplaceClientFactory}, you provided
 * an {@link HttpConfiguration} with {@link HttpConfiguration.Credentials}).
 * <li> The Marketplace server may reject uploads if you submit many of them in rapid
 * succession.
 * <li> If an uploaded asset is not used in any model object (for instance, if you upload
 * a logo image, but then do not set it as the logo for an add-on or a vendor), the
 * server will delete it after about a day.
 * </ul>
 * @since 2.0.0
 */
public interface Assets
{
    /**
     * Uploads an add-on artifact (.jar or .obr) to the Marketplace server, which can then
     * be used for an add-on version that you are planning to create.
     * <p>
     * If successful, you will receive an {@link ArtifactId}, which you can pass to
     * {@link ModelBuilders.AddonVersionBuilder#artifact}.
     * <p>
     * Note that if the artifact is available online (for instance, an Atlassian Connect
     * descriptor), you can instead use {@link ArtifactId#fromUri(java.net.URI)} to
     * tell Marketplace where to get the artifact.
     * 
     * @param artifactFile  a local file containing the artifact
     * @return  an {@link ArtifactId} that uniquely identifies the new asset resource
     * @throws MpacException  if the server was unavailable or returned an error
     */
    ArtifactId uploadAddonArtifact(File artifactFile) throws MpacException;
    
    /**
     * Uploads an image file to the Marketplace server, which can then be used for an image
     * property of a model object that you are planning to create or update.
     * <p>
     * If successful, you will receive an {@link ImageId}, which you can pass to one of the
     * {@link ModelBuilders} methods such as {@link ModelBuilders.AddonBuilder#logo(com.atlassian.fugue.Option)}
     * or {@link ModelBuilders.ScreenshotBuilder#image(ImageId)}.
     * <p>
     * Note that if the image is available online, you do not have to upload it, but can
     * instead use {@link ImageId#fromUri(java.net.URI)} to tell Marketplace where
     * to get the image.
     * 
     * @param imageFile  a local file containing the image data
     * @param imageType  specifies what kind of property the image will be used for; this
     *   determines how it will be scaled
     * @return  an {@link ImageId} that uniquely identifies the new asset resource
     * @throws MpacException  if the server was unavailable or returned an error
     */
    ImageId uploadImage(File imageFile, ImagePurpose imageType) throws MpacException;
}
