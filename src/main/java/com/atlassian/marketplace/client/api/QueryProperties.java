package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Iterables.concat;

/**
 * Interfaces for common query criteria, allowing classes like {@link AddonQuery} to be treated abstractly.
 * @since 2.0.0
 */
public abstract class QueryProperties
{
    private QueryProperties()
    {
    }
    
    /**
     * Interface for query criteria that include an "accessToken" parameter.
     * @see QueryBuilderProperties.AccessToken
     */
    public interface AccessToken
    {
        /**
         * The access token, if any, that the client is providing for the query (in order to allow
         * access to a private listing).
         * @see QueryBuilderProperties.AccessToken#accessToken(Option)
         */
        Option<String> getAccessToken();
    }
    
    /**
     * Interface for query criteria that include "application" and "application build number" parameters.
     * @see QueryBuilderProperties.ApplicationCriteria
     */
    public interface ApplicationCriteria
    {
        /**
         * The key of the application, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.ApplicationCriteria#application(Option)
         */
        Option<ApplicationKey> getApplication();

        /**
         * The application build number, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.ApplicationCriteria#appBuildNumber(Option)
         */
        Option<Integer> getAppBuildNumber();
    }
    
    /**
     * Interface for query criteria that include "offset" and "limit" parameters.
     * @see QueryBuilderProperties.Bounds
     */
    public interface Bounds
    {
        /**
         * The offset/limit parameters attached to this query.
         * @see QueryBuilderProperties.Bounds#bounds(QueryBounds)
         */
        QueryBounds getBounds();
    }

    /**
     * Interface for query criteria that include a "cost" parameter.
     * @see QueryBuilderProperties.Cost
     */
    public interface Cost
    {
        /**
         * The {@link Cost} filter, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.Cost#cost(Option)
         */
        Option<com.atlassian.marketplace.client.api.Cost> getCost();
    }

    /**
     * Interface for query criteria that include a "hosting" parameter.
     * @see QueryBuilderProperties.Hosting
     */
    public interface Hosting
    {
        /**
         * The hosting type that the client is using to restrict the query, if any.
         * @see QueryBuilderProperties.Hosting#hosting(Option)
         */
        Option<HostingType> getHosting();
    }

    /**
     * Interface for query criteria that include a "includePrivate" parameter.
     * @see QueryBuilderProperties.IncludePrivate
     */
    public interface IncludePrivate
    {
        /**
         * The "include private" flag that the client is using to restrict the query, if any.
         * @see QueryBuilderProperties.IncludePrivate#includePrivate(boolean)
         */
        boolean isIncludePrivate();
    }

    /**
     * Interface for query criteria that include a "withVersion" parameter.
     * @see QueryBuilderProperties.WithVersion
     */
    public interface WithVersion
    {
        /**
         * True if the client is requesting version-level detail in the result set.
         * @see QueryBuilderProperties.WithVersion#withVersion(boolean)
         */
        boolean isWithVersion();
    }
    
    // helper methods used internally
    
    static String describeParams(String className, Iterable<String>... paramLists)
    {
        return className + "(" + Joiner.on(", ").join(concat(paramLists)) + ")";
    }
    
    static Iterable<String> describeOptBoolean(String name, boolean value)
    {
        return value ? some(name + "(true)") : Option.<String>none();
    }
    
    static <T extends EnumWithKey> Iterable<String> describeOptEnum(String name, Option<T> value)
    {
        for (T v: value)
        {
            return some(name + "(" + v.getKey() + ")");
        }
        return none();
    }
    
    static Iterable<String> describeValues(String name, Iterable<?> values)
    {
        if (!Iterables.isEmpty(values))
        {
            return some(name + "(" + Joiner.on(",").join(values) + ")");
        }
        return none();
    }
}
