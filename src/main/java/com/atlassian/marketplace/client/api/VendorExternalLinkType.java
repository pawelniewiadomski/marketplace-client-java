package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of vendor-specified external links for vendors.
 * @since 2.0.0
 */
public enum VendorExternalLinkType
{
    /**
     * A link to the vendor's home page.
     */
    HOME_PAGE("homePage"),
    
    /**
     * A link to a Service Level Agreement.
     */
    SLA("sla");
    
    private final String key;
    
    private VendorExternalLinkType(String key)
    {
        this.key = key;
    }
    
    /**
     * The property name that corresponds to this link type within the {@code vendorLinks} or
     * property of the vendor's JSON representation.
     */
    public String getKey()
    {
        return key;
    }
}
