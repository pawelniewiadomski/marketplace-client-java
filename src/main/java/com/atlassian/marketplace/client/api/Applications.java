package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationVersion;

/**
 * Starting point for all resources that return application information.  Use
 * {@link MarketplaceClient#applications()} to access this API.
 * <p>
 * An "application", in this context, is a top-level Atlassian software platform such as JIRA or Confluence;
 * software packages like JIRA Service Desk, which are called "applications" in the UI, are instead accessed
 * through the {@link Products} API.
 * 
 * @since 2.0.0
 */
public interface Applications
{
    /**
     * Queries a single application.
     * @param applicationKey  the application's unique key
     * @return the application information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<Application> getByKey(ApplicationKey applicationKey) throws MpacException;
    
    /**
     * Queries a specific version of an application.
     * @param applicationKey  the application's unique key
     * @param versionQuery  search criteria for identifying a single version
     * @return the application version information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<ApplicationVersion> getVersion(ApplicationKey applicationKey, ApplicationVersionSpecifier versionQuery) throws MpacException;
    
    /**
     * Gets a list of versions for an application.
     * @param applicationKey  the application's unique key
     * @param versionsQuery  an {@link ApplicationVersionsQuery} which may contain criteria to limit the
     *   search; use {@link ApplicationVersionsQuery#any()} if there are no additional criteria
     * @return the application versions
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<ApplicationVersion> getVersions(ApplicationKey applicationKey, ApplicationVersionsQuery versionsQuery) throws MpacException;

    /**
     * Creates a new version for an existing application.  Only Marketplace administrators can perform
     * this operation.
     * <p>
     * Use {@link com.atlassian.marketplace.client.model.ModelBuilders#applicationVersion} to build a new
     * {@link ApplicationVersion} object.
     *
     * @param applicationKey unique key of the existing application
     * @param version the add-on version to create
     * @return an {@link ApplicationVersion} representing the version in its initial state after being created
     * @throws MpacException  if the server was unavailable or returned an error
     */
    ApplicationVersion createVersion(ApplicationKey applicationKey, ApplicationVersion version) throws MpacException;

    /**
     * Attempts to modify an existing application version.  Only Marketplace administrators can perform this
     * operation.  You must have already queried the current state of the version with
     * {@link Applications#getVersion(ApplicationKey, ApplicationVersionSpecifier)};
     * then, build another instance that includes any changes you want to make, using
     * {@link com.atlassian.marketplace.client.model.ModelBuilders#applicationVersion(ApplicationVersion)}.
     * 
     * @param original the existing application version
     * @param updated a copy of the version that includes some changes
     * @return the updated version (re-queried from the server) if successful
     * @throws MpacException  if the server was unavailable or returned an error
     */
    ApplicationVersion updateVersion(ApplicationVersion original, ApplicationVersion updated) throws MpacException;
}
