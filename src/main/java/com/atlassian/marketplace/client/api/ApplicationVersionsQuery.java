package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates parameters that can be passed to {@link Applications#getVersion}.
 * @since 2.0.0
 */
public final class ApplicationVersionsQuery implements QueryProperties.Bounds
{
    private static final ApplicationVersionsQuery DEFAULT_QUERY = builder().build();
    
    private final Option<Integer> afterBuildNumber;
    private final QueryBounds bounds;

    /**
     * Returns a new {@link Builder} for constructing an ApplicationVersionsQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns an ApplicationsQuery with no criteria, which will query the most recent application versions.
     */
    public static ApplicationVersionsQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing an ApplicationVersionsQuery based on an existing ApplicationVersionsQuery.
     */
    public static Builder builder(ApplicationVersionsQuery query)
    {
        Builder builder = builder()
            .afterBuildNumber(query.getAfterBuildNumber())
            .bounds(query.getBounds());

        return builder;
    }
    
    private ApplicationVersionsQuery(Builder builder)
    {
        afterBuildNumber = builder.afterBuildNumber;
        bounds = builder.bounds;
    }

    /**
     * The version build number, if any, that the client has specified in order to search for
     * later versions.
     * @see Builder#afterBuildNumber(Option)
     */
    public Option<Integer> getAfterBuildNumber()
    {
        return afterBuildNumber;
    }

    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("ApplicationVersionsQuery",
            describeValues("afterBuildNumber", afterBuildNumber),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ApplicationVersionsQuery) ? toString().equals(other.toString()) : false;
    }
    
    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link ApplicationVersionsQuery}.  Use {@link ApplicationVersionsQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.Bounds<Builder>
    {
        private Option<Integer> afterBuildNumber = none();
        private QueryBounds bounds = QueryBounds.defaultBounds();

        /**
         * Returns an immutable {@link ApplicationVersionsQuery} based on the current builder properties.
         */
        public ApplicationVersionsQuery build()
        {
            return new ApplicationVersionsQuery(this);
        }

        /**
         * Specifies an existing version if you want to query versions that are later than that
         * version.
         * @param afterBuildNumber  the build number of an existing application version
         *   to restrict the query to versions later than this version, or {@link Option#none()} to
         *   remove any such restriction 
         * @return  the same query builder
         * @see ApplicationVersionsQuery#getAfterBuildNumber()
         */
        public Builder afterBuildNumber(Option<Integer> afterBuildNumber)
        {
            this.afterBuildNumber = checkNotNull(afterBuildNumber);
            return this;
        }

        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
