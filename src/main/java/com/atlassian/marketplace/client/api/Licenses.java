package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.License;
import com.atlassian.marketplace.client.model.Vendor;
import com.atlassian.marketplace.client.model.VendorSummary;

/**
 * Starting point for all resources that return license information.  Use
 * {@link MarketplaceClient#licenses()} to access this API.
 * @since 2.1.0
 */
public interface Licenses
{
    /**
     * Gets a list of licenses.
     * @param query a {@link LicenseQuery} which can constrain the result set
     * @return the vendors
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<License> find(LicenseQuery query) throws MpacException;
}
