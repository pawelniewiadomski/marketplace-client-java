package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Vendor;
import com.atlassian.marketplace.client.model.VendorSummary;

/**
 * Starting point for all resources that return vendor information.  Use
 * {@link MarketplaceClient#vendors()} to access this API.
 * @since 2.0.0
 */
public interface Vendors
{
    /**
     * Queries a single vendor.
     * @param id  the unique identifier of the vendor
     * @return the vendor information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<Vendor> getById(VendorId id) throws MpacException;

    /**
     * Gets a list of vendors.
     * @param query a {@link VendorQuery} which can constrain the result set
     * @return the vendors
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<VendorSummary> find(VendorQuery query) throws MpacException;

    /**
     * Creates a new vendor.  You must be authenticated to perform this action.
     * <p>
     * Use {@link com.atlassian.marketplace.client.model.ModelBuilders#vendor} to build a new
     * {@link Vendor} object.
     * 
     * @param vendor  the vendor to create
     * @return a {@link Vendor} representing the vendor in its current state after being created
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Vendor createVendor(Vendor vendor) throws MpacException;

    /**
     * Attempts to modify an existing vendor.  You must have already queried the current state of
     * the vendor with {@link #getById(VendorId)}; then, build another instance that includes any
     * changes you want to make, using
     * {@link com.atlassian.marketplace.client.model.ModelBuilders#vendor(Vendor)}.
     * 
     * @param original the existing vendor
     * @param updated a copy of the vendor that includes some changes
     * @return the updated vendor (re-queried from the server) if successful
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Vendor updateVendor(Vendor original, Vendor updated) throws MpacException;
}
