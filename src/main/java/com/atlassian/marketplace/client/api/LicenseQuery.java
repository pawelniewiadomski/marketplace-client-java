package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import org.joda.time.LocalDate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.Collections.emptyList;

/**
 * Encapsulates search parameters that can be passed to {@link Vendors} to determine what
 * subset of vendors you are interested in.  Besides the usual {@link QueryBounds} parameters,
 * you can specify whether to query all vendors (the default) or only the vendors that are
 * associated with the current authenticated user.
 * @since 2.0.0
 */
public final class LicenseQuery implements QueryProperties.Bounds
{
    private static final LicenseQuery DEFAULT_QUERY = builder().build();

    private final Iterable<String> addon;
    private final Option<LocalDate> startDate;
    private final Option<LocalDate> endDate;
    private final Option<String> text;
    private final Iterable<Integer> tier;
    private final Option<String> dateType;
    private final Iterable<String> licenseType;
    private final Iterable<PartnerType> partnerTypes;
    private final Iterable<HostingType> hosting;
    private final Iterable<LicenseStatusType> status;
    private final Option<LocalDate> lastUpdated;
    private final Option<String> sortBy;
    private final Option<String> order;
    private final QueryBounds bounds;

    /**
     * Returns a new {@link Builder} for constructing a VendorQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns a VendorQuery with default criteria.
     */
    public static LicenseQuery any()
    {
        return DEFAULT_QUERY;
    }

    /**
     * Returns a new {@link Builder} for constructing a VendorQuery based on an existing VendorQuery.
     */
    public static Builder builder(LicenseQuery query)
    {
        return builder()
            .addon(query.getAddon())
            .startDate(query.getStartDate())
            .endDate(query.getEndDate())
            .text(query.getText())
            .tier(query.getTier())
            .dateType(query.getDateType())
            .licenseType(query.getLicenseType())
            .partnerTypes(query.getPartnerTypes())
            .hosting(query.getHosting())
            .status(query.getStatus())
            .lastUpdated(query.getLastUpdated())
            .sortBy(query.getSortBy())
            .order(query.getOrder())
            .bounds(query.getBounds());
    }

    private LicenseQuery(Builder builder)
    {
        addon = copyOf(builder.addon);
        startDate = builder.startDate;
        endDate = builder.endDate;
        text = builder.text;
        tier = copyOf(builder.tier);
        dateType = builder.dateType;
        licenseType = copyOf(builder.licenseType);
        partnerTypes = copyOf(builder.partnerTypes);
        hosting = builder.hosting;
        status = copyOf(builder.status);
        lastUpdated = builder.lastUpdated;
        sortBy = builder.sortBy;
        order = builder.order;
        bounds = builder.bounds;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    public Iterable<String> getAddon() {
        return addon;
    }

    public Option<LocalDate> getStartDate() {
        return startDate;
    }

    public Option<LocalDate> getEndDate() {
        return endDate;
    }

    public Option<String> getText() {
        return text;
    }

    public Iterable<Integer> getTier() {
        return tier;
    }

    public Option<String> getDateType() {
        return dateType;
    }

    public Iterable<String> getLicenseType() {
        return licenseType;
    }

    public Iterable<PartnerType> getPartnerTypes() {
        return partnerTypes;
    }

    public Iterable<HostingType> getHosting() {
        return hosting;
    }

    public Iterable<LicenseStatusType> getStatus() {
        return status;
    }

    public Option<LocalDate> getLastUpdated() {
        return lastUpdated;
    }

    public Option<String> getSortBy() {
        return sortBy;
    }

    public Option<String> getOrder() {
        return order;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("LicenseQuery",
            describeValues("addon", addon),
            describeValues("startDate", startDate),
            describeValues("endDate", endDate),
            describeValues("text", text),
            describeValues("tier", tier),
            describeValues("dateType", dateType),
            describeValues("licenseType", licenseType),
            describeValues("partnerTypes", partnerTypes),
            describeValues("hosting", hosting),
            describeValues("status", status),
            describeValues("lastUpdated", lastUpdated),
            describeValues("sortBy", sortBy),
            describeValues("order", order),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof LicenseQuery) ? toString().equals(other.toString()) : false;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }

    /**
     * Builder class for {@link LicenseQuery}.  Use {@link LicenseQuery#builder()} to create an instance.
     */
    public static class Builder implements QueryBuilderProperties.Bounds<Builder>
    {
        private QueryBounds bounds = QueryBounds.defaultBounds();
        private Iterable<String> addon = emptyList();
        private Option<LocalDate> startDate = none();
        private Option<LocalDate> endDate = none();
        private Option<String> text = none();
        private Iterable<Integer> tier = emptyList();
        private Option<String> dateType = none();
        private Iterable<String> licenseType = emptyList();
        private Iterable<PartnerType> partnerTypes = emptyList();
        private Iterable<HostingType> hosting = emptyList();
        private Iterable<LicenseStatusType> status = emptyList();
        private Option<LocalDate> lastUpdated = none();
        private Option<String> sortBy = none();
        private Option<String> order = none();

        /**
         * Returns an immutable {@link LicenseQuery} based on the current builder properties.
         */
        public LicenseQuery build()
        {
            return new LicenseQuery(this);
        }

        public LicenseQuery.Builder addon(Iterable<String> addon)
        {
            this.addon = copyOf(addon);
            return this;
        }

        public LicenseQuery.Builder startDate(Option<LocalDate> startDate) {
            this.startDate = checkNotNull(startDate);
            return this;
        }

        public LicenseQuery.Builder endDate(Option<LocalDate> endDate) {
            this.endDate = checkNotNull(endDate);
            return this;
        }
        public LicenseQuery.Builder text(Option<String> text) {
            this.text = checkNotNull(text);
            return this;
        }

        public LicenseQuery.Builder tier(Iterable<Integer> tier) {
            this.tier = copyOf(tier);
            return this;
        }

        public LicenseQuery.Builder dateType(Option<String> dateType) {
            this.dateType = checkNotNull(dateType);
            return this;
        }
        public LicenseQuery.Builder licenseType(Iterable<String> licenseType) {
            this.licenseType = copyOf(licenseType);
            return this;
        }

        public LicenseQuery.Builder partnerTypes(Iterable<PartnerType> partnerTypes) {
            this.partnerTypes = copyOf(partnerTypes);
            return this;
        }

        public LicenseQuery.Builder hosting(Iterable<HostingType> hosting) {
            this.hosting = copyOf(hosting);
            return this;
        }

        public LicenseQuery.Builder status(Iterable<LicenseStatusType> status) {
            this.status = copyOf(status);
            return this;
        }

        public LicenseQuery.Builder lastUpdated(Option<LocalDate> lastUpdated) {
            this.lastUpdated = checkNotNull(lastUpdated);
            return this;
        }
        public LicenseQuery.Builder sortBy(Option<String> sortBy) {
            this.sortBy = checkNotNull(sortBy);
            return this;
        }
        public LicenseQuery.Builder order(Option<String> ord) {
            this.order = checkNotNull(order);
            return this;
        }

        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
