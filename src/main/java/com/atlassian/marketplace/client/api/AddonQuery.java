package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptBoolean;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptEnum;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates search parameters that can be passed to {@link Addons} to determine what
 * subset of add-on listings you are interested in and/or what information should be included
 * in the results.
 * @since 2.0.0
 */
public final class AddonQuery implements QueryProperties.AccessToken,
        QueryProperties.ApplicationCriteria,
        QueryProperties.Bounds,
        QueryProperties.Cost,
        QueryProperties.Hosting,
        QueryProperties.IncludePrivate,
        QueryProperties.WithVersion
{
    private static final AddonQuery DEFAULT_QUERY = builder().build();
    
    private final Option<String> accessToken;
    private final QueryBuilderProperties.ApplicationCriteriaHelper app;
    private final QueryBounds bounds;
    private final Iterable<String> categoryNames;
    private final Option<Cost> cost;
    private final boolean forThisUserOnly;
    private final Option<HostingType> hosting;
    private final Option<IncludeHiddenType> includeHidden;
    private final boolean includePrivate;
    private final Option<String> label;
    private final Option<TreatPartlyFreeAs> treatPartlyFreeAs;
    private final Option<String> searchText;
    private final Option<View> view;
    private final boolean withVersion;
    
    /**
     * Returns a new {@link Builder} for constructing an AddonQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns an {@link AddonQuery} with no criteria, which will match any available add-on.
     */
    public static AddonQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing an {@link AddonQuery} based on an existing {@link AddonQuery}.
     */
    public static Builder builder(AddonQuery query)
    {
        Builder builder = builder()
            .application(query.getApplication())
            .appBuildNumber(query.getAppBuildNumber())
            .categoryNames(query.getCategoryNames())
            .cost(query.getCost())
            .forThisUserOnly(query.isForThisUserOnly())
            .hosting(query.getHosting())
            .includeHidden(query.getIncludeHidden())
            .includePrivate(query.isIncludePrivate())
            .label(query.getLabel())
            .treatPartlyFreeAs(query.getTreatPartlyFreeAs())
            .view(query.getView())
            .withVersion(query.isWithVersion())
            .bounds(query.getBounds())
            .searchText(query.getSearchText());

        return builder;
    }

    private AddonQuery(Builder builder)
    {
        accessToken = builder.accessToken;
        app = builder.app;
        bounds = builder.bounds;
        categoryNames = builder.categoryNames;
        cost = builder.cost;
        forThisUserOnly = builder.forThisUserOnly;
        hosting = builder.hosting;
        includeHidden = builder.includeHidden;
        includePrivate = builder.includePrivate;
        label = builder.label;
        searchText = builder.searchText;
        treatPartlyFreeAs = builder.treatPartlyFreeAs;
        view = builder.view;
        withVersion = builder.withVersion;
    }
    
    @Override
    public Option<String> getAccessToken()
    {
        return accessToken;
    }
    
    @Override
    public Option<ApplicationKey> getApplication()
    {
        return app.application;
    }
    
    @Override
    public Option<Integer> getAppBuildNumber()
    {
        return app.appBuildNumber;
    }
    
    /**
     * The list of add-on category names, if any, that the client has specified to restrict
     * the query results.
     * @see Builder#categoryNames(Iterable)
     */
    public Iterable<String> getCategoryNames()
    {
        return categoryNames;
    }
    
    @Override
    public Option<Cost> getCost()
    {
        return cost;
    }

    /**
     * True if the client is querying only add-ons that are associated with the current
     * authenticated user's vendor(s).
     * @see Builder#forThisUserOnly(boolean)
     */
    public boolean isForThisUserOnly()
    {
        return forThisUserOnly;
    }

    /**
     * The subset of normally hidden add-ons, if any, that the client has specified to include
     * in the query results.
     * @see Builder#includeHidden(Option)
     */
    public Option<IncludeHiddenType> getIncludeHidden()
    {
        return includeHidden;
    }
    
    /**
     * True if the client is querying private add-ons as well as public add-ons.
     * @see Builder#includePrivate(boolean)
     */
    public boolean isIncludePrivate()
    {
        return includePrivate;
    }
    
    @Override
    public Option<HostingType> getHosting()
    {
        return hosting;
    }
    
    /**
     * The marketing label string, if any, that the client has specified to restrict the query results.
     * @see Builder#label(Option)
     */
    public Option<String> getLabel()
    {
        return label;
    }
    
    /**
     * The {@link TreatPartlyFreeAs} value, if any, that the client has specified to determine how
     * free-tier listings will be treated in the query.
     * @see Builder#treatPartlyFreeAs(Option)
     */
    public Option<TreatPartlyFreeAs> getTreatPartlyFreeAs()
    {
        return treatPartlyFreeAs;
    }

    /**
     * The search text, if any, that the client has specified for the query.
     * @see Builder#searchText(Option)
     */
    public Option<String> getSearchText()
    {
        return searchText;
    }
    
    /**
     * The {@link View} value, if any, that the client has specified as a filter and/or sort order
     * for the query.
     * @see Builder#view(Option)
     */
    public Option<View> getView()
    {
        return view;
    }
    
    @Override
    public boolean isWithVersion()
    {
        return withVersion;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("AddonQuery", 
            describeValues("accessToken", accessToken),
            app.describe(),
            describeValues("categoryNames", categoryNames),
            describeOptEnum("cost", cost),
            describeOptBoolean("forThisUserOnly", forThisUserOnly),
            describeOptEnum("hosting", hosting),
            describeOptEnum("includeHidden", includeHidden),
            describeOptBoolean("includePrivate", includePrivate),
            describeValues("label", label),
            describeValues("searchText", searchText),
            describeOptEnum("treatPartlyFreeAs", treatPartlyFreeAs),
            describeOptEnum("view", view),
            describeOptBoolean("withVersion", withVersion),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof AddonQuery) ? toString().equals(other.toString()) : false;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }

    /**
     * Builder class for {@link AddonQuery}.  Use {@link AddonQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.AccessToken<Builder>,
        QueryBuilderProperties.ApplicationCriteria<Builder>,
        QueryBuilderProperties.Bounds<Builder>,
        QueryBuilderProperties.Cost<Builder>,
        QueryBuilderProperties.Hosting<Builder>,
        QueryBuilderProperties.WithVersion<Builder>
    {
        private Option<String> accessToken = none();
        private QueryBuilderProperties.ApplicationCriteriaHelper app = new QueryBuilderProperties.ApplicationCriteriaHelper();
        private QueryBounds bounds = QueryBounds.defaultBounds();
        private Iterable<String> categoryNames = ImmutableList.of();
        private Option<Cost> cost = none();
        private boolean forThisUserOnly = false;
        private Option<HostingType> hosting = none();
        private Option<IncludeHiddenType> includeHidden = none();
        private boolean includePrivate = false;
        private Option<String> label = none();
        private Option<TreatPartlyFreeAs> treatPartlyFreeAs = none();
        private Option<String> searchText = none();
        private Option<View> view = none();
        private boolean withVersion = false;
        
        /**
         * Returns an immutable {@link AddonQuery} based on the current builder properties.
         */
        public AddonQuery build()
        {
            return new AddonQuery(this);
        }
        
        @Override
        public Builder accessToken(Option<String> accessToken)
        {
            this.accessToken = checkNotNull(accessToken);
            return this;
        }
        
        @Override
        public Builder application(Option<ApplicationKey> application)
        {
            app = app.application(application);
            return this;
        }
        
        @Override
        public Builder appBuildNumber(Option<Integer> appBuildNumber)
        {
            app = app.appBuildNumber(appBuildNumber);
            return this;
        }
        
        /**
         * Restricts the query to add-ons that belong to one of the specified categories.  An empty list
         * means there is no such restriction.  To obtain a list of allowable category names, use
         * {@link AddonCategories}.
         * @param categoryNames  category names to search for, or an empty {@code Iterable} if you do
         *   not want to filter by add-on category
         * @return  the same query builder
         * @see AddonQuery#getCategoryNames()
         */
        public Builder categoryNames(Iterable<String> categoryNames)
        {
            this.categoryNames = ImmutableList.copyOf(categoryNames);
            return this;
        }

        @Override
        public Builder cost(Option<Cost> cost)
        {
            this.cost = checkNotNull(cost);
            return this;
        }
        
        /**
         * Specifies whether to query only the addons associated with the current authenticated user's vendor(s).
         * @param forThisUserOnly  true to query only addons associated with the current
         *   authenticated user; false (the default) to query add-ons from any vendor.
         * @return  the same query builder
         * @see AddonQuery#isForThisUserOnly()
         */
        public Builder forThisUserOnly(boolean forThisUserOnly)
        {
            this.forThisUserOnly = forThisUserOnly;
            return this;
        }
        
        @Override
        public Builder hosting(Option<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }

        /**
         * Specifies whether to include special "hidden" add-ons in the results.  These are not private
         * listings; they are add-ons that Atlassian has configured to be invisible on the Marketplace site, but
         * still discoverable from within applications under some circumstances (such as language packs).
         * @param includeHidden  the type of hidden add-ons to include, or {@link Option#none()} (the default)
         *   to exclude them all
         * @return  the same query builder
         * @see AddonQuery#getIncludeHidden()
         */
        public Builder includeHidden(Option<IncludeHiddenType> includeHidden)
        {
            this.includeHidden = checkNotNull(includeHidden);
            return this;
        }
        
        /**
         * Specifies whether to include private add-ons in the results.  This will only have an effect if you
         * are authenticated, and you will only be able to see private add-ons that you would be able to access
         * in the Marketplace site (that is, unless you are a Marketplace administrator, only private add-ons
         * from your vendor(s) will be accessible).
         * @param includePrivate  true to include private add-ons; false (the default) to include only public
         *   add-ons
         * @return  the same query builder
         * @see AddonQuery#isIncludePrivate()
         */
        public Builder includePrivate(boolean includePrivate)
        {
            this.includePrivate = includePrivate;
            return this;
        }
        
        /**
         * Restricts the query to add-ons that have been tagged with the specified label string.
         * This is an internal identifier, not visible on the Marketplace site, that Atlassian
         * administrators may use to create subsets of add-ons for any purpose. 
         * @param label  label string to search for, or {@link Option#none()} for no label search
         * @return  the same query builder
         * @see AddonQuery#getLabel()
         */
        public Builder label(Option<String> label)
        {
            this.label = checkNotNull(label);
            return this;
        }
        
        /**
         * Specify how partly free, paid via Atlassian add-ons (those with a free tier) should be treated.
         * By default none is included.
         *
         * @param treatPartlyFreeAs how to treat partly free, PvA add-ons in queries
         * @return  the same query builder
         */
        public Builder treatPartlyFreeAs(Option<TreatPartlyFreeAs> treatPartlyFreeAs)
        {
            this.treatPartlyFreeAs = treatPartlyFreeAs;
            return this;
        }

        /**
         * Restricts the query to add-ons that have the specified text somewhere in their name or
         * description.
         * @param searchText  text to search for, or {@link Option#none()} for no text search
         * @return  the same query builder
         * @see AddonQuery#getSearchText()
         */
        public Builder searchText(Option<String> searchText)
        {
            this.searchText = checkNotNull(searchText);
            return this;
        }
        
        /**
         * Specifies an {@link View} which determines the overall sort order and/or subset of
         * add-ons to be queried.
         * @param view  an {@link View} value, or {@link Option#none()} for the default view
         * @return  the same query builder
         * @see AddonQuery#getView()
         */
        public Builder view(Option<View> view)
        {
            this.view = checkNotNull(view);
            return this;
        }
        
        @Override
        public Builder withVersion(boolean withVersion)
        {
            this.withVersion = withVersion;
            return this;
        }
        
        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }

    /**
     * Constants representing subsets of add-ons that are normally hidden but may still be queried.
     * @see AddonQuery.Builder#includeHidden(Option)
     */
    public enum IncludeHiddenType implements EnumWithKey
    {
        /**
         * Add-ons that do not appear on the Marketplace site, but that are normally discoverable from within
         * an application (such as language packs), will be included in the query results.
         */
        VISIBLE_IN_APP ("visibleInApp"),
        
        /**
         * Add-ons that do not appear on the Marketplace site will be included in the query results,
         * even if they are not normally discoverable from within an application (for instance, add-ons
         * that are bundled within an application but may have updates made available via Marketplace).
         */
        ALL ("all");
        
        private final String key;
        
        private IncludeHiddenType(String key)
        {
            this.key = key;
        }
        
        @Override
        public String getKey()
        {
            return key;
        }
    }
    
    /**
     * Determines how paid-via-Atlassian add-ons with free tiers, such as those where the vendor has opted into the
     * Cloud free-for-five tier, are treated in queries.
     * @see AddonQuery.Builder#treatPartlyFreeAs(Option)
     */
    public enum TreatPartlyFreeAs implements EnumWithKey
    {
        /**
         * Add-ons with free tiers will be effectively treated as free in queries. An example of when this should be
         * used is when the application has a Cloud free for X number of users and there are less than or equal to
         * X active users.
         */
        FREE("free"),

        /**
         * Add-ons with free tiers will be effectively treated as paid in queries.
         */
        PAID("paid");

        private final String key;

        private TreatPartlyFreeAs(String key)
        {
            this.key = key;
        }

        @Override
        public String getKey()
        {
            return key;
        }
    }

    /**
     * Constants representing preset add-on list views, which may affect both the set of
     * add-ons being queried and the sort order of the results.
     * @see AddonQuery.Builder#view(Option)
     */
    public enum View implements EnumWithKey
    {
        /**
         * Restricts the query to add-ons made by Atlassian, with the most recently updated add-ons first.
         */
        BY_ATLASSIAN ("atlassian"),
        /**
         * Restricts the query to a set of add-ons picked by Atlassian staff, with the most recently
         * updated add-ons first.
         */
        FEATURED ("featured"),
        /**
         * Query all add-ons, sorted by rating.
         */
        HIGHEST_RATED ("highest-rated"),
        /**
         * Queries all add-ons, in descending order of total number of downloads.
         */
        POPULAR ("popular"),
        /**
         * Queries all add-ons, in descending order of modification date.
         */
        RECENTLY_UPDATED ("recent"),
        /**
         * Queries all Paid-via-Atlassian add-ons, in descending order of gross sales.
         */
        TOP_GROSSING ("top-grossing"),
        /**
         * Queries all add-ons, in descending order of number of recent downloads.
         */
        TRENDING ("trending"),
        /**
         * Restricts the query to add-ons that are made by Verified vendors.
         */
        VERIFIED("verified");
        
        private final String key;
        
        private View(String key)
        {
            this.key = key;
        }
        
        @Override
        public String getKey()
        {
            return key;
        }
    }
}
