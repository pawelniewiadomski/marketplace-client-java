package com.atlassian.marketplace.client.api;

import java.lang.reflect.Method;

import com.atlassian.fugue.Option;
import com.google.common.base.Function;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Common interface for {@code enum} types that have a unique string key for each possible value.
 * @since 2.0.0
 */
public interface EnumWithKey
{
    /**
     * The string key that represents this value in the Marketplace API.
     */
    String getKey();
    
    /**
     * Helper class that can get allowable values or match a key string for any {@link EnumWithKey} type.
     */
    class Parser<A extends EnumWithKey>
    {
        private final A[] values;
        
        private Parser(A[] values)
        {
            this.values = checkNotNull(values);
        }
        
        public A[] getValues()
        {
            return values;
        }
        
        public Option<A> valueForKey(String key)
        {
            for (A v: values)
            {
                if (v.getKey().equalsIgnoreCase(key))
                {
                    return some(v);
                }
            }
            return none();
        }
        
        public static <A extends EnumWithKey> Parser<A> forType(Class<A> enumClass)
        {
            try
            {
                // This is ugly, but unfortunately Java's built-in {@code Enum} doesn't provide
                // any *generic* way to get the list of allowed values.
                Method method = enumClass.getDeclaredMethod("values");
                @SuppressWarnings("unchecked")
                A[] values = (A[]) method.invoke(null);
                return new Parser<A>(values);
            }
            catch (Exception e)
            {
                throw new IllegalStateException(e);
            }
        }
    }

    Function<EnumWithKey, String> toString = new Function<EnumWithKey, String>()
    {
        @Nullable
        @Override
        public String apply (@Nullable EnumWithKey enumWithKey) {
            return enumWithKey.getKey();
        }
    };
}
