package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of pricing for Atlassian applications.
 * @since 2.0.0
 */
public enum LicenseStatusType implements EnumWithKey
{
    ACTIVE("active"),

    INACTIVE("inactive"),

    CANCELLED("cancelled");

    private final String key;

    private LicenseStatusType(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
