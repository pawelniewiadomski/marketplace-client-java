package com.atlassian.marketplace.client.api;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.marketplace.client.model.Link;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableMap;

/**
 * A simple string wrapper that represents a URI template (RFC 6570).  The current implementation
 * only provides for very simple expansion of template parameters.
 * @see Link
 */
public final class UriTemplate
{
    private static final Pattern SIMPLE_PLACEHOLDER_REGEX = Pattern.compile("\\{([^}]*)\\}");
    
    private final String value;
    
    public static UriTemplate create(String value)
    {
        return new UriTemplate(value);
    }
    
    private UriTemplate(String value)
    {
        this.value = value;
    }

    /**
     * Returns the template string.
     */
    public String getValue()
    {
        return value;
    }
    
    /**
     * Constructs a URI by substituting values for the named variable placeholders ("{name}" or {?queryName}")
     * in the template.  Any placeholders that do not have a corresponding value in the "params" map will be
     * left empty.
     * @param params  map of placeholder names to values
     * @return  a URI
     */
    public URI resolve(Map<String, String> params)
    {
        StringBuilder b = new StringBuilder();
        int pos = 0;
        Matcher m = SIMPLE_PLACEHOLDER_REGEX.matcher(value);
        ImmutableMap.Builder<String, String> query = ImmutableMap.builder();
        while (m.find()) {
            b.append(value.substring(pos, m.start()));
            String name = m.group(1);
            if (name.startsWith("?"))
            {
                for (String subName: name.substring(1).split(","))
                {
                    String realName = subName.endsWith("*") ? subName.substring(0, subName.length() - 1) : subName;
                    if (params.containsKey(realName))
                    {
                        query.put(realName, params.get(realName));
                    }
                }
            }
            else
            {
                String value = params.containsKey(name) ? params.get(name) : "";
                try
                {
                    b.append(URLEncoder.encode(value, "UTF-8"));
                }
                catch (UnsupportedEncodingException e)
                {
                    throw new RuntimeException(e); // UTF-8 should never be unavailable
                }
            }
            pos = m.end();
        }
        b.append(value.substring(pos));
        UriBuilder ub = UriBuilder.fromUri(b.toString());
        for (Map.Entry<String, String> e: query.build().entrySet())
        {
            ub.queryParam(e.getKey(), e.getValue());
        }
        return ub.build();
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof UriTemplate) && ((UriTemplate) other).value.equals(this.value);
    }
    
    @Override
    public int hashCode()
    {
        return value.hashCode();
    }
}