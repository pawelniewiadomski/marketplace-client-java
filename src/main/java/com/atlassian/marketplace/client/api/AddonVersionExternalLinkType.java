package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of vendor-specified external links for add-on versions.
 * Deprecated types are available only for some old add-on listings and cannot be set for new ones.
 * @since 2.0.0
 */
public enum AddonVersionExternalLinkType
{
    /**
     * A link to a downloadable file for the add-on, if it is not hosted on the Marketplace server
     * (for add-ons that are not directly installable).
     */
    BINARY("binary", true),
    
    /**
     * A link to online documentation.
     */
    DOCUMENTATION("documentation", true),
    
    /**
     * A link to a web page for donating to the vendor.
     * @deprecated This link cannot be set for new add-on versions, but may be present in old versions.
     */
    @Deprecated
    DONATE("donate", false),
    
    /**
     * A link to an end-user license agreement.
     */
    EULA("eula", true),
    
    /**
     * A link to a page for obtaining evaluation licenses.
     * @deprecated This link cannot be set for new add-on versions, but may be present in old versions.
     */
    @Deprecated
    EVALUATION_LICENSE("evaluationLicense", false),
    
    /**
     * A link to Javadoc API documentation.
     * @deprecated This link cannot be set for new add-on versions, but may be present in old versions.
     */
    @Deprecated
    JAVADOC("javadocs", false),
    
    /**
     * A link to a "learn more" page.
     */
    LEARN_MORE("learnMore", true),
    
    /**
     * A link to a web page for licensing details.
     */
    LICENSE("license", true),
    
    /**
     * A link to a web page for purchasing the add-on, if it is not paid via Atlassian
     */
    PURCHASE("purchase", true),
    
    /**
     * A link to a web page containing release notes.
     */
    RELEASE_NOTES("releaseNotes", true),
    
    /**
     * A link to a source control repository.
     * @deprecated This link cannot be set for new add-on versions, but may be present in old versions.
     */
    @Deprecated
    SOURCE("source", false);
    
    private final String key;
    private final boolean canSetForNewAddonVersions;
    
    private AddonVersionExternalLinkType(String key, boolean canSetForNewAddonVersions)
    {
        this.key = key;
        this.canSetForNewAddonVersions = canSetForNewAddonVersions;
    }
    
    /**
     * Returns true if it is possible to set this type of link for new add-on versions.
     */
    public boolean canSetForNewAddonVersions()
    {
        return canSetForNewAddonVersions;
    }
    
    /**
     * The property name that corresponds to this link type within the {@code vendorLinks} or
     * {@code legacy.vendorLinks} property of the add-on version's JSON representation.
     */
    public String getKey()
    {
        return key;
    }
}
