package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptBoolean;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptEnum;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates search parameters that can be passed to {@link Products#find(ProductQuery)}.
 * @since 2.0.0
 */
public final class ProductQuery implements QueryProperties.ApplicationCriteria,
        QueryProperties.Bounds,
        QueryProperties.Cost,
        QueryProperties.Hosting,
        QueryProperties.WithVersion
{
    private static final ProductQuery DEFAULT_QUERY = builder().build();
    
    private final QueryBuilderProperties.ApplicationCriteriaHelper app;
    private final Option<Cost> cost;
    private final Option<HostingType> hosting;
    private final boolean withVersion;
    private final QueryBounds bounds;
    
    /**
     * Returns a new {@link Builder} for constructing a ProductQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns a ProductQuery with no criteria, which will match any available product.
     */
    public static ProductQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing a ProductQuery based on an existing ProductQuery.
     */
    public static Builder builder(ProductQuery query)
    {
        Builder builder = builder()
            .application(query.getApplication())
            .appBuildNumber(query.getAppBuildNumber())
            .cost(query.getCost())
            .hosting(query.getHosting())
            .withVersion(query.isWithVersion())
            .bounds(query.getBounds());

        return builder;
    }

    private ProductQuery(Builder builder)
    {
        app = builder.app;
        cost = builder.cost;
        hosting = builder.hosting;
        withVersion = builder.withVersion;
        bounds = builder.bounds;
    }
    
    @Override
    public Option<ApplicationKey> getApplication()
    {
        return app.application;
    }
    
    @Override
    public Option<Integer> getAppBuildNumber()
    {
        return app.appBuildNumber;
    }
    
    @Override
    public Option<Cost> getCost()
    {
        return cost;
    }

    @Override
    public Option<HostingType> getHosting()
    {
        return hosting;
    }
        
    @Override
    public boolean isWithVersion()
    {
        return withVersion;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("ProductQuery",
            app.describe(),
            describeOptEnum("cost", cost),
            describeOptEnum("hosting", hosting),
            describeOptBoolean("withVersion", withVersion),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ProductQuery) ? toString().equals(other.toString()) : false;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link ProductQuery}.  Use {@link ProductQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.ApplicationCriteria<Builder>,
        QueryBuilderProperties.Bounds<Builder>,
        QueryBuilderProperties.Cost<Builder>,
        QueryBuilderProperties.Hosting<Builder>,
        QueryBuilderProperties.WithVersion<Builder>
    {
        private QueryBuilderProperties.ApplicationCriteriaHelper app = new QueryBuilderProperties.ApplicationCriteriaHelper();
        private Option<Cost> cost = none();
        private Option<HostingType> hosting = none();
        private boolean withVersion = false;
        private QueryBounds bounds = QueryBounds.defaultBounds();

        /**
         * Returns an immutable {@link ProductQuery} based on the current builder properties.
         */
        public ProductQuery build()
        {
            return new ProductQuery(this);
        }
        
        @Override
        public Builder application(Option<ApplicationKey> application)
        {
            app = app.application(application);
            return this;
        }
        
        @Override
        public Builder appBuildNumber(Option<Integer> appBuildNumber)
        {
            app = app.appBuildNumber(appBuildNumber);
            return this;
        }
        
        @Override
        public Builder cost(Option<Cost> cost)
        {
            this.cost = checkNotNull(cost);
            return this;
        }

        @Override
        public Builder hosting(Option<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }
        
        @Override
        public Builder withVersion(boolean withVersion)
        {
            this.withVersion = withVersion;
            return this;
        }
        
        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
