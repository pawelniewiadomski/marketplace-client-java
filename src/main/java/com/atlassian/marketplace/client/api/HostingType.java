package com.atlassian.marketplace.client.api;

/**
 * Represents the available hosting models for Atlassian applications.
 * @since 2.0.0
 */
public enum HostingType implements EnumWithKey
{
    SERVER("server"),
    
    DATA_CENTER("datacenter"),
    
    CLOUD("cloud");
    
    private final String key;
    
    private HostingType(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
