package com.atlassian.marketplace.client.api;

import java.io.InputStream;

import com.atlassian.marketplace.client.MpacException;

/**
 * Used internally in {@link Page} to keep track of how to get additional pages of a result set.  You should
 * not need to refer to this class directly.
 * @since 2.0.0
 */
public abstract class PageReader<T>
{
    public abstract Page<T> readPage(PageReference<T> ref, InputStream in) throws MpacException;
    
    public static <T> PageReader<T> stub()
    {
        return new PageReader<T>()
        {
            public Page<T> readPage(PageReference<T> ref, InputStream in)
            {
                return Page.empty();
            }
        };
    }
}
