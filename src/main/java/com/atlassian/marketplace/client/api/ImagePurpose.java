package com.atlassian.marketplace.client.api;

/**
 * Constants representing preset sizes for images.
 * @since 2.0.0
 */
public enum ImagePurpose implements EnumWithKey
{
    LOGO ("logo"),
    BANNER ("banner"),
    SCREENSHOT ("screenshot"),
    THUMBNAIL ("screenshot-thumbnail"),
    TITLE_LOGO ("title-logo"),
    HERO ("hero");
    
    private final String key;
    
    private ImagePurpose(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
