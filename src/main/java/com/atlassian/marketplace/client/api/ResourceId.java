package com.atlassian.marketplace.client.api;

import java.net.URI;

import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for simple value types that wrap a resource URI.
 * @since 2.0.0
 */
public class ResourceId
{
    private final URI uri;
    
    protected ResourceId(URI uri)
    {
        this.uri = checkNotNull(uri);
    }
    
    /**
     * Returns the wrapped resource URI.
     */
    public URI getUri()
    {
        return uri;
    }
    
    @Override
    public String toString()
    {
        return this.getClass().getSimpleName() + "(" + uri + ")";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other.getClass() == this.getClass()) && ((ResourceId) other).uri.equals(uri);
    }
    
    @Override
    public int hashCode()
    {
        return uri.hashCode();
    }
    
    public static Function<ResourceId, URI> resourceIdToUri()
    {
        return new Function<ResourceId, URI>()
        {
            public URI apply(ResourceId input)
            {
                return input.getUri();
            }
        };
    }
}
