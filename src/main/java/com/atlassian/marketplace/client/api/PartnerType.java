package com.atlassian.marketplace.client.api;

/**
 * Represents the available types of pricing for Atlassian applications.
 * @since 2.0.0
 */
public enum PartnerType implements EnumWithKey
{
    DIRECT("direct"),

    EXPERT("expert"),

    RESELLER("reseller");

    private final String key;

    private PartnerType(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
