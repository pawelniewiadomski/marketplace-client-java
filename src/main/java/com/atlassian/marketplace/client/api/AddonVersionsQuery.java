package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.api.QueryProperties.describeOptEnum;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates search parameters that can be passed to {@link Addons#getVersion}.
 * @since 2.0.0
 */
public final class AddonVersionsQuery implements QueryProperties.AccessToken,
        QueryProperties.ApplicationCriteria,
        QueryProperties.Bounds,
        QueryProperties.Cost,
        QueryProperties.Hosting,
        QueryProperties.IncludePrivate
{
    private static final AddonVersionsQuery DEFAULT_QUERY = builder().build();

    private final boolean includePrivate;
    private final Option<String> accessToken;
    private final QueryBuilderProperties.ApplicationCriteriaHelper app;
    private final Option<Cost> cost;
    private final Option<HostingType> hosting;
    private final QueryBounds bounds;
    private final Option<String> afterVersionName;
    
    /**
     * Returns a new {@link Builder} for constructing an AddonVersionsQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns an AddonVersionsQuery with no criteria, which will match any available versions.
     */
    public static AddonVersionsQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    public static Builder fromAddonQuery(AddonQuery aq)
    {
        return builder()
            .accessToken(aq.getAccessToken())
            .application(aq.getApplication())
            .appBuildNumber(aq.getAppBuildNumber())
            .cost(aq.getCost())
            .hosting(aq.getHosting())
            .bounds(aq.getBounds());
    }
    
    /**
     * Returns a new {@link Builder} for constructing an AddonVersionsQuery based on an existing AddonVersionsQuery.
     */
    public static Builder builder(AddonVersionsQuery query)
    {
        Builder builder = builder()
            .application(query.getApplication())
            .appBuildNumber(query.getAppBuildNumber())
            .cost(query.getCost())
            .hosting(query.getHosting())
            .bounds(query.getBounds());

        return builder;
    }

    private AddonVersionsQuery(Builder builder)
    {
        accessToken = builder.accessToken;
        app = builder.app;
        cost = builder.cost;
        hosting = builder.hosting;
        bounds = builder.bounds;
        afterVersionName = builder.afterVersionName;
        includePrivate = builder.includePrivate;
    }
    
    @Override
    public Option<String> getAccessToken()
    {
        return accessToken;
    }
    
    @Override
    public Option<ApplicationKey> getApplication()
    {
        return app.application;
    }
    
    @Override
    public Option<Integer> getAppBuildNumber()
    {
        return app.appBuildNumber;
    }
    
    @Override
    public Option<Cost> getCost()
    {
        return cost;
    }
    
    @Override
    public Option<HostingType> getHosting()
    {
        return hosting;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }
    
    /**
     * The version string, if any, that the client has specified in order to search for
     * later versions.
     * @see Builder#afterVersion(Option)
     */
    public Option<String> getAfterVersionName()
    {
        return afterVersionName;
    }

    @Override
    public boolean isIncludePrivate() {
        return includePrivate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("AddonVersionsQuery",
            describeValues("accessToken", accessToken),
            app.describe(),
            describeOptEnum("cost", cost),
            describeOptEnum("hosting", hosting),
            bounds.describe(),
            describeValues("afterVersion", afterVersionName)
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof AddonVersionsQuery) && toString().equals(other.toString());
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link AddonVersionsQuery}.  Use {@link AddonVersionsQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.AccessToken<Builder>,
        QueryBuilderProperties.ApplicationCriteria<Builder>,
        QueryBuilderProperties.Bounds<Builder>,
        QueryBuilderProperties.Cost<Builder>,
        QueryBuilderProperties.Hosting<Builder>,
        QueryBuilderProperties.IncludePrivate<Builder>
    {
        private boolean includePrivate;
        private Option<String> accessToken = none();
        private QueryBuilderProperties.ApplicationCriteriaHelper app = new QueryBuilderProperties.ApplicationCriteriaHelper();
        private Option<Cost> cost = none();
        private Option<HostingType> hosting = none();
        private QueryBounds bounds = QueryBounds.defaultBounds();
        private Option<String> afterVersionName = none();
        
        /**
         * Returns an immutable {@link AddonVersionsQuery} based on the current builder properties.
         */
        public AddonVersionsQuery build()
        {
            return new AddonVersionsQuery(this);
        }
        
        @Override
        public Builder accessToken(Option<String> accessToken)
        {
            this.accessToken = checkNotNull(accessToken);
            return this;
        }
        
        @Override
        public Builder application(Option<ApplicationKey> application)
        {
            app = app.application(application);
            return this;
        }
        
        @Override
        public Builder appBuildNumber(Option<Integer> appBuildNumber)
        {
            app = app.appBuildNumber(appBuildNumber);
            return this;
        }
        
        @Override
        public Builder cost(Option<Cost> cost)
        {
            this.cost = checkNotNull(cost);
            return this;
        }

        @Override
        public Builder hosting(Option<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }

        @Override
        public Builder includePrivate(boolean includePrivate)
        {
            this.includePrivate = includePrivate;
            return this;
        }
        
        /**
         * Specifies an existing version if you want to query versions that are later than that
         * version.
         * @param versionName  the version string (for instance, "1.0") of an existing add-on version
         *   to restrict the query to versions later than this version, or {@link Option#none()} to
         *   remove any such restriction 
         * @return  the same query builder
         * @see AddonVersionsQuery#getAfterVersionName()
         */
        public Builder afterVersion(Option<String> versionName)
        {
            this.afterVersionName = checkNotNull(versionName);
            return this;
        }
        
        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
