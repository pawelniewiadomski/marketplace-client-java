package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Interfaces for common query builder criteria, allowing classes like {@link AddonQuery.Builder} to be treated abstractly.
 * @since 2.0.0
 */
public abstract class QueryBuilderProperties
{
    private QueryBuilderProperties()
    {
    }
    
    /**
     * Common interface for query builders that have an "access token" parameter.
     * @see QueryProperties.AccessToken
     */
    public interface AccessToken<T extends AccessToken<T>>
    {
        /**
         * Optionally includes an access token in the query, allowing access to any private add-on
         * that has such a token.
         * @param accessToken an access token string, or {@link Option#none}
         * @return  the same query builder
         * @see QueryProperties.AccessToken#getAccessToken()
         */
        T accessToken(Option<String> accessToken);
    }

    /**
     * Common interface for query builders that have "application" and "application build number" parameters.
     * @see QueryProperties.ApplicationCriteria
     */
    public interface ApplicationCriteria<T extends ApplicationCriteria<T>>
    {
        /**
         * Restricts the query to add-ons that are compatible with the specified application.
         * @param application  an {@link ApplicationKey}, or {@link Option#none} for no application filter
         * @return  the same query builder
         * @see QueryBuilderProperties.ApplicationCriteria#application(Option)
         */
        T application(Option<ApplicationKey> application);
        
        /**
         * Restricts the query to add-ons that are compatible with the specified application version.
         * This is ignored if you have not specified {@link #application}.
         * @param appBuildNumber  the application build number, or {@link Option#none} for no application
         *   version filter
         * @return  the same query builder
         * @see QueryBuilderProperties.ApplicationCriteria#appBuildNumber(Option)
         */
        T appBuildNumber(Option<Integer> appBuildNumber);
    }

    /**
     * Common interface for query builders that have "offset" and "limit" parameters.
     */
    public interface Bounds<T extends Bounds<T>>
    {
        /**
         * Sets the starting offset and/or the maximum result page size for the query, from a
         * {@link QueryBounds} instance.
         * @param bounds  a {@link QueryBounds} specifying an offset and/or limit
         * @return  the same query builder
         */
        T bounds(QueryBounds bounds);
    }

    /**
     * Common interface for query builders that have a "cost" parameter.
     */
    public interface Cost<T extends Cost<T>>
    {
        /**
         * Restricts the query to add-ons/products that match the given {@link com.atlassian.marketplace.client.api.Cost} criteria.
         * @param cost  a {@link com.atlassian.marketplace.client.api.Cost} value, or {@link Option#none} for no cost filter
         * @return  the same query builder
         */
        T cost(Option<com.atlassian.marketplace.client.api.Cost> cost);
    }
    
    /**
     * Common interface for query builders that have a "hosting" parameter.
     */
    public interface Hosting<T extends Hosting<T>>
    {
        /**
         * Restricts the query to add-ons that support the given hosting type.
         * @param hosting  a {@link HostingType} constant, or {@link Option#none} for no hosting filter
         * @return  the same query builder
         */
        T hosting(Option<HostingType> hosting);
    }

    /**
     * Common interface for query builders that have an "includePrivate" parameter.
     */
    public interface IncludePrivate<T extends IncludePrivate<T>>
    {
        /**
         * Includes private results in the query results.
         * @param includePrivate whether to include private entities in the query results
         * @return the same query builder
         */
        T includePrivate(boolean includePrivate);
    }

    /**
     * Common interface for query builders that have a "withVersion" parameter.
     */
    public interface WithVersion<T extends WithVersion<T>>
    {
        /**
         * Specifies whether the result set should include version-level properties.  This is false by default.
         * @param withVersion  true if the result set should include version data
         * @return  the same query builder
         */
        T withVersion(boolean withVersion);
    }
    
    // Helper classes beyond this point are used internally to simplify query builder implementations.

    static class ApplicationCriteriaHelper
    {
        public final Option<ApplicationKey> application;
        public final Option<Integer> appBuildNumber;

        public ApplicationCriteriaHelper()
        {
            this(none(ApplicationKey.class), none(Integer.class));
        }
        
        private ApplicationCriteriaHelper(Option<ApplicationKey> application, Option<Integer> appBuildNumber)
        {
            this.application = application;
            this.appBuildNumber = appBuildNumber;
        }
        
        public ApplicationCriteriaHelper application(Option<ApplicationKey> application)
        {
            return new ApplicationCriteriaHelper(checkNotNull(application), this.appBuildNumber);
        }
        
        public ApplicationCriteriaHelper appBuildNumber(Option<Integer> appBuildNumber)
        {
            return new ApplicationCriteriaHelper(this.application, checkNotNull(appBuildNumber));
        }
        
        public Iterable<String> describe()
        {
            ImmutableList.Builder<String> ret = ImmutableList.builder();
            for (ApplicationKey a: application)
            {
                ret.add("application(" + a.getKey() + ")");
            }
            for (Integer ab: appBuildNumber)
            {
                ret.add("appBuildNumber(" + ab + ")");
            }
            return ret.build();
        }
    }
}
