package com.atlassian.marketplace.client.api;

import java.util.Iterator;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import static com.atlassian.fugue.Option.none;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.isEmpty;

/**
 * A subset of items returned by a query. The maximum size of the subset is determined by the
 * <tt>limit</tt> parameter used in the query, or by the server's maximum result set size,
 * whichever is lower.
 * <p>
 * The items themselves are accessed by treating the page as an Iterable.
 * <p>
 * To get the next or previous page of query results, if any, pass a {@link PageReference}
 * obtained from {@link #getNext()} or {@link #getPrevious()} to the client's generic
 * {@link com.atlassian.marketplace.client.MarketplaceClient#getMore} method.
 * for the type of items.  For instance, for plugin listings:
 * <pre>
 *     Page&lt;AddonSummary&gt; aPage = client.addons().find(addonQuery);
 *     if (aPage.getNext().isDefined())
 *     {
 *         for (PageReference&lt;AddonSummary&gt; next: aPage.getNext())
 *         {
 *             Page&lt;AddonSummary&gt; nextPage = client.getMore(next);
 *         }
 *     }
 * </pre>
 */
public abstract class Page<T> implements Iterable<T>
{
    private final ImmutableList<T> items;
    private final int totalSize;
    protected final PageReader<T> reader;

    /**
     * Returns a Page containing no items, with no server URI ({@link #getReference()}, {@link #getNext()},
     * and {@link #getPrevious()} will all return {@link Option#none()}).
     */
    @SuppressWarnings("unchecked")
    public static <T> Page<T> empty()
    {
        return (Page<T>) EMPTY_PAGE;
    }

    /**
     * Returns a Page containing no items, with no server URI ({@link #getReference()}, {@link #getNext()},
     * and {@link #getPrevious()} will all return {@link Option#none()}).
     */
    @SuppressWarnings("unchecked")
    public static <T> Page<T> empty(final Class<T> type)
    {
        return (Page<T>) EMPTY_PAGE;
    }

    /**
     * Returns a Page from a fixed list of items, with no server URI ({@link #getReference()},
     * {@link #getNext()}, and {@link #getPrevious()} will all return {@link Option#none()}).
     */
    public static <T> Page<T> fromItems(Iterable<T> items)
    {
        return isEmpty(items) ? Page.<T>empty() : new FixedPage<T>(items);
    }
    
    protected Page(Iterable<T> items, int totalSize, PageReader<T> reader)
    {
        this.items = ImmutableList.copyOf(checkNotNull(items, "items"));
        this.totalSize = totalSize;
        this.reader = checkNotNull(reader);
    }
    
    @Override
    public Iterator<T> iterator()
    {
        return items.iterator();
    }
    
    /**
     * The number of items on this page.
     */
    public int size()
    {
        return items.size();
    }
    
    /**
     * The number of items in the entire result set, of which this page is a subset.
     */
    public int totalSize()
    {
        return totalSize;
    }
    
    /**
     * Returns a reference to the address of this query page on the server allowing it to be requeried
     * in the future, or {@link Option#none()} if it does not exist on the server.
     */
    public abstract Option<PageReference<T>> getReference();
    
    /**
     * If there are other items before this subset in the full result set, returns a {@link PageReference}
     * allowing you to query the previous page; otherwise returns {@link Option#none()}.
     */
    public abstract Option<PageReference<T>> getPrevious();
    
    /**
     * If there are other items after this subset in the full result set, returns a {@link PageReference}
     * allowing you to query the next page; otherwise returns {@link Option#none()}.
     */
    public abstract Option<PageReference<T>> getNext();

    /**
     * Shortcut for <tt>getReference().get().getOffset()</tt>, but returns zero if <tt>getReference()</tt>
     * is <tt>none()</tt>.
     */
    public int getOffset()
    {
        for (PageReference<T> ref: getReference())
        {
            return ref.getBounds().getOffset();
        }
        return 0;
    }
    
    private static final Page<Object> EMPTY_PAGE = new FixedPage<Object>(ImmutableList.<Object>of());
    
    private static final class FixedPage<T> extends Page<T>
    {
        FixedPage(Iterable<T> items)
        {
            super(ImmutableList.copyOf(items), Iterables.size(items), PageReader.<T>stub());
        }
        
        @Override
        public Option<PageReference<T>> getReference()
        {
            return none();
        }
        
        @Override
        public Option<PageReference<T>> getPrevious()
        {
            return none();
        }
        
        @Override
        public Option<PageReference<T>> getNext()
        {
            return none();
        }
    };
}
