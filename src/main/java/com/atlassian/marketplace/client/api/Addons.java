package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonPricing;
import com.atlassian.marketplace.client.model.AddonReference;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionSummary;

/**
 * Starting point for all resources that return add-on information.  Use {@link MarketplaceClient#addons()}
 * to access this API.
 * @since 2.0.0
 */
public interface Addons
{
    /**
     * Queries detail information for a single add-on.
     * @param addonKey  the add-on's unique key
     * @param query  an {@link AddonQuery} which may contain criteria to limit the search, such as the
     *   compatible application version; use {@link AddonQuery#any()} if there are no additional criteria
     * @return the add-on information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<Addon> getByKey(String addonKey, AddonQuery query) throws MpacException;
    
    /**
     * Returns a list of add-ons.
     * @param query  an {@link AddonQuery}
     * @return summary information for the available add-ons
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<AddonSummary> find(AddonQuery query) throws MpacException;

    /**
     * Creates a new add-on listing.  Assuming all properties are valid, if the add-on is private
     * it will be created in a private state immediately; if it is public, it will be created in a "submitted"
     * state and will not be visible until it has been approved by Atlassian.
     * <p>
     * Use {@link com.atlassian.marketplace.client.model.ModelBuilders#addon} to build a new
     * {@link Addon} object. The add-on must include an initial version; to set this, use
     * {@link com.atlassian.marketplace.client.model.ModelBuilders.AddonBuilder#version(Option)}.
     * 
     * @param addon the add-on to create
     * @return an {@link Addon} representing the add-on in its initial state after being created
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Addon createAddon(Addon addon) throws MpacException;
    
    /**
     * Attempts to modify an existing add-on.  You must have already queried the current state
     * of the add-on with {@link Addons#getByKey(String, com.atlassian.marketplace.client.api.AddonQuery)};
     * then, build another instance that includes any changes you want to make, using
     * {@link com.atlassian.marketplace.client.model.ModelBuilders#addon(Addon)}.
     * 
     * @param original the existing add-on
     * @param updated a copy of the add-on that includes some changes
     * @return the updated add-on (re-queried from the server) if successful
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Addon updateAddon(Addon original, Addon updated) throws MpacException;
    
    /**
     * Queries one version of an add-on.
     * @param addonKey  the add-on's unique key
     * @param version  an {@link AddonVersionSpecifier} which designates either a specific
     *   version or the latest version
     * @param query  an {@link AddonVersionsQuery} which may contain criteria to limit the search, such as the
     *   compatible application version; use {@link AddonVersionsQuery#any()} if there are no additional criteria
     * @return the add-on version information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<AddonVersion> getVersion(String addonKey, AddonVersionSpecifier version, AddonVersionsQuery query) throws MpacException;

    /**
     * Gets a list of versions for an add-on.
     * @param addonKey  the add-on's unique key
     * @param versionsQuery  an {@link AddonVersionsQuery} which may contain criteria to limit the search, such as the
     *   compatible application version; use {@link AddonVersionsQuery#any()} if there are no additional criteria
     * @return the add-on versions
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<AddonVersionSummary> getVersions(String addonKey, AddonVersionsQuery versionsQuery) throws MpacException;

    /**
     * Creates a new version for an existing add-on.  Assuming all properties are valid,
     * if the version requires approval for any reason (such as being the first paid-via-Atlassian version of
     * a formerly free add-on, or the first Cloud version of a Server add-on) it will be created in a
     * "submitted" state pending Atlassian approval; otherwise it will be public or private as you specified.
     * <p>
     * Use {@link com.atlassian.marketplace.client.model.ModelBuilders#addonVersion} to build a new
     * {@link AddonVersion} object.
     *
     * @param addonKey unique key of the existing add-on
     * @param version the add-on version to create
     * @return an {@link AddonVersion} representing the version in its initial state after being created
     * @throws MpacException  if the server was unavailable or returned an error
     */
    AddonVersion createVersion(String addonKey, AddonVersion version) throws MpacException;

    /**
     * Attempts to modify an existing add-on version.  You must have already queried the current state
     * of the version with {@link Addons#getVersion(String, AddonVersionSpecifier, AddonVersionsQuery)}; 
     * then, build another instance that includes any changes you want to make, using
     * {@link com.atlassian.marketplace.client.model.ModelBuilders#addonVersion(AddonVersion)}.
     * 
     * @param original the existing add-on version
     * @param updated a copy of the version that includes some changes
     * @return the updated version (re-queried from the server) if successful
     * @throws MpacException  if the server was unavailable or returned an error
     */
    AddonVersion updateVersion(AddonVersion original, AddonVersion updated) throws MpacException;
    
    /**
     * Queries the current pricing (the prices that are publicly visible on the Marketplace site) for
     * an add-on.  Returns {@code none()} if the add-on is not paid via Atlassian, is not available
     * for the given hosting model, or has not yet been approved.
     * @param addonKey unique key of the add-on
     * @param pricingType specifies pricing for Cloud or Server
     * @return the pricing, if available
     * @throws MpacException if the server was unavailable or returned an error
     */
    Option<AddonPricing> getPricing(String addonKey, PricingType pricingType) throws MpacException;
    
    /**
     * Similar to {@link #find}, but further restricts the query to add-ons that have a banner image,
     * and returns minimal {@link AddonReference} results, whose {@link AddonReference#getImage()} method
     * will return the banner image. 
     * @param query  an {@link AddonQuery}
     * @return a list of matching add-ons
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<AddonReference> findBanners(AddonQuery query) throws MpacException;
    
    /**
     * Returns add-ons that Marketplace considers similar to the specified add-on in some way,
     * optionally constrained by compatibility or other {@link AddonQuery} properties.
     * @param addonKey  the add-on's unique key
     * @param query  an {@link AddonQuery} which may contain criteria to limit the search, such as the
     *   compatible application version; use {@link AddonQuery#any()} if there are no additional criteria
     * @return a list of recommended add-ons
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<AddonReference> findRecommendations(String addonKey, AddonQuery query) throws MpacException;

    /**
     * Checks if the access token for the given add-on is valid for this application instance to use,
     * and if so, marks it as claimed by this instance (based on the current hostname).  
     * @param addonKey  the add-on's unique key
     * @param token  the license token to check
     * @return true if the token is valid for this add-on and can be used by this instance; false otherwise
     * @throws MpacException  if the server was unavailable or returned an error
     */
    boolean claimAccessToken(String addonKey, String token) throws MpacException;
}
