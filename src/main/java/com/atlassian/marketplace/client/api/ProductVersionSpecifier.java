package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;

/**
 * Encapsulates parameters that can be passed to {@link Products#getVersion}.
 * @since 2.0.0
 */
public final class ProductVersionSpecifier
{
    private final Option<Either<Integer, String>> nameOrBuild;
    
    private ProductVersionSpecifier(Option<Either<Integer, String>> nameOrBuild)
    {
        this.nameOrBuild = nameOrBuild;
    }
    
    /**
     * Searches for a product version by build number.
     */
    public static ProductVersionSpecifier buildNumber(int buildNumber)
    {
        return new ProductVersionSpecifier(some(Either.<Integer, String>left(buildNumber)));
    }

    /**
     * Searches for a product version by name (version string).
     */
    public static ProductVersionSpecifier name(String name)
    {
        return new ProductVersionSpecifier(some(Either.<Integer, String>right(name)));
    }

    /**
     * Searches for the latest version.
     */
    public static ProductVersionSpecifier latest()
    {
        return new ProductVersionSpecifier(Option.<Either<Integer, String>>none());
    }
    
    public Option<Integer> getBuildNumber()
    {
        for (Either<Integer, String> nb: nameOrBuild)
        {
            return nb.left().toOption();
        }
        return none();
    }

    public Option<String> getName()
    {
        for (Either<Integer, String> nb: nameOrBuild)
        {
            return nb.right().toOption();
        }
        return none();
    }

    @Override
    public String toString()
    {
        for (Integer b: getBuildNumber())
        {
            return "buildNumber(" + b + ")";            
        }
        for (String n: getName())
        {
            return "name(" + n + ")";
        }
        return "latest";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ProductVersionSpecifier) && ((ProductVersionSpecifier) other).nameOrBuild.equals(this.nameOrBuild);
    }
    
    @Override
    public int hashCode()
    {
        return nameOrBuild.hashCode();
    }
}