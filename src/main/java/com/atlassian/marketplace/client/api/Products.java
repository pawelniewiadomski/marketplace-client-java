package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Product;
import com.atlassian.marketplace.client.model.ProductVersion;

/**
 * Starting point for all resources that return product information.  Use
 * {@link MarketplaceClient#products()} to access this API.
 * <p>
 * A "product", in this context, is an Atlassian software package that can be either purchased separately
 * or installed into a platform instance, such as JIRA Service Desk.  These are called "applications" in
 * the user-facing UI, but that term is already used for a different purpose in the Marketplace model
 * (see {@link Applications}).
 * 
 * @since 2.0.0
 */
public interface Products
{
    /**
     * Queries a single product.
     * @param productKey  the product's unique key
     * @param query  a {@link ProductQuery} which may contain criteria to limit the search, such as the
     *   compatible application version; use {@link ProductQuery#any()} if there are no additional criteria
     * @return the product information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<Product> getByKey(String productKey, ProductQuery query) throws MpacException;
    
    /**
     * Queries a specific version of a product.
     * @param productKey  the product's unique key
     * @param versionQuery  search criteria for identifying a single version
     * @return the product version information, or {@link Option#none()} if there is no match
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Option<ProductVersion> getVersion(String productKey, ProductVersionSpecifier versionQuery) throws MpacException;
    
    /**
     * Returns a list of products.
     * @param query  a {@link ProductQuery}
     * @return the available products
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Page<Product> find(ProductQuery query) throws MpacException;
}
