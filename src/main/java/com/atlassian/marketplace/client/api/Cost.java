package com.atlassian.marketplace.client.api;

import com.atlassian.marketplace.client.model.PaymentModel;

/**
 * Constants for querying add-ons according to their paid/free attributes.
 */
public enum Cost implements EnumWithKey
{
    /**
     * Restricts the query to add-ons whose payment model is {@link PaymentModel#FREE}.
     */
    FREE ("free"),
    /**
     * Restricts the query to add-ons whose payment model is either
     * {@link PaymentModel#PAID_VIA_VENDOR} or {@link PaymentModel#PAID_VIA_ATLASSIAN}.
     */
    ALL_PAID ("paid"),
    /**
     * Restricts the query to add-ons whose payment model is
     * {@link PaymentModel#PAID_VIA_ATLASSIAN}.
     */
    PAID_VIA_ATLASSIAN ("marketplace");

    private final String key;
    
    private Cost(String key)
    {
        this.key = key;
    }
    
    @Override
    public String getKey()
    {
        return key;
    }
}
