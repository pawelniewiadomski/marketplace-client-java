package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Option;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Holds the pagination parameters {@code offset} and {@code limit}, which are supported by many queries.
 * @since 2.0.0
 */
public class QueryBounds
{
    private static final QueryBounds DEFAULT = new QueryBounds(0, none(Integer.class));
    private static final QueryBounds EMPTY = new QueryBounds(0, some(0));
    
    private final int offset;
    private final Option<Integer> limit;
    
    private QueryBounds(int offset, Option<Integer> limit)
    {
        checkArgument(offset >= 0, "offset may not be negative");
        for (int l: checkNotNull(limit))
        {
            checkArgument(l >= 0, "limit may not be negative");
        }
        
        this.offset = offset;
        this.limit = limit;
    }
    
    /**
     * Constructs a new {@link QueryBounds} instance that specifies some number of items to skip past at the
     * start of the result set.
     * @param offset  the number of items to skip
     * @return  a {@link QueryBounds} instance
     */
    public static QueryBounds offset(int offset)
    {
        return new QueryBounds(offset, none(Integer.class));
    }

    /**
     * Constructs a new {@link QueryBounds} instance that optionally specifies the maximum number of items
     * to be included in each page of the result set.  Note that the server may choose to impose a
     * smaller maximum limit.
     * @param limit  the maximum number of items per result page, or {@link Option#none()} to leave this unspecified 
     * @return  a {@link QueryBounds} instance
     */
    public static QueryBounds limit(Option<Integer> limit)
    {
        return new QueryBounds(0, limit);
    }

    /**
     * Returns a {@link QueryBounds} instance with no {@link #offset} or {@link #limit} specified.
     */
    public static QueryBounds defaultBounds()
    {
        return DEFAULT;
    }

    /**
     * Returns a {@link QueryBounds} instance with {@link #limit} set to <tt>some(0)</tt>, so that the
     * result set will always be empty.  This is useful if you want to query a collection resource
     * simply to find out the total number of items it contains, or to look at its links, without
     * actually accessing the items.
     */
    public static QueryBounds empty()
    {
        return EMPTY;
    }
    
    /**
     * The number of items that the client wants to skip past at the start of the result set.
     */
    public int getOffset()
    {
        return offset;
    }
    
    /**
     * The maximum number of items that the client wants to receive in the result set.
     */
    public Option<Integer> getLimit()
    {
        return limit;
    }
    
    /**
     * Returns a copy of this QueryBounds instance with the <tt>offset</tt> property changed.
     * @see QueryBounds#offset
     */
    public QueryBounds withOffset(int offset)
    {
        return new QueryBounds(offset, this.limit);
    }
    
    /**
     * Returns a copy of this QueryBounds instance with the <tt>limit</tt> property changed.
     * @see QueryBounds#limit
     */
    public QueryBounds withLimit(Option<Integer> limit)
    {
        return new QueryBounds(this.offset, limit);
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof QueryBounds)
        {
            QueryBounds o = (QueryBounds) other;
            return offset == o.offset && limit.equals(o.limit);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return offset + limit.hashCode();
    }
    
    public Iterable<String> describe()
    {
        ImmutableList.Builder<String> ret = ImmutableList.builder();
        if (offset > 0)
        {
            ret.add("offset(" + offset + ")");
        }
        for (Integer l: limit)
        {
            ret.add("limit(" + l + ")");
        }
        return ret.build();
    }
}
