package com.atlassian.marketplace.client.api;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.trimToNull;

/**
 * A string that uniquely identifies one of the applications known to the Atlassian
 * Marketplace.
 */
public final class ApplicationKey
{
    public static final ApplicationKey BAMBOO = new ApplicationKey("bamboo");
    public static final ApplicationKey BITBUCKET = new ApplicationKey("bitbucket");
    public static final ApplicationKey CONFLUENCE = new ApplicationKey("confluence");
    public static final ApplicationKey FECRU = new ApplicationKey("fecru");
    public static final ApplicationKey HIPCHAT = new ApplicationKey("hipchat");
    public static final ApplicationKey JIRA = new ApplicationKey("jira");

    private static final ApplicationKey[] PREDEFINED =
        { BAMBOO, BITBUCKET, CONFLUENCE, FECRU, HIPCHAT, JIRA };
    
    private final String key;

    private ApplicationKey(String key)
    {
        this.key = key;
    }
    
    /**
     * Returns an ApplicationKey instance corresponding to the given string value.  This is
     * case-insensitive, e.g. <tt>valueOf("Bamboo")</tt> will return {@link #BAMBOO}. 
     * @param key  an application key string
     * @return  an {@link ApplicationKey}
     * @throws NullPointerException  if key is null, empty, or whitespace
     */
    public static ApplicationKey valueOf(String key)
    {
        String s = checkNotNull(trimToNull(key)).toLowerCase();
        for (ApplicationKey a: PREDEFINED)
        {
            if (a.key.equals(s))
            {
                return a;
            }
        }
        return new ApplicationKey(s);
    }
    
    /**
     * Returns the string key used for this application in the Marketplace API.
     */
    public String getKey()
    {
        return key;
    }

    @Override
    public boolean equals(Object other)
    {
        if (other instanceof ApplicationKey)
        {
            return key.equals(((ApplicationKey) other).key);
        }
        return false;
    }
    
    @Override
    public int hashCode()
    {
        return key.hashCode();
    }
    
    @Override
    public String toString()
    {
        return "ApplicationKey(" + key + ")";
    }
}
