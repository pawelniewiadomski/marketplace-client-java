package com.atlassian.marketplace.client.api;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;

import static com.atlassian.fugue.Option.some;

/**
 * Encapsulates parameters that can be passed to {@link Applications#getVersion}.
 */
public final class ApplicationVersionSpecifier
{
    private final Option<Either<String, Integer>> nameOrBuild;
    
    private ApplicationVersionSpecifier(Option<Either<String, Integer>> nameOrBuild)
    {
        this.nameOrBuild = nameOrBuild;
    }
    
    /**
     * Searches for an application version by build number.
     */
    public static ApplicationVersionSpecifier buildNumber(int buildNumber)
    {
        return new ApplicationVersionSpecifier(some(Either.<String, Integer>right(buildNumber)));
    }
    
    /**
     * Searches for an application version by name (version string, e.g. "1.0.0").
     */
    public static ApplicationVersionSpecifier versionName(String name)
    {
        return new ApplicationVersionSpecifier(some(Either.<String, Integer>left(name))); 
    }
    
    /**
     * Searches for the latest version.
     */
    public static ApplicationVersionSpecifier latest()
    {
        return new ApplicationVersionSpecifier(Option.<Either<String, Integer>>none());
    }
    
    /**
     * Used internally to access the previously set details of the specifier.  
     */
    public Option<Either<String, Integer>> getSpecifiedVersion()
    {
        return nameOrBuild;
    }
    
    @Override
    public String toString()
    {
        for (Either<String, Integer> vob: nameOrBuild)
        {
            for (Integer b: vob.right())
            {
                return "buildNumber(" + b + ")";
            }
            for (String n: vob.left())
            {
                return "name(" + n + ")";
            }
        }
        return "latest";
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof ApplicationVersionSpecifier) && ((ApplicationVersionSpecifier) other).nameOrBuild.equals(this.nameOrBuild);
    }
    
    @Override
    public int hashCode()
    {
        return nameOrBuild.hashCode();
    }
}
