package com.atlassian.marketplace.client;

import java.net.URI;

import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.impl.DefaultMarketplaceClient;

/**
 * Static factory methods for creating the standard implementation of {@link MarketplaceClient}.
 * @since 2.0.0
 */
public abstract class MarketplaceClientFactory
{
    /**
     * The standard base URI of the Atlassian Marketplace server.
     */
    public static final URI DEFAULT_MARKETPLACE_URI = URI.create("https://marketplace.atlassian.com");
    
    private MarketplaceClientFactory()
    {
    }
    
    /**
     * Creates a {@link MarketplaceClient} instance using a specific server URI and configuration.
     * @param baseUri  the base URI of the Atlassian Marketplace server
     * @param configuration  an {@link HttpConfiguration} object
     */
    public static MarketplaceClient createMarketplaceClient(URI baseUri, HttpConfiguration configuration)
    {
        return new DefaultMarketplaceClient(baseUri, configuration);
    }

    /**
     * Creates a {@link MarketplaceClient} instance using a specific server URI, using defaults for all
     * other configuration properties.
     * @param baseUri  the base URI of the Atlassian Marketplace server
     */
    public static MarketplaceClient createMarketplaceClient(URI baseUri)
    {
        return createMarketplaceClient(baseUri, HttpConfiguration.defaults());
    }

    /**
     * Creates a {@link MarketplaceClient} instance using a specific configuration, and the default
     * server URI.
     * @param configuration  an {@link HttpConfiguration} object
     */
    public static MarketplaceClient createMarketplaceClient(HttpConfiguration configuration)
    {
        return createMarketplaceClient(DEFAULT_MARKETPLACE_URI, configuration);
    }
    
    /**
     * Creates a {@link MarketplaceClient} instance using the default server URI and configuration.
     */
    public static MarketplaceClient createMarketplaceClient()
    {
        return createMarketplaceClient(DEFAULT_MARKETPLACE_URI);
    }
}
