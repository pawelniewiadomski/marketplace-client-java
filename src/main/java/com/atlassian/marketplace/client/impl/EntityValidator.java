package com.atlassian.marketplace.client.impl;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.encoding.MissingRequiredField;
import com.atlassian.marketplace.client.encoding.SchemaViolation;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.RequiredLink;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonParseException;

import static com.atlassian.fugue.Option.none;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.isEmpty;

/**
 * Provides post-processing of generic model instances after they have gone through the standard
 * JSON deserialization.  This consists of the following:
 * <ul>
 * <li> For any field with an {@code Option} type, if the field's value is null (meaning that the
 * field was omitted in the JSON document) we set it to {@code none()}.  This is necessary because
 * there's no concept of default values in Gson.
 * <li> For any field with a {@code @RequiredLink} annotation, we look for the corresponding link
 * within the {@code _links} property and set it to that value, or signal an error if the link is
 * missing.  This ensures, for instance, that an object can promise that it always has a {@code self}
 * link and we will never return an instance that does not.
 * <li> For any other field, if its value is null we signal an error.  Thus, no model properties are
 * nullable; optional fields must always use {@code Option}.
 * </ul>  
 * @since 2.0.0
 */
public abstract class EntityValidator
{
    private static ConcurrentHashMap<Class<?>, Map<String, Field>> classFields =
        new ConcurrentHashMap<Class<?>, Map<String,Field>>();
    
    public static <T> T validateInstance(T instance) throws SchemaViolationException
    {
        Iterable<SchemaViolation> violations = ImmutableList.of();
        for (Field f: getClassFields(instance.getClass()).values())
        {
            violations = concat(violations, postProcessField(f, instance));
        }
        if (!isEmpty(violations))
        {
            throw new SchemaViolationException(violations);
        }
        return instance;
    }
    
    private static Iterable<SchemaViolation> postProcessField(Field f, Object o)
    {
        f.setAccessible(true);
        try
        {
            if (f.get(o) == null)
            {
                // The field did not have a value in the source document (or had an explicit null value)
                if (Option.class.isAssignableFrom(f.getType()))
                {
                    // That's OK, it's an optional field, just replace the null with none()
                    f.set(o, none());
                }
                else
                {
                    RequiredLink reqLinkAnno = f.getAnnotation(RequiredLink.class);
                    if (reqLinkAnno != null)
                    {
                        // It's a calculated field derived from a link
                        return setRequiredLinkField(reqLinkAnno, f, o);
                    }
                    else
                    {
                        // It's not optional and it's not a calculated field, so this is an error
                        return Option.<SchemaViolation>some(new MissingRequiredField(o.getClass(), f.getName()));
                    }
                }
            }
            // No schema violations detected
            return none();
        }
        catch (IllegalAccessException e)
        {
            // This should never happen
            throw new JsonParseException(e);
        }
    }
    
    private static Iterable<SchemaViolation> setRequiredLinkField(RequiredLink anno, Field f, Object o) throws IllegalAccessException
    {
        Field linksField = getClassFields(o.getClass()).get("_links");
        if (linksField == null || linksField.getType() != Links.class)
        {
            throw new IllegalStateException("@RequiredLink annotation was found in a class without a 'Links _links' field");
        }
        Links links = (Links) linksField.get(o);
        for (URI u: links.getUri(anno.rel()))
        {
            f.set(o, u);
            return none();
        }
        return Option.<SchemaViolation>some(new MissingRequiredField(o.getClass(), "_links." + anno.rel()));
    }
    
    public static Map<String, Field> getClassFields(Class<?> c)
    {
        // Unfortunately this logic is necessary because Class.getFields() won't give you private fields,
        // but Class.getDeclaredFields() won't give you any superclass fields.
        Map<String, Field> m = classFields.get(c);
        if (m == null)
        {
            m = new HashMap<String, Field>();
            Class<?> c1 = c;
            while (c1 != null)
            {
                for (Field f: c1.getDeclaredFields())
                {
                    f.setAccessible(true);
                    m.put(f.getName(), f);
                }
                c1 = c1.getSuperclass();
            }
            classFields.put(c, m);
        }
        return m;
    }
}
