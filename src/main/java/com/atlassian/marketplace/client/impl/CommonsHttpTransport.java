package com.atlassian.marketplace.client.impl;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthMethod;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyConfiguration;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.http.RequestDecorator;
import com.atlassian.marketplace.client.http.SimpleHttpResponse;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AUTH;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static java.lang.System.getProperty;
import static org.apache.commons.lang.StringUtils.trimToNull;
import static org.apache.http.client.config.CookieSpecs.IGNORE_COOKIES;
import static org.apache.http.client.protocol.HttpClientContext.AUTH_CACHE;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.protocol.HTTP.CONTENT_TYPE;
import static org.apache.http.protocol.HttpCoreContext.HTTP_TARGET_HOST;
import static org.apache.http.util.EntityUtils.consumeQuietly;

/**
 * Implementation of {@link HttpTransport} based on Apache HttpComponents.
 * @since 2.0.0
 */
public final class CommonsHttpTransport implements HttpTransport
{
    private static final Logger logger = LoggerFactory.getLogger(MarketplaceClient.class);
    private static final ContentType APPLICATION_JSON_PATCH =
            ContentType.create("application/json-patch+json", Consts.UTF_8);
    
    private final HttpClient client;
    private final HttpConfiguration config;
    private final HttpTransport defaultOperations;

    /**
     * Use with {@link CommonsHttpTransport#createHttpClient} to indicate whether or not to
     * enable HTTP caching.
     */
    public enum CachingBehavior
    {
        NO_CACHING,
        CACHING
    }
    
    public CommonsHttpTransport(HttpConfiguration configuration, URI baseUri)
    {
        this.config = checkNotNull(configuration, "configuration");
        this.client = httpClientBuilder(config, some(baseUri), CachingBehavior.CACHING).build();
        this.defaultOperations = new OperationsImpl(ImmutableList.<RequestDecorator>of());
    }
    
    @Override
    public SimpleHttpResponse get(URI uri) throws MpacException
    {
        return defaultOperations.get(uri);
    }

    @Override
    public SimpleHttpResponse postParams(URI uri, Multimap<String, String> params) throws MpacException
    {
        return defaultOperations.postParams(uri, params);
    }

    @Override
    public SimpleHttpResponse post(URI uri, InputStream content, long length, String contentType, String acceptContentType) throws MpacException
    {
        return defaultOperations.post(uri, content, length, contentType, acceptContentType);
    }

    @Override
    public SimpleHttpResponse put(URI uri, byte[] content) throws MpacException
    {
        return defaultOperations.put(uri, content);
    }

    @Override
    public SimpleHttpResponse patch(URI uri, byte[] content) throws MpacException
    {
        return defaultOperations.patch(uri, content);
    }
    
    @Override
    public SimpleHttpResponse delete(URI uri) throws MpacException
    {
        return defaultOperations.delete(uri);
    }

    @Override
    public HttpTransport withRequestDecorator(RequestDecorator decorator)
    {
        return defaultOperations.withRequestDecorator(decorator);
    }
    
    /**
     * Should be called to shutdown the Client HttpConnectionManager, this will prevent finaliser errors
     * from appearing in the log, and helps to prevent a resource leak.
     */
    @Override
    public void close()
    {
        if (client instanceof Closeable)
        {
            try
            {
                ((CloseableHttpClient) client).close();
            }
            catch (IOException e)
            {
                logger.warn("Unexpected error while closing HTTP client: " + e);
                logger.debug(e.toString(), e);
            }
        }
    }
    
    /**
     * Helper method that configures an HttpClient with the specified proxy/timeout properties.
     * All requests done through the resulting client will use the configured settings, in terms of
     * timeouts, proxies, {@link RequestDecorator}s, etc.
     *  
     * @param config  an {@link HttpConfiguration}
     * @param baseUri  base URI of the Marketplace server; this is only relevant if you're providing
     * basicauth credentials for the server itself (rather than for the proxy)
     * @return  a configured HttpClient
     */
    public static HttpClient createHttpClient(HttpConfiguration config, Option<URI> baseUri)
    {
        return httpClientBuilder(config, baseUri, CachingBehavior.NO_CACHING).build();
    }

    /**
     * Helper method that configures an HttpClientBuilder with the specified proxy/timeout properties.
     * All requests done through the resulting client will use the configured settings, in terms of
     * timeouts, proxies, {@link RequestDecorator}s, etc.
     *  
     * @param config  an {@link HttpConfiguration}
     * @param baseUri  base URI of the Marketplace server; this is only relevant if you're providing
     * basicauth credentials for the server itself (rather than for the proxy)
     * @param cachingBehavior  determines whether to use a caching client
     * @return  a configured HttpClientBuilder
     */
    public static HttpClientBuilder httpClientBuilder(HttpConfiguration config,
            Option<URI> baseUri, CachingBehavior cachingBehavior)
    {
        HttpClientBuilder builder;
        if (cachingBehavior == CachingBehavior.CACHING)
        {
            CachingHttpClientBuilder cachingBuilder = CachingHttpClientBuilder.create();
            CacheConfig.Builder configBuilder = CacheConfig.custom();
            configBuilder.setSharedCache(false);
            configBuilder.setMaxCacheEntries(config.getMaxCacheEntries());
            configBuilder.setMaxObjectSize(config.getMaxCacheObjectSize());
            cachingBuilder.setCacheConfig(configBuilder.build());
            builder = cachingBuilder;
        }
        else
        {
            builder = HttpClientBuilder.create();
        }
        builder.useSystemProperties();
        builder.setMaxConnPerRoute(config.getMaxConnections());

        RequestConfig.Builder rc = RequestConfig.custom()
            .setConnectTimeout(config.getConnectTimeoutMillis())
            .setSocketTimeout(config.getReadTimeoutMillis())
            .setCookieSpec(IGNORE_COOKIES)
            .setProxyPreferredAuthSchemes(getProxyPreferredAuthSchemes(config));
        for (int maxRedirects: config.getMaxRedirects())
        {
            rc.setMaxRedirects(maxRedirects);
        }
        builder.setDefaultRequestConfig(rc.build());

        Option<HttpConfiguration.ProxyHost> realProxyHost = getRealProxyHost(config, baseUri);
        for (HttpConfiguration.ProxyHost ph: realProxyHost)
        {
            builder.setProxy(new HttpHost(ph.getHostname(), ph.getPort()));
        }

        builder.addInterceptorFirst(new DefaultRequestInterceptor(config, realProxyHost));
        builder.setDefaultCredentialsProvider(new DefaultCredentialsProvider(config, realProxyHost));

        return builder;
    }
    
    private static Option<HttpConfiguration.ProxyHost> getRealProxyHost(HttpConfiguration config, Option<URI> baseUri)
    {
        for (ProxyConfiguration proxy: config.getProxyConfiguration())
        {
            if (proxy.getProxyHost().isDefined())
            {
                return proxy.getProxyHost();
            }
            String prefix = "https";
            for (URI u: baseUri)
            {
                if (u.getScheme() != null && u.getScheme().equalsIgnoreCase("http"))
                {
                    prefix = "http";
                }
            }
            for (String host: option(trimToNull(getProperty(prefix + ".proxyHost"))))
            {
                int port = Integer.parseInt(getProperty(prefix + ".proxyPort", String.valueOf(HttpConfiguration.ProxyHost.DEFAULT_PORT)));
                return some(new HttpConfiguration.ProxyHost(host, port));
            }
        }
        return none();
    }
    
    private static ImmutableList<String> getProxyPreferredAuthSchemes(HttpConfiguration config)
    {
        for (ProxyConfiguration proxy: config.getProxyConfiguration())
        {
            for (HttpConfiguration.ProxyAuthParams auth: proxy.getAuthParams())
            {
                return ImmutableList.of(auth.getAuthMethod().name().toUpperCase());
            }
        }
        return ImmutableList.of();
    }
    
    private class OperationsImpl implements HttpTransport
    {
        private final Iterable<RequestDecorator> decorators;
        
        OperationsImpl(Iterable<RequestDecorator> decorators)
        {
            this.decorators = ImmutableList.copyOf(decorators);
        }

        @Override
        public HttpTransport withRequestDecorator(RequestDecorator decorator)
        {
            return new OperationsImpl(concat(decorators, ImmutableList.of(decorator)));
        }
        
        @Override
        public SimpleHttpResponse get(URI uri) throws MpacException
        {
            HttpGet method = new HttpGet(uri);
            return executeMethod(method);
        }

        @Override
        public SimpleHttpResponse postParams(URI uri, Multimap<String, String> params) throws MpacException
        {
            HttpPost method = new HttpPost(uri);
            List<NameValuePair> formParams = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> param: params.entries())
            {
                formParams.add(new BasicNameValuePair(param.getKey(), param.getValue()));
            }
            try
            {
                method.setEntity(new UrlEncodedFormEntity(formParams));
            }
            catch (UnsupportedEncodingException e)
            {
                throw new MpacException(e);
            }
            return executeMethod(method);
        }

        @Override
        public SimpleHttpResponse post(URI uri, InputStream content, long length, String contentType, String acceptContentType) throws MpacException
        {
            HttpPost method = new HttpPost(uri);
            method.setEntity(new InputStreamEntity(content, length, ContentType.create(contentType)));
            method.addHeader(CONTENT_TYPE, contentType);
            method.addHeader("Accept", acceptContentType);
            return executeMethod(method);
        }

        @Override
        public SimpleHttpResponse put(URI uri, byte[] content) throws MpacException
        {
            HttpPut method = new HttpPut(uri);
            method.setEntity(new ByteArrayEntity(content, APPLICATION_JSON));
            return executeMethod(method);
        }

        @Override
        public SimpleHttpResponse patch(URI uri, byte[] content) throws MpacException
        {
            HttpPatch method = new HttpPatch(uri);
            method.setEntity(new ByteArrayEntity(content, APPLICATION_JSON_PATCH));
            return executeMethod(method);
        }
        
        @Override
        public SimpleHttpResponse delete(URI uri) throws MpacException
        {
            HttpDelete method = new HttpDelete(uri);
            return executeMethod(method);
        }
        
        @Override
        public void close()
        {
            CommonsHttpTransport.this.close();
        }
        
        private SimpleHttpResponse executeMethod(HttpUriRequest method) throws MpacException
        {
            logger.info(method.getMethod() + " " + method.getURI());
            for (RequestDecorator rd: decorators)
            {
                Map<String, String> moreHeaders = rd.getRequestHeaders();
                if (moreHeaders != null)
                {
                    for (Map.Entry<String, String> header: moreHeaders.entrySet())
                    {
                        method.addHeader(header.getKey(), header.getValue());
                    }                    
                }
            }
            try
            {
                return new ResponseImpl(client.execute(method));
            }
            catch (SocketException e)
            {
                throw new MpacException.ConnectionFailure(e);
            }
            catch (IOException e)
            {
                throw new MpacException(e);
            }
        }
    }
    
    private static class ResponseImpl implements SimpleHttpResponse
    {
        private final HttpResponse response;
        
        ResponseImpl(HttpResponse response)
        {
            this.response = response;
        }
        
        public int getStatus()
        {
            return response.getStatusLine().getStatusCode();
        }
        
        public Iterable<String> getHeader(String name)
        {
            ImmutableList.Builder<String> ret = ImmutableList.builder();
            for (Header h: response.getHeaders(name))
            {
                ret.add(h.getValue());
            }
            return ret.build();
        }
        
        public InputStream getContentStream() throws MpacException
        {
            try
            {
                return response.getEntity().getContent();
            }
            catch (IOException e)
            {
                throw new MpacException(e);
            }
        }
        
        public boolean isEmpty()
        {
            Header h = response.getFirstHeader("Content-Length");
            return (h != null) && (h.getValue().trim().equals("0"));
        }
        
        public void close()
        {
            if (response.getEntity() != null)
            {
                consumeQuietly(response.getEntity());
            }
        }
    }
    
    private static class DefaultRequestInterceptor implements HttpRequestInterceptor
    {
        private final HttpConfiguration config;
        private final Option<HttpConfiguration.ProxyHost> proxyHost;
        
        DefaultRequestInterceptor(HttpConfiguration config, Option<HttpConfiguration.ProxyHost> proxyHost)
        {
            this.config = checkNotNull(config);
            this.proxyHost = checkNotNull(proxyHost);
        }
        
        @Override
        public void process(HttpRequest request, HttpContext context)
        {
            AuthCache authCache = null;
         
            for (RequestDecorator rd: config.getRequestDecorator())
            {
                Map<String, String> headers = rd.getRequestHeaders();
                if (headers != null)
                {
                    for (Map.Entry<String, String> header: headers.entrySet())
                    {
                        request.addHeader(header.getKey(), header.getValue());
                    }
                }
            }
            
            if (config.hasCredentials())
            {
                // enable preemptive authentication for this request
                if (authCache == null)
                {
                    authCache = new BasicAuthCache();
                }
                HttpHost targetHost = (HttpHost) context.getAttribute(HTTP_TARGET_HOST);
                if (targetHost != null)
                {
                    authCache.put(targetHost, new BasicScheme());
                }
            }
            
            for (ProxyConfiguration proxy: config.getProxyConfiguration())
            {
                for (HttpConfiguration.ProxyAuthParams auth: proxy.getAuthParams())
                {
                    // If using basicauth for the proxy, send credentials preemptively to avoid a challenge
                    if (auth.getAuthMethod() == ProxyAuthMethod.BASIC)
                    {
                        for (HttpConfiguration.ProxyHost ph: proxyHost)
                        {
                            // The following method of causing preemptive proxy authentication is better than simply
                            // setting the Proxy-Authorization header on the request, because it will (or should)
                            // work even for a tunneled request.  See: http://stackoverflow.com/questions/21121121/preemptive-proxy-authentication-with-http-tunnel-https-connection-in-apache-ht
                            HttpHost hph = new HttpHost(ph.getHostname(), ph.getPort());
                            if (authCache == null)
                            {
                                authCache = new BasicAuthCache();
                            }
                            BasicScheme proxyAuth = new BasicScheme();
                            try
                            {
                                proxyAuth.processChallenge(new BasicHeader(AUTH.PROXY_AUTH, "BASIC realm=default"));
                                authCache.put(hph, proxyAuth);
                            }
                            catch (Exception e)
                            {
                                logger.warn("Error, unable to set preemptive proxy auth: " + e);
                                logger.debug(e.toString(), e);
                            }
                        }
                    }
                }
            }
            
            if (authCache != null)
            {
                context.setAttribute(AUTH_CACHE, authCache);
            }
        }
    }
    
    private static class DefaultCredentialsProvider implements CredentialsProvider
    {
        private final Option<HttpConfiguration.ProxyHost> proxyHost;
        private final Option<Credentials> proxyCredentials;
        private final Option<Credentials> targetHostCredentials;
        
        DefaultCredentialsProvider(HttpConfiguration config, Option<HttpConfiguration.ProxyHost> proxyHost)
        {
            this.proxyCredentials = makeProxyCredentials(config);
            this.targetHostCredentials = makeTargetHostCredentials(config);
            this.proxyHost = checkNotNull(proxyHost);
        }
        
        @Override
        public void setCredentials(AuthScope authscope, Credentials credentials)
        {
            // unused, this object is immutable
        }

        @Override
        public Credentials getCredentials(AuthScope authScope)
        {
            for (HttpConfiguration.ProxyHost ph: proxyHost)
            {
                if (ph.getHostname().equals(authScope.getHost()) && ph.getPort() == authScope.getPort())
                {
                    return proxyCredentials.getOrElse((Credentials) null);
                }
            }
            
            return targetHostCredentials.getOrElse((Credentials) null);
        }

        @Override
        public void clear()
        {
        }
        
        private Option<Credentials> makeProxyCredentials(HttpConfiguration config)
        {
            for (HttpConfiguration.ProxyConfiguration proxy: config.getProxyConfiguration())
            {
                for (HttpConfiguration.ProxyAuthParams auth: proxy.getAuthParams())
                {
                    Credentials c;
                    switch (auth.getAuthMethod())
                    {
                        case NTLM:
                            c = new NTCredentials(auth.getCredentials().getUsername(),
                                                  auth.getCredentials().getPassword(),
                                                  auth.getNtlmWorkstation().getOrElse(""),
                                                  auth.getNtlmDomain().getOrElse(""));
    
                        default:
                            c = new UsernamePasswordCredentials(auth.getCredentials().getUsername(),
                                        auth.getCredentials().getPassword());
                    }
                    return some(c);
                }
            }
            return none();
        }
        
        private Option<Credentials> makeTargetHostCredentials(HttpConfiguration config)
        {
            for (HttpConfiguration.Credentials c: config.getCredentials())
            {
                return Option.<Credentials>some(new UsernamePasswordCredentials(c.getUsername(), c.getPassword()));
            }
            return none();
        }
    }
}
