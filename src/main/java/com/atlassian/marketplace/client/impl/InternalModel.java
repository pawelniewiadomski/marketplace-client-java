package com.atlassian.marketplace.client.impl;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.Licenses;
import com.atlassian.marketplace.client.model.*;

import com.google.common.collect.ImmutableList;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Model classes that are only used internally by the client implementation.
 * @since 2.0.0
 */
public abstract class InternalModel
{
    public static Addons addons(Links links, ImmutableList<AddonSummary> items, int count)
    {
        return makeCollectionRep(Addons.class, links, items, some(count));
    }

    public static AddonCategories addonCategories(Links links, ImmutableList<AddonCategorySummary> items)
    {
        return makeCollectionRep(AddonCategories.class, links, items, none(Integer.class));
    }
    
    public static AddonReferences addonReferences(Links links, ImmutableList<AddonReference> items, int count)
    {
        return makeCollectionRep(AddonReferences.class, links, items, some(count));
    }
    
    public static AddonVersions addonVersions(Links links, ImmutableList<AddonVersionSummary> items, int count)
    {
        return makeCollectionRep(AddonVersions.class, links, items, some(count));
    }
    
    public static Applications applications(Links links, ImmutableList<Application> items)
    {
        return makeCollectionRep(Applications.class, links, items, none(Integer.class));
    }

    public static ApplicationVersions applicationVersions(Links links, ImmutableList<ApplicationVersion> items, int count)
    {
        return makeCollectionRep(ApplicationVersions.class, links, items, some(count));
    }

    public static ErrorDetails errorDetails(Iterable<ErrorDetail> errorDetails)
    {
        return new ErrorDetails(errorDetails);
    }
    
    public static MinimalLinks minimalLinks(Links links)
    {
        return new MinimalLinks(links);
    }
    
    public static Products products(Links links, ImmutableList<Product> items, int count)
    {
        return makeCollectionRep(Products.class, links, items, some(count));
    }

    public static Vendors vendors(Links links, ImmutableList<VendorSummary> items, int count)
    {
        return makeCollectionRep(Vendors.class, links, items, some(count));
    }

    public static Licenses licenses(Links links, ImmutableList<License> items, int count)
    {
        return makeCollectionRep(Licenses.class, links, items, some(count));
    }
    
    /**
     * Common properties for resource collections.
     */
    public static abstract class EntityCollection<T> implements Entity
    {
        private Links _links;
        private Option<Integer> count;
        @RequiredLink(rel = "self") private URI selfUri;
        
        protected EntityCollection()
        {
        }
        
        protected EntityCollection(Links links, int count)
        {
            this._links = links;
            this.count = some(count);
        }
        
        /**
         * The top-level links for the resource.
         */
        public Links getLinks()
        {
            return _links;
        }
        
        public URI getSelfUri()
        {
            return selfUri;
        }
        
        /**
         * The total number of entities in the collection, which may be more than the subset in this representation.
         */
        public int getCount()
        {
            return count.getOrElse(0);
        }
        
        /**
         * A list of entities, either the full result set or a subset (page) of a larger set.
         */
        public abstract Iterable<T> getItems();
    }

    public static class AddonCategories
    {
        public Links _links;
        public Embedded _embedded;
        
        public static class Embedded
        {
            public ImmutableList<AddonCategorySummary> categories;
        }
    }
    
    public static class AddonReferences extends EntityCollection<AddonReference>
    {
        private Embedded _embedded;
    
        public Iterable<AddonReference> getItems()
        {
            return _embedded.addons;
        }
        
        public static class Embedded
        {
            private ImmutableList<AddonReference> addons;
        }
    }

    public static class AddonVersions extends EntityCollection<AddonVersionSummary>
    {
        private Embedded _embedded;
    
        public Iterable<AddonVersionSummary> getItems()
        {
            return _embedded.versions;
        }
        
        public static class Embedded
        {
            private ImmutableList<AddonVersionSummary> versions;
        }
    }
    
    public static class Addons extends EntityCollection<AddonSummary>
    {
        private Embedded _embedded;
     
        public Iterable<AddonSummary> getItems()
        {
            return _embedded.addons;
        }
        
        public static class Embedded
        {
            private ImmutableList<AddonSummary> addons;
        }
    }
    
    public static class ApplicationVersions extends EntityCollection<ApplicationVersion>
    {
        private Embedded _embedded;
        
        public Iterable<ApplicationVersion> getItems()
        {
            return _embedded.versions;
        }
        
        public static class Embedded
        {
            private Collection<ApplicationVersion> versions;
        }
    }
    
    public static class Applications
    {
        public Links _links;
        public Embedded _embedded;

        public static class Embedded
        {
            public ImmutableList<Application> applications;
        }
    }

    public static class ErrorDetails
    {
        ImmutableList<ErrorDetail> errors;
        
        ErrorDetails(Iterable<ErrorDetail> errorDetails)
        {
            this.errors = ImmutableList.copyOf(errorDetails);
        }
    }
    
    public static class LicenseTypes
    {
        private Embedded _embedded;
    
        public Iterable<LicenseType> getItems()
        {
            return _embedded.types;
        }
        
        public static class Embedded
        {
            public ImmutableList<LicenseType> types;
        }
    }

    public static class MinimalLinks
    {
        private Links _links;

        public MinimalLinks(Links _links)
        {
            this._links = checkNotNull(_links);
        }
        
        public Links getLinks()
        {
            return _links;
        }
    }

    public static class Products extends EntityCollection<Product>
    {
        private Embedded _embedded;
        
        public Iterable<Product> getItems()
        {
            return _embedded.products;
        }
        
        public static class Embedded
        {
            private ImmutableList<Product> products;
        }
    }

    public static class Vendors extends EntityCollection<VendorSummary>
    {
        private Embedded _embedded;
    
        public Iterable<VendorSummary> getItems()
        {
            return _embedded.vendors;
        }
        
        public static class Embedded
        {
            private ImmutableList<VendorSummary> vendors;
        }
    }

    public static class Licenses extends EntityCollection<License>
    {
        private ImmutableList<License> licenses;

        public Iterable<License> getItems()
        {
            return licenses;
        }
    }

    // Super hacky reflective method for creating test instances of collection representations
    private static <A> A makeCollectionRep(Class<A> repClass, Links links, ImmutableList<?> items, Option<Integer> count)
    {
        Map<String, Field> fields = EntityValidator.getClassFields(repClass);
        try
        {
            A instance = (A) repClass.getConstructor().newInstance();

            fields.get("_links").set(instance, links);
            if (count.isDefined()) {
                fields.get("count").set(instance, count);
            }

            Field f = fields.get("_embedded");
            Class<?> ec = f.getType(); 
            Object e = ec.getConstructor().newInstance();
            Field eif = ec.getDeclaredFields()[0];
            eif.setAccessible(true);
            eif.set(e, items);
            f.set(instance, e);
            
            return instance;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
