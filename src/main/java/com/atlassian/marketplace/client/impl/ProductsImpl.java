package com.atlassian.marketplace.client.impl;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.ProductQuery;
import com.atlassian.marketplace.client.api.ProductVersionSpecifier;
import com.atlassian.marketplace.client.api.Products;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.model.Product;
import com.atlassian.marketplace.client.model.ProductVersion;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableMap;

final class ProductsImpl extends ApiImplBase implements Products
{
    ProductsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "products");
    }

    @Override
    public Option<Product> getByKey(String productKey, ProductQuery query) throws MpacException
    {
        InternalModel.Products collectionRep = getEmptyBaseCollectionRep();
        UriTemplate byKeyTemplate = ApiHelper.requireLinkUriTemplate(collectionRep.getLinks(), "byKey", InternalModel.Products.class);
        UriBuilder uri = UriBuilder.fromUri(byKeyTemplate.resolve(ImmutableMap.of("productKey", productKey)));
        ApiHelper.addProductQueryParams(query, uri);
        return apiHelper.getOptionalEntity(uri.build(), Product.class);
    }

    @Override
    public Option<ProductVersion> getVersion(String productKey, ProductVersionSpecifier versionQuery) throws MpacException
    {
        InternalModel.Products collectionRep = getEmptyBaseCollectionRep();
        UriTemplate template = ApiHelper.requireLinkUriTemplate(collectionRep.getLinks(),
                getVersionLinkTemplateRel(versionQuery), InternalModel.Products.class);
        ImmutableMap.Builder<String, String> params = ImmutableMap.builder();
        params.put("productKey", productKey);
        for (Integer b: versionQuery.getBuildNumber())
        {
            params.put("buildNumber", String.valueOf(b));
        }
        for (String n: versionQuery.getName())
        {
            params.put("versionName", n);
        }
        UriBuilder uri = UriBuilder.fromUri(template.resolve(params.build()));
        return apiHelper.getOptionalEntity(uri.build(), ProductVersion.class);
    }
    
    private String getVersionLinkTemplateRel(ProductVersionSpecifier versionQuery)
    {
        return versionQuery.getBuildNumber().isDefined() ? "versionByBuild" :
            (versionQuery.getName().isDefined() ? "versionByName" : "latestVersion");
    }
    
    @Override
    public Page<Product> find(ProductQuery query) throws MpacException
    {
        final UriBuilder uri = fromApiRoot();
        ApiHelper.addProductQueryParams(query, uri);
        return apiHelper.getMore(new PageReference<Product>(
            uri.build(), query.getBounds(), pageReader(InternalModel.Products.class)));
    }

    private InternalModel.Products getEmptyBaseCollectionRep() throws MpacException
    {
        UriBuilder uri = fromApiRoot();
        ApiHelper.addBoundsParams(ProductQuery.builder().bounds(QueryBounds.empty()).build(), uri);
        return apiHelper.getEntity(uri.build(), InternalModel.Products.class);
    }
}
