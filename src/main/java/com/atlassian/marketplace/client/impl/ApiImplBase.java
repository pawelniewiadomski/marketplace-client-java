package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.http.SimpleHttpResponse;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.TestModelBuilders;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.base.Function;
import com.google.common.base.Functions;

import static com.atlassian.marketplace.client.impl.ApiHelper.JSON;
import static com.atlassian.marketplace.client.impl.ApiHelper.closeQuietly;

abstract class ApiImplBase
{
    protected final ApiHelper apiHelper;
    protected final URI apiRoot;
    
    protected ApiImplBase(ApiHelper apiHelper, InternalModel.MinimalLinks root, String rootLinkRel) throws MpacException
    {
        this(apiHelper, apiHelper.requireLinkUri(root.getLinks(), rootLinkRel, root.getClass()));
    }

    protected ApiImplBase(ApiHelper apiHelper, URI apiRoot) throws MpacException
    {
        this.apiHelper = apiHelper;
        this.apiRoot = apiHelper.resolveLink(apiRoot);
    }
    
    protected URI getApiRoot()
    {
        return apiRoot;
    }
    
    protected UriBuilder fromApiRoot()
    {
        return UriBuilder.fromUri(apiRoot);
    }
    
    protected Links getLinksOnly(URI uri) throws MpacException
    {
        InternalModel.MinimalLinks rep = apiHelper.getEntity(uri, InternalModel.MinimalLinks.class);
        return rep.getLinks();
    }

    protected <T> T genericCreate(URI collectionUri, T entity) throws MpacException
    {
        return genericCreate(collectionUri, entity, Functions.<URI>identity());
    }
    
    protected <T> T genericCreate(URI collectionUri, T entity, Function<URI, URI> resultUriTransform) throws MpacException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        apiHelper.getEncoding().encode(bos, entity, false);
        byte[] bytes = bos.toByteArray();
        return createdOrUpdatedEntityResult(apiHelper.getHttp().post(apiHelper.resolveLink(collectionUri),
            new ByteArrayInputStream(bytes), bytes.length, JSON, JSON),
            entity.getClass(),
            resultUriTransform);
    }

    protected <T> T genericUpdate(URI uri, T original, T updated) throws MpacException
    {
        if (uri == TestModelBuilders.DEFAULT_URI)
        {
            // If the entity's self URI is DEFAULT_URI, then it's not a real, updatable entity that was obtained
            // from the server - it's something the caller built.  In that case there's no point in trying to
            // send a PATCH request to this fake URI; return a more informative error instead.
            throw new MpacException.CannotUpdateNonServerSideEntity();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        apiHelper.getEncoding().encodeChanges(bos, original, updated);
        byte[] bytes = bos.toByteArray();
        return createdOrUpdatedEntityResult(apiHelper.getHttp().patch(apiHelper.resolveLink(uri), bytes),
            original.getClass(), Functions.<URI>identity());
    }
    
    @SuppressWarnings("unchecked")
    private <T> T createdOrUpdatedEntityResult(SimpleHttpResponse response, Class<?> entityClass,
        Function<URI, URI> resultUriTransform) throws MpacException
    {
        try
        {
            int status = response.getStatus();
            if (status == 200 || status == 201 || status == 204)
            {
                for (String location: response.getHeader("Location"))
                {
                    return apiHelper.getEntityUncached(resultUriTransform.apply(URI.create(location)), (Class<T>) entityClass);
                }
                throw new MpacException("Server did not return expected Location header");
            }
            throw apiHelper.responseException(response);
        }
        finally
        {
            closeQuietly(response);
        }
    }

    protected <T, U extends InternalModel.EntityCollection<T>> PageReader<T> pageReader(final Class<U> collectionRepClass)
    {
        return new PageReader<T>()
        {
            @Override
            public Page<T> readPage(PageReference<T> ref, InputStream in) throws MpacException
            {
                U rep = apiHelper.decode(in, collectionRepClass);
                return new PageImpl<T>(ref, rep.getLinks(), rep.getItems(), rep.getCount(), this);
            }
        };
    }
}
