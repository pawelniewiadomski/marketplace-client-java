package com.atlassian.marketplace.client.impl;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.LicenseTypes;
import com.atlassian.marketplace.client.model.LicenseType;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

final class LicenseTypesImpl implements LicenseTypes
{
    private final ApiHelper apiHelper;
    private final InternalModel.MinimalLinks root;

    LicenseTypesImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root)
    {
        this.apiHelper = apiHelper;
        this.root = root;
    }

    @Override
    public Iterable<LicenseType> getAllLicenseTypes() throws MpacException
    {
        UriBuilder uri = licenseTypesBaseUri();
        InternalModel.LicenseTypes collectionRep =
            apiHelper.getEntity(uri.build(), InternalModel.LicenseTypes.class);
        return ImmutableList.copyOf(collectionRep.getItems());
    }
    
    @Override
    public Option<LicenseType> getByKey(final String licenseTypeKey) throws MpacException
    {
        return Iterables.findFirst(getAllLicenseTypes(), new Predicate<LicenseType>()
            {
                public boolean apply(LicenseType l)
                {
                    return l.getKey().equals(licenseTypeKey);
                }
            });
    }
    
    private UriBuilder licenseTypesBaseUri() throws MpacException
    {
        return UriBuilder.fromUri(apiHelper.requireLinkUri(root.getLinks(), "licenseTypes", root.getClass()));
    }
}
