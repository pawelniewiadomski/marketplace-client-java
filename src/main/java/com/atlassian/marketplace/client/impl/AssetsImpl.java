package com.atlassian.marketplace.client.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.api.Assets;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.ImagePurpose;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableMap;

final class AssetsImpl extends ApiImplBase implements Assets
{
    AssetsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "assets");
    }

    @Override
    public ArtifactId uploadAddonArtifact(File artifactFile) throws MpacException
    {
        URI uri = apiHelper.requireLinkUri(getLinksOnly(getApiRoot()), "artifact", Assets.class);
        return ArtifactId.fromUri(uploadAssetFromFile(uri, artifactFile));
    }

    @Override
    public ImageId uploadImage(File imageFile, ImagePurpose imageType) throws MpacException
    {
        UriTemplate ut = ApiHelper.requireLinkUriTemplate(getLinksOnly(getApiRoot()), "imageByType", Assets.class);
        URI uri = apiHelper.resolveLink(ut.resolve(ImmutableMap.of("imageType", imageType.getKey())));
        return ImageId.fromUri(uploadAssetFromFile(uri, imageFile));
    }
    
    private URI uploadAssetFromFile(URI collectionUri, File file) throws MpacException
    {
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                return uploadAssetFromStream(collectionUri, fis, file.length(), file.getName());
            }
            finally
            {
                fis.close();
            }
        }
        catch (IOException e)
        {
            throw new MpacException(e);
        }
    }
    
    private URI uploadAssetFromStream(URI collectionUri, InputStream stream, long length, String logicalFileName) throws MpacException
    {
        URI uri = UriBuilder.fromUri(collectionUri).queryParam("file", logicalFileName).build();
        InternalModel.MinimalLinks result =
            apiHelper.postContent(uri, stream, length, "application/binary", InternalModel.MinimalLinks.class);
        return ApiHelper.requireLink(result.getLinks(), "self", Links.class).getUri();
    }
}
