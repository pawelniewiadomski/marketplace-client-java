package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReader;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.model.Links;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

final class PageImpl<T> extends Page<T>
{
    public static final String NEXT_REL = "next";
    public static final String PREVIOUS_REL = "prev";
    
    private final PageReference<T> reference;
    private final Option<URI> previousUri;
    private final Option<URI> nextUri;
    
    PageImpl(PageReference<T> reference, Links links, Iterable<T> items, int count, PageReader<T> reader)
    {
        super(items, count, reader);
        this.reference = checkNotNull(reference, "reference");
        checkNotNull(links, "links");
        this.previousUri = links.getUri(PREVIOUS_REL);
        this.nextUri = links.getUri(NEXT_REL);
    }

    @Override
    public Option<PageReference<T>> getReference()
    {
        return some(reference);
    }
    
    @Override
    public Option<PageReference<T>> getPrevious()
    {
        for (URI p: previousUri)
        {
            return some(new PageReference<T>(p,
                QueryBounds.offset(getBounds().getOffset() - getBounds().getLimit().getOrElse(size()))
                    .withLimit(getBounds().getLimit().orElse(some(size()))), reader));
        }
        return none();
    }

    @Override
    public Option<PageReference<T>> getNext()
    {
        for (URI n: nextUri)
        {
            return some(new PageReference<T>(n,
                QueryBounds.offset(getBounds().getOffset() + getBounds().getLimit().getOrElse(size()))
                    .withLimit(getBounds().getLimit().orElse(some(size()))), reader));
        }
        return none();
    }
    
    private QueryBounds getBounds()
    {
        return reference.getBounds();
    }
}
