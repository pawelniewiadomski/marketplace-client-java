package com.atlassian.marketplace.client.impl;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.List;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.EnumWithKey;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.encoding.InvalidFieldValue;
import com.atlassian.marketplace.client.model.HtmlString;
import com.atlassian.marketplace.client.model.Link;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.model.ReadOnly;
import com.atlassian.marketplace.client.model.RequiredLink;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.ConstructorConstructor;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.bind.ReflectiveTypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.marketplace.client.encoding.DateFormats.DATE_FORMAT;
import static com.atlassian.marketplace.client.encoding.DateFormats.DATE_TIME_FORMAT;
import static com.atlassian.marketplace.client.impl.EntityValidator.validateInstance;
import static com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner;

/**
 * Manages all types for which we have custom JSON (de)serialization.
 * @since 2.0.0
 */
abstract class TypeAdapters
{
    private static TypeAdapterFactory factoryWithReadOnlyFields = makeTypeAdapterFactory(true);
    private static TypeAdapterFactory factoryWithoutReadOnlyFields = makeTypeAdapterFactory(false);
    
    private TypeAdapters()
    {
    }
    
    public static Map<Class<?>, Object> all()
    {
        return ADAPTERS;
    }
    
    @SuppressWarnings("unchecked")
    static <A extends EnumWithKey> TypeAdapter<A> enumTypeAdapter(final Class<?> enumClass)
    {
        final EnumWithKey.Parser<A> parser = EnumWithKey.Parser.forType((Class<A>) enumClass);

        return new TypeAdapter<A>()
        {
            @Override
            public void write(JsonWriter out, A value) throws IOException
            {
                out.value(value.getKey());
            }
        
            @Override
            public A read(JsonReader in) throws IOException
            {
                String s = in.nextString();
                for (A v: parser.valueForKey(s))
                {
                    return v;
                }
                throw new SchemaViolationException(new InvalidFieldValue(s, enumClass));
            }
        };
    }
    
    static <A> TypeAdapter<A> objectTypeAdapter(Gson gson, com.google.gson.reflect.TypeToken<A> typeToken,
        boolean includeReadOnlyFields)
    {
        TypeAdapterFactory factory = includeReadOnlyFields ? factoryWithReadOnlyFields : factoryWithoutReadOnlyFields;
        final TypeAdapter<A> baseAdapter = factory.create(gson, typeToken);
        
        return new TypeAdapter<A>()
        {
            @Override
            public void write(JsonWriter out, A value) throws IOException
            {
                baseAdapter.write(out, value);
            }
        
            @Override
            public A read(JsonReader in) throws IOException
            {
                try
                {
                    return validateInstance(baseAdapter.read(in));
                }
                catch (SchemaViolationException e)
                {
                    throw new JsonParseException(e);
                }
            }
        };
    }
    
    private interface JsonSerDeser<A> extends JsonSerializer<A>, JsonDeserializer<A>
    {
    }
    
    private static <A> JsonSerDeser<A> stringLikeTypeAdapter(final Function<A, String> writer, final Function<String, A> reader)
    {
        return new JsonSerDeser<A>()
        {
            @Override
            public JsonElement serialize(A value, Type type, JsonSerializationContext context)
            {
                return new JsonPrimitive(writer.apply(value));
            }

            @Override
            public A deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException
            {
                try
                {
                    return reader.apply(json.getAsString());
                }
                catch (IllegalArgumentException e)
                {
                    String s = json.isJsonPrimitive() ? json.getAsString() : json.toString();
                    throw new SchemaViolationException(new InvalidFieldValue(s, (Class<?>) type));
                }
            }
        }; 
    }

    private static final Function<ApplicationKey, String> appKeyToString = new Function<ApplicationKey, String>()
    {
        public String apply(ApplicationKey value)
        {
            return value.getKey();
        }
    };

    private static final Function<String, ApplicationKey> stringToAppKey = new Function<String, ApplicationKey>()
    {
        public ApplicationKey apply(String value)
        {
            return ApplicationKey.valueOf(value);
        }
    };

    private static final Function<DateTime, String> dateTimeToString = new Function<DateTime, String>()
    {
        public String apply(DateTime value)
        {
            return DATE_TIME_FORMAT.print(value);
        }
    };

    private static final Function<String, DateTime> stringToDateTime = new Function<String, DateTime>()
    {
        public DateTime apply(String value)
        {
            return DATE_TIME_FORMAT.parseDateTime(value);
        }
    };

    private static final Function<HtmlString, String> htmlStringToString = new Function<HtmlString, String>()
    {
        public String apply(HtmlString value)
        {
            return value.getHtml();
        }
    };

    private static final Function<String, HtmlString> stringToHtmlString = new Function<String, HtmlString>()
    {
        public HtmlString apply(String value)
        {
            return HtmlString.html(value);
        }
    };

    private static final Function<LocalDate, String> localDateToString = new Function<LocalDate, String>()
    {
        public String apply(LocalDate value)
        {
            return DATE_FORMAT.print(value);
        }
    };

    private static final Function<String, LocalDate> stringToLocalDate = new Function<String, LocalDate>()
    {
        public LocalDate apply(String value)
        {
            return DATE_FORMAT.parseDateTime(value).toLocalDate();
        }
    };

    private static final Function<URI, String> uriToString = new Function<URI, String>()
    {
        public String apply(URI value)
        {
            return value.toASCIIString();
        }
    };

    private static final Function<String, URI> stringToUri = new Function<String, URI>()
    {
        public URI apply(String value)
        {
            return URI.create(value);
        }
    };

    private static final Map<Class<?>, Object> ADAPTERS = ImmutableMap.<Class<?>, Object>builder()
        .put(ApplicationKey.class, stringLikeTypeAdapter(appKeyToString, stringToAppKey))
        .put(DateTime.class, stringLikeTypeAdapter(dateTimeToString, stringToDateTime))
        .put(HtmlString.class, stringLikeTypeAdapter(htmlStringToString, stringToHtmlString))
        .put(LocalDate.class, stringLikeTypeAdapter(localDateToString, stringToLocalDate))
        .put(URI.class, stringLikeTypeAdapter(uriToString, stringToUri))
        .put(ImmutableList.class, new ListTypeAdapter())
        .put(ImmutableMap.class, new MapTypeAdapter())
        .put(Option.class, new OptionTypeAdapter())
        .put(Link.class, new LinkTypeAdapter())
        .put(Links.class, new LinksTypeAdapter())
        .build();

    private static TypeAdapterFactory makeTypeAdapterFactory(boolean includeReadOnlyFields)
    {
        return new ReflectiveTypeAdapterFactory(
            new ConstructorConstructor(ImmutableMap.<Type, InstanceCreator<?>>of()),
            FieldNamingPolicy.IDENTITY,
            new Excluder().withExclusionStrategy(new CustomExclusionStrategy(includeReadOnlyFields), true, true)
        );
    }
    
    private static class CustomExclusionStrategy implements ExclusionStrategy
    {
        private final boolean includeReadOnlyFields;
        
        CustomExclusionStrategy(boolean includeReadOnlyFields)
        {
            this.includeReadOnlyFields = includeReadOnlyFields;
        }
        
        @Override
        public boolean shouldSkipField(FieldAttributes f)
        {
            return (f.getAnnotation(RequiredLink.class) != null) ||
                (!includeReadOnlyFields && (f.getAnnotation(ReadOnly.class) != null));
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz)
        {
            return false;
        }
    }
    
    private static class LinkTypeAdapter implements JsonDeserializer<Link>, JsonSerializer<Link>
    {
        // The HAL-JSON schema supports two kinds of links, a regular URI or a URI template, depending on
        // the "templated" property.
        
        @Override
        public JsonElement serialize(Link link, Type type, JsonSerializationContext context)
        {
            LinkInternal li = new LinkInternal();
            li.href = link.getTemplateOrUri().fold(
                new Function<UriTemplate, String>()
                {
                    public String apply(UriTemplate u)
                    {
                        return u.getValue();
                    }
                },
                new Function<URI, String>()
                {
                    public String apply(URI u)
                    {
                        return u.toASCIIString();
                    }
                }
            );
            li.type = link.getType();
            li.templated = link.getUriTemplate().isDefined() ? some(true) : none(Boolean.class);
            return context.serialize(li);
        }

        @Override
        public Link deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException
        {
            LinkInternal li = (LinkInternal) context.deserialize(json, LinkInternal.class);
            if (li.templated.getOrElse(false))
            {
                return Link.fromUriTemplate(UriTemplate.create(li.href), li.type);
            }
            return Link.fromUri(URI.create(li.href), li.type);
        }
            
        private static class LinkInternal
        {
            String href;
            Option<String> type;
            Option<Boolean> templated;
        }
    }
    
    private static class LinksTypeAdapter implements JsonDeserializer<Links>, JsonSerializer<Links>
    {
        private static final Type linkListType = (new TypeToken<List<Link>>() { }).getType();
        
        @Override
        public JsonElement serialize(Links links, Type type, JsonSerializationContext context)
        {
            JsonObject out = new JsonObject();
            for (Map.Entry<String, ImmutableList<Link>> e: links.getItems().entrySet())
            {
                JsonElement value;
                if (e.getValue().size() == 1)
                {
                    value = context.serialize(e.getValue().get(0));
                }
                else
                {
                    JsonArray a = new JsonArray();
                    for (Link l: e.getValue())
                    {
                        a.add(context.serialize(l));
                    }
                    value = a;
                }
                out.add(e.getKey(), value);
            }
            return out;
        }

        @SuppressWarnings("unchecked")
        @Override
        public Links deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException
        {
            ImmutableMap.Builder<String, ImmutableList<Link>> links = ImmutableMap.builder();
            JsonObject o = json.getAsJsonObject();
            for (Map.Entry<String, JsonElement> e: o.entrySet())
            {
                ImmutableList<Link> value;
                if (e.getValue().isJsonArray())
                {
                    value = ImmutableList.copyOf((List<Link>) context.deserialize(e.getValue(), linkListType));
                }
                else
                {
                    value = ImmutableList.of((Link) context.deserialize(e.getValue(), Link.class));
                }
                links.put(e.getKey(), value);
            }
            return new Links(links.build());
        }
    }
    
    private static class ListTypeAdapter implements JsonDeserializer<ImmutableList<?>>, JsonSerializer<ImmutableList<?>>
    {
        // Gson can already serialize a list, but we want to make sure that they're always deserialized as
        // immutable Guava lists.

        @Override
        public ImmutableList<?> deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException
        {
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            Type listType = makeListType(typeArguments[0]);
            return ImmutableList.copyOf((List<?>) context.deserialize(json, listType));
        }
    
        @Override
        public JsonElement serialize(ImmutableList<?> list, Type type, JsonSerializationContext context)
        {
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            Type listType = makeListType(typeArguments[0]);
            return context.serialize(list, listType);
        }
    
        private <T> Type makeListType(Type typeParam)
        {
            return newParameterizedTypeWithOwner(null, List.class, typeParam);
        }
    }

    private static class MapTypeAdapter implements JsonDeserializer<ImmutableMap<?, ?>>, JsonSerializer<ImmutableMap<?, ?>>
    {
        // Gson can already serialize a map, but we want to make sure that they're always deserialized as
        // immutable Guava maps.
        
        @Override
        public ImmutableMap<?, ?> deserialize(JsonElement json, Type type, JsonDeserializationContext context)
            throws JsonParseException
        {
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            Type mapType = makeMapType(typeArguments[0], typeArguments[1]);
            return ImmutableMap.copyOf((Map<?, ?>) context.deserialize(json, mapType));
        }
    
        @Override
        public JsonElement serialize(ImmutableMap<?, ?> map, Type type, JsonSerializationContext context)
        {
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            Type mapType = makeMapType(typeArguments[0], typeArguments[1]);
            return context.serialize(map, mapType);
        }
    
        private <A, B> Type makeMapType(Type keyTypeParam, Type valueTypeParam)
        {
            return newParameterizedTypeWithOwner(null, Map.class, keyTypeParam, valueTypeParam);
        }
    }

    private static class OptionTypeAdapter implements JsonDeserializer<Option<?>>, JsonSerializer<Option<?>>
    {
        @Override
        public JsonElement serialize(Option<?> o, Type type, JsonSerializationContext context)
        {
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            for (Object value: o)
            {
                return context.serialize(value, typeArguments[0]);
            }
            return JsonNull.INSTANCE;
        }

        @Override
        public Option<?> deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException
        {
            if (json.isJsonNull())
            {
                return none();
            }
            Type[] typeArguments = ((ParameterizedType) type).getActualTypeArguments();
            return some(context.deserialize(json, typeArguments[0]));
        }
    }
}
