package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonCategories;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.model.AddonCategorySummary;
import com.atlassian.marketplace.client.model.Application;

import com.google.common.collect.ImmutableList;

final class AddonCategoriesImpl implements AddonCategories
{
    private final ApiHelper apiHelper;
    private final Applications applicationsApi;

    AddonCategoriesImpl(ApiHelper apiHelper, Applications applicationsApi)
    {
        this.apiHelper = apiHelper;
        this.applicationsApi = applicationsApi;
        // Note that the per-application category list resource is accessed through a link from the application resource,
        // because we don't have link templates in the API for accessing it directly.
    }

    @Override
    public Iterable<AddonCategorySummary> findForApplication(ApplicationKey appKey) throws MpacException
    {
        for (Application a: applicationsApi.getByKey(appKey))
        {
            URI uri = apiHelper.requireLinkUri(a.getLinks(), "addonCategories", Application.class);
            InternalModel.AddonCategories listRep = apiHelper.getEntity(uri, InternalModel.AddonCategories.class);
            return ImmutableList.copyOf(listRep._embedded.categories);
        }
        return ImmutableList.of();
    }
}
