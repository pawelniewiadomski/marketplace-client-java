package com.atlassian.marketplace.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.*;
import com.atlassian.marketplace.client.http.HttpTransport;
import com.atlassian.marketplace.client.http.RequestDecorator;
import com.atlassian.marketplace.client.http.SimpleHttpResponse;
import com.atlassian.marketplace.client.model.Entity;
import com.atlassian.marketplace.client.model.Link;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDate;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Iterables.transform;

/**
 * Common methods used by implementations of the 2.0 API.
 */
class ApiHelper
{
    public static final String JSON = "application/json";
    
    private static final RequestDecorator NO_CACHE =
        RequestDecorator.Instances.forHeaders(ImmutableMap.of("Cache-Control", "no-cache"));
    
    private final URI baseUri;
    private final HttpTransport httpHelper;
    private final EntityEncoding encoding;
    
    public ApiHelper(URI baseUri, HttpTransport httpHelper, EntityEncoding encoding)
    {
        this.baseUri = baseUri;
        this.httpHelper = httpHelper;
        this.encoding = encoding;
    }

    public EntityEncoding getEncoding()
    {
        return encoding;
    }
    
    public HttpTransport getHttp()
    {
        return httpHelper;
    }

    public boolean checkReachable(URI resource)
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.get(resource);
            return !errorOrEmpty(response.getStatus());
        }
        catch (MpacException e)
        {
            return false;
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public static URI normalizeBaseUri(URI baseUri)
    {
        URI norm = baseUri.normalize();
        if (norm.getPath().endsWith("/"))
        {
            return norm;
        }
        return URI.create(norm.toString() + "/");
    }
    
    public <T> T getEntity(URI uri, Class<T> type) throws MpacException
    {
        return getEntityInternal(httpHelper, uri, type);
    }

    public <T> T getEntityUncached(URI uri, Class<T> type) throws MpacException
    {
        return getEntityInternal(httpHelper.withRequestDecorator(NO_CACHE), uri, type);
    }
    
    private <T> T getEntityInternal(HttpTransport h, URI uri, Class<T> type) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = h.get(resolveLink(uri));
            if (errorOrEmpty(response.getStatus()))
            {
                throw responseException(response);
            }
            return decode(response.getContentStream(), type);
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public <T> Option<T> getOptionalEntity(URI uri, Class<T> type) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.get(resolveLink(uri));
            if ((response.getStatus() == 204) || (response.getStatus() == 404))
            {
                return none();
            }
            if (error(response.getStatus()))
            {
                throw responseException(response);
            }
            if (response.isEmpty())
            {
                return none();
            }
            else
            {
                return some(decode(response.getContentStream(), type));
            }
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public void postParams(URI uri, Multimap<String, String> params) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.postParams(resolveLink(uri), params);
            if (error(response.getStatus()))
            {
                throw responseException(response);
            }
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public <T, U> T postEntity(URI uri, U entity, Class<T> type) throws MpacException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        encoding.encode(bos, entity, false);
        byte[] bytes = bos.toByteArray();
        return postContent(uri, new ByteArrayInputStream(bytes), bytes.length, JSON, type);
    }

    public <T> T postContent(URI uri, InputStream content, long length, String contentType, Class<T> returnType) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.post(resolveLink(uri), content, length, contentType, contentType);
            if (errorOrEmpty(response.getStatus()))
            {
                throw responseException(response);
            }
            return decode(response.getContentStream(), returnType);
        }
        finally
        {
            closeQuietly(response);
        }
    }
    
    public <T> void putEntity(URI uri, T entity) throws MpacException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        encoding.encode(bos, entity, false);
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.put(resolveLink(uri), bos.toByteArray());
            if (error(response.getStatus()))
            {
                throw responseException(response);
            }
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public <T, U> T putEntity(URI uri, U entity, Class<T> type) throws MpacException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        encoding.encode(bos, entity, false);
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.put(resolveLink(uri), bos.toByteArray());
            if (errorOrEmpty(response.getStatus()))
            {
                throw responseException(response);
            }
            return decode(response.getContentStream(), type);
        }
        finally
        {
            closeQuietly(response);
        }
    }

    public void deleteEntity(URI uri) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.delete(resolveLink(uri));
            if (error(response.getStatus()))
            {
                throw responseException(response);
            }
        }
        finally
        {
            closeQuietly(response);
        }
    }
    
    public URI resolveLink(URI href)
    {
        return href.isAbsolute() ? href : baseUri.resolve(href.toString());
    }

    public static Link requireLink(Links links, String rel, Class<?> entityClass) throws MpacException
    {
        for (Link l: links.getLink(rel))
        {
            return l;
        }
        throw new MpacException("Missing required API link \"" + rel + "\" from " + entityClass.getSimpleName());
    }

    public URI requireLinkUri(Links links, String rel, Class<?> entityClass) throws MpacException
    {
        return resolveLink(requireLink(links, rel, entityClass).getUri());
    }
    
    public <T> T decode(InputStream is, Class<T> type) throws MpacException
    {
        try
        {
            return encoding.decode(is, type);
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }
    }

    public boolean error(int statusCode)
    {
        return statusCode >= 400;
    }

    public boolean errorOrEmpty(int statusCode)
    {
        return statusCode >= 400 || statusCode == 204;
    }
    
    public MpacException responseException(SimpleHttpResponse response)
    {
        int status = response.getStatus();
        try
        {
            String body = IOUtils.toString(response.getContentStream());
            if (body.trim().startsWith("{"))
            {
                try
                {
                    InternalModel.ErrorDetails ed = encoding.decode(new ByteArrayInputStream(body.getBytes()),
                        InternalModel.ErrorDetails.class);
                    return new MpacException.ServerError(status, ed.errors);
                }
                catch (Exception e)
                {
                    // Can't parse it, we'll just return the body as a string. We don't want
                    // to throw any exceptions out of this method, since that would hide the
                    // server error that we're trying to return.
                }
            }
            return new MpacException.ServerError(status, body);
        }
        catch (Exception e)
        {
            // Error response body couldn't be read - ignore (see comment above).
            return new MpacException.ServerError(status);
        }
    }
    
    protected static void closeQuietly(SimpleHttpResponse response)
    {
        if (response != null)
        {
            response.close();
        }
    }
    
    private static void addOptionalBoolean(UriBuilder uri, String name, boolean value)
    {
        if (value)
        {
            uri.queryParam(name, true);
        }
    }


    private static void addOptionalString(UriBuilder uri, String name, Option<String> value) {
        for (String v: value)
        {
            uri.queryParam(name, v);
        }
    }


    private static <T extends EnumWithKey> void addOptionalEnumKey(UriBuilder uri, String name, Option<T> value)
    {
        for (T v: value)
        {
            uri.queryParam(name, v.getKey());
        }
    }

    private static <T extends LocalDate> void addOptionalDate(UriBuilder uri, String name, Option<T> value)
    {
        for (T v: value)
        {
            uri.queryParam(name, v.toString());
        }
    }
    
    public static void addAddonQueryParams(AddonQuery query, UriBuilder uri)
    {
        addAccessTokenParams(query, uri);
        addApplicationCriteriaParams(query, uri);
        if (!isEmpty(query.getCategoryNames()))
        {
            uri.queryParam("category", toArray(query.getCategoryNames(), Object.class));
        }
        addOptionalEnumKey(uri, "filter", query.getView());
        addCostParam(query, uri);
        addOptionalBoolean(uri, "forThisUser", query.isForThisUserOnly());
        addHostingParam(query, uri);
        addOptionalEnumKey(uri, "includeHidden", query.getIncludeHidden());
        addOptionalBoolean(uri, "includePrivate", query.isIncludePrivate());
        for (String label: query.getLabel())
        {
            uri.queryParam("marketingLabel", label);
        }
        for (String searchText: query.getSearchText())
        {
            uri.queryParam("text", searchText);
        }
        addOptionalEnumKey(uri, "treatPartlyFreeAs", query.getTreatPartlyFreeAs());
        addWithVersionParam(query, uri);
        addBoundsParams(query, uri);
    }
    
    public static void addAddonVersionsQueryParams(AddonVersionsQuery query, UriBuilder uri)
    {
        addAccessTokenParams(query, uri);
        addApplicationCriteriaParams(query, uri);
        addCostParam(query, uri);
        addHostingParam(query, uri);
        for (String v: query.getAfterVersionName())
        {
            uri.queryParam("afterVersion", v);
        }
        addBoundsParams(query, uri);
        addIncludePrivateParam(query, uri);
    }
    
    public static void addProductQueryParams(ProductQuery query, UriBuilder uri)
    {
        addApplicationCriteriaParams(query, uri);
        addCostParam(query, uri);
        addHostingParam(query, uri);
        addWithVersionParam(query, uri);
        addBoundsParams(query, uri);
    }

    public static void addAccessTokenParams(QueryProperties.AccessToken q, UriBuilder uriBuilder)
    {
        for (String a: q.getAccessToken())
        {
            uriBuilder.queryParam("accessToken", a);
        }
    }
    
    public static void addApplicationCriteriaParams(QueryProperties.ApplicationCriteria q, UriBuilder uriBuilder)
    {
        for (ApplicationKey a: q.getApplication())
        {
            uriBuilder.queryParam("application", a.getKey());
            for (Integer b: q.getAppBuildNumber())
            {
                uriBuilder.queryParam("applicationBuild", b);  // note that this is different from the v1 parameter name
            }
        }
    }

    public static void addBoundsParams(QueryBounds b, UriBuilder uriBuilder)
    {
        if (b.getOffset() > 0)
        {
            uriBuilder.queryParam("offset", b.getOffset());
        }
        for (Integer l: b.getLimit())
        {
            uriBuilder.queryParam("limit", l);
        }
    }
    
    public static void addBoundsParams(QueryProperties.Bounds q, UriBuilder uriBuilder)
    {
        addBoundsParams(q.getBounds(), uriBuilder);
    }

    public static void addCostParam(QueryProperties.Cost q, UriBuilder uriBuilder)
    {
        addOptionalEnumKey(uriBuilder, "cost", q.getCost());
    }
    
    public static void addHostingParam(QueryProperties.Hosting q, UriBuilder uriBuilder)
    {
        addOptionalEnumKey(uriBuilder, "hosting", q.getHosting());
    }

    public static void addIncludePrivateParam(QueryProperties.IncludePrivate q, UriBuilder uriBuilder)
    {
        addOptionalBoolean(uriBuilder, "includePrivate", q.isIncludePrivate());
    }

    public static void addWithVersionParam(QueryProperties.WithVersion q, UriBuilder uriBuilder)
    {
        addOptionalBoolean(uriBuilder, "withVersion", q.isWithVersion());
    }

    public static void addVendorQueryParams(VendorQuery q, UriBuilder uriBuilder)
    {
        addBoundsParams(q, uriBuilder);
        addOptionalBoolean(uriBuilder, "forThisUser", q.isForThisUserOnly());
    }

    public static void addLicenseQueryParams(LicenseQuery q, UriBuilder uriBuilder)
    {
        addBoundsParams(q, uriBuilder);
        uriBuilder.queryParam("addon", toArray(q.getAddon(), String.class));
        addOptionalDate(uriBuilder, "startDate", q.getStartDate());
        addOptionalDate(uriBuilder, "endDate", q.getEndDate());
        addOptionalString(uriBuilder, "text", q.getText());
        uriBuilder.queryParam("tier", toArray(q.getTier(), Integer.class));
        addOptionalString(uriBuilder, "dateType", q.getDateType());
        uriBuilder.queryParam("licenseType", toArray(q.getLicenseType(), String.class));
        uriBuilder.queryParam("partnerTypes", toArray(transform(q.getPartnerTypes(), EnumWithKey.toString), String.class));
        uriBuilder.queryParam("hostingType", toArray(transform(q.getHosting(), EnumWithKey.toString), String.class));
        uriBuilder.queryParam("status", toArray(transform(q.getStatus(), EnumWithKey.toString), String.class));
        addOptionalDate(uriBuilder, "lastUpdated", q.getLastUpdated());
        addOptionalString(uriBuilder, "sortBy", q.getSortBy());
        addOptionalString(uriBuilder, "order", q.getOrder());
    }

    public static URI withAccessToken(URI u, String token)
    {
        return UriBuilder.fromUri(u).queryParam("accessToken", token).build();
    }
    
    public static URI withZeroLimit(URI u)
    {
        return UriBuilder.fromUri(u).queryParam("limit", 0).build();
    }
    
    public <T> Page<T> getMore(PageReference<T> ref) throws MpacException
    {
        SimpleHttpResponse response = null;
        try
        {
            response = httpHelper.get(resolveLink(ref.getUri()));
            if (errorOrEmpty(response.getStatus()))
            {
                throw responseException(response);
            }
            return ref.getReader().readPage(ref, response.getContentStream());
        }
        finally
        {
            closeQuietly(response);
        }
    }
    
    public static UriTemplate requireLinkUriTemplate(Links links, String rel, Class<?> entityClass) throws MpacException
    {
        for (UriTemplate ut: links.getUriTemplate(rel))
        {
            return ut;
        }
        throw new MpacException("Missing required API link \"" + rel + "\" from " + entityClass.getSimpleName());
    }

    public static <A extends Entity> URI getTemplatedLink(A a, String rel, String paramName, String paramValue) throws MpacException
    {
        UriTemplate t = requireLinkUriTemplate(a.getLinks(), rel, a.getClass());
        return t.resolve(ImmutableMap.of(paramName, paramValue));
    }
}
