package com.atlassian.marketplace.client.impl;

import java.io.InputStream;
import java.io.OutputStream;

import com.atlassian.marketplace.client.MpacException;

/**
 * Interface for an object that marshals and unmarshals request/response entities.
 * @since 2.0.0
 */
public interface EntityEncoding
{
    <T> T decode(InputStream stream, Class<T> type) throws MpacException;
    
    <T> void encode(OutputStream stream, T entity, boolean includeReadOnlyFields) throws MpacException;
    
    <T> void encodeChanges(OutputStream stream, T original, T updated) throws MpacException;
}
