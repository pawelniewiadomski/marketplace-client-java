package com.atlassian.marketplace.client.impl;

import java.net.URI;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.ApplicationVersionSpecifier;
import com.atlassian.marketplace.client.api.ApplicationVersionsQuery;
import com.atlassian.marketplace.client.api.Applications;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.ProductQuery;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.api.UriTemplate;
import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.atlassian.marketplace.client.util.UriBuilder;

import com.google.common.collect.ImmutableMap;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.marketplace.client.impl.ApiHelper.getTemplatedLink;

final class ApplicationsImpl extends ApiImplBase implements Applications
{
    ApplicationsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "applications");
    }

    @Override
    public Option<Application> getByKey(ApplicationKey applicationKey) throws MpacException
    {
        InternalModel.Applications collectionRep = getEmptyBaseCollectionRep();
        UriTemplate byKeyTemplate = ApiHelper.requireLinkUriTemplate(collectionRep._links, "byKey", InternalModel.Applications.class);
        UriBuilder uri = UriBuilder.fromUri(byKeyTemplate.resolve(ImmutableMap.of("applicationKey", applicationKey.getKey())));
        return apiHelper.getOptionalEntity(uri.build(), Application.class);
    }
    
    @Override
    public Option<ApplicationVersion> getVersion(ApplicationKey applicationKey, ApplicationVersionSpecifier versionQuery) throws MpacException
    {
        for (Application a: getByKey(applicationKey))
        {
            URI uri = getVersionUri(a, versionQuery);
            return apiHelper.getOptionalEntity(uri, ApplicationVersion.class);
        }
        return none();
    }

    @Override
    public Page<ApplicationVersion> getVersions(ApplicationKey applicationKey, ApplicationVersionsQuery versionsQuery) throws MpacException
    {
        for (Application a: getByKey(applicationKey))
        {
            UriTemplate ut = getVersionsUriTemplate(a);
            ImmutableMap.Builder<String, String> params = ImmutableMap.builder();
            for (int b: versionsQuery.getAfterBuildNumber())
            {
                params.put("afterBuildNumber", String.valueOf(b));
            }
            UriBuilder ub = UriBuilder.fromUri(ut.resolve(params.build()));
            ApiHelper.addBoundsParams(versionsQuery, ub);
            return apiHelper.getMore(new PageReference<ApplicationVersion>(
                ub.build(), versionsQuery.getBounds(), pageReader(InternalModel.ApplicationVersions.class)));
        }
        return Page.empty();
    }
   
    @Override
    public ApplicationVersion createVersion(ApplicationKey applicationKey, ApplicationVersion version) throws MpacException
    {
        for (Application a: getByKey(applicationKey))
        {
            return genericCreate(getVersionsUriTemplate(a).resolve(ImmutableMap.<String, String>of()), version);
        }
        throw new MpacException.ServerError(404);
    }

    @Override
    public ApplicationVersion updateVersion(ApplicationVersion original, ApplicationVersion updated) throws MpacException
    {
        return genericUpdate(original.getSelfUri(), original, updated);
    }
    
    private UriTemplate getVersionsUriTemplate(Application a) throws MpacException
    {
        return ApiHelper.requireLinkUriTemplate(a.getLinks(), "versions", Application.class);
    }
    
    private InternalModel.Applications getEmptyBaseCollectionRep() throws MpacException
    {
        UriBuilder uri = fromApiRoot();
        ApiHelper.addBoundsParams(ProductQuery.builder().bounds(QueryBounds.empty()).build(), uri);
        return apiHelper.getEntity(uri.build(), InternalModel.Applications.class);
    }

    private URI getVersionUri(Application a, ApplicationVersionSpecifier v) throws MpacException
    {
        for (Either<String, Integer> specifiedVersion: v.getSpecifiedVersion())
        {
            for (String name: specifiedVersion.left())
            {
                return getTemplatedLink(a, "versionByName", "name", name);
            }
            for (Integer build: specifiedVersion.right())
            {
                return getTemplatedLink(a, "versionByBuild", "applicationBuildNumber", String.valueOf(build));
            }
        }
        return apiHelper.requireLinkUri(a.getLinks(), "latestVersion", Application.class);
    }
}
