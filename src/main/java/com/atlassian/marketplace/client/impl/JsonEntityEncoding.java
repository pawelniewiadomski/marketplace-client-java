package com.atlassian.marketplace.client.impl;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.atlassian.marketplace.client.MpacException;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import static com.atlassian.fugue.Iterables.flatMap;
import static com.google.common.collect.Iterables.concat;

/**
 * Gson-based implementation of {@link EntityEncoding}.
 * @since 2.0.0
 */
public class JsonEntityEncoding implements EntityEncoding
{
    private final Gson gsonWithReadOnlyFields = makeGson(true);
    private final Gson gsonWithoutReadOnlyFields = makeGson(false);

    private Gson makeGson(boolean includeReadOnlyFields)
    {
        GsonBuilder builder = new GsonBuilder()
            .disableHtmlEscaping();  // we don't want it to transform '<' to an HTML entity within strings
        for (Map.Entry<Class<?>, Object> e: TypeAdapters.all().entrySet())
        {
            builder.registerTypeAdapter(e.getKey(), e.getValue());
        }
        return builder.registerTypeAdapterFactory(new BaseTypeAdapterFactory(includeReadOnlyFields)).create();    
    }
    
    @Override
    public <T> T decode(InputStream stream, Class<T> type) throws MpacException
    {
        try
        {
            return gsonWithReadOnlyFields.fromJson(new InputStreamReader(stream), type);
        }
        catch (JsonParseException e)
        {
            throw toMpacException(e);
        }
    }

    private static MpacException toMpacException(Throwable e)
    {
        while (e.getCause() != null)
        {
            e = e.getCause();
        }
        if (e instanceof MpacException)
        {
            return (MpacException) e;
        }
        if (e instanceof SchemaViolationException)
        {
            return new MpacException.InvalidResponseError(((SchemaViolationException) e).getSchemaViolations());
        }
        return new MpacException.InvalidResponseError(e.getMessage(), e);
    }
    
    @Override
    public <T> void encode(OutputStream stream, T entity, boolean includeReadOnlyFields) throws MpacException
    {
        Gson gson = includeReadOnlyFields ? gsonWithReadOnlyFields : gsonWithoutReadOnlyFields;
        OutputStreamWriter w = new OutputStreamWriter(stream);
        try
        {
            gson.toJson(entity, w);    
        }
        catch (JsonIOException e)
        {
            throw new MpacException(e);
        }
        finally
        {
            IOUtils.closeQuietly(w);
        }
    }
    
    @Override
    public <T> void encodeChanges(OutputStream stream, T original, T updated) throws MpacException
    {
        JsonObject jOrig = gsonWithoutReadOnlyFields.toJsonTree(original).getAsJsonObject();
        JsonObject jUpdated = gsonWithoutReadOnlyFields.toJsonTree(updated).getAsJsonObject();
        JsonArray jResult = new JsonArray();
        for (JsonElement n: makeJsonPatch(jOrig, jUpdated, ""))
        {
            jResult.add(n);
        }
        OutputStreamWriter w = new OutputStreamWriter(stream);
        try
        {
            gsonWithoutReadOnlyFields.toJson(jResult, w);
        }
        catch (JsonIOException e)
        {
            throw new MpacException(e);
        }
        finally
        {
            IOUtils.closeQuietly(w);
        }
    }
    
    /**
     * Creates a JSON document in the JSON Patch format to describe differences between two JSON documents. 
     * @param jOrig  the original JSON document
     * @param jUpdated  a document that is based on {@code jOrig} but may have had some properties added/changed/removed
     * @param basePath  a JSON Pointer string such as "/_links" describing the path to the enclosing object if any,
     * or an empty string if {@code jOrig} is the root document
     * @return  a list of valid JSON Patch elements - empty if there were no changes
     */
    private Iterable<JsonElement> makeJsonPatch(final JsonObject jOrig, final JsonObject jUpdated, final String basePath)
    {
        Iterable<JsonElement> addsAndReplaces = flatMap(jUpdated.entrySet(),
            new Function<Map.Entry<String, JsonElement>, Iterable<JsonElement>>()
            {
                public Iterable<JsonElement> apply(Map.Entry<String, JsonElement> e)
                {
                    String name = e.getKey();
                    JsonElement n1 = e.getValue();
                    if (!n1.isJsonNull())
                    {
                        String subPath = basePath + "/" + name;
                        JsonElement n0 = jOrig.get(name);
                        if (n0 != null && !n0.isJsonNull())
                        {
                            if (n0.isJsonObject() && n1.isJsonObject())
                            {
                                return makeJsonPatch(n0.getAsJsonObject(), n1.getAsJsonObject(), subPath);
                            }
                            else if (!n0.equals(n1))
                            {
                                JsonObject op = new JsonObject();
                                op.addProperty("op", "replace");
                                op.addProperty("path", subPath);
                                op.add("value", n1);
                                return ImmutableList.<JsonElement>of(op);
                            }
                            else
                            {
                                return ImmutableList.of();
                            }
                        }
                        else
                        {
                            JsonObject op = new JsonObject();
                            op.addProperty("op", "add");
                            op.addProperty("path", subPath);
                            op.add("value", n1);
                            return ImmutableList.<JsonElement>of(op);
                        }
                    }
                    else
                    {
                        return ImmutableList.of();
                    }
                }
            });
        Iterable<JsonElement> removes = flatMap(ImmutableList.copyOf(jOrig.entrySet()),
            new Function<Map.Entry<String, JsonElement>, Iterable<JsonElement>>()
            {
                public Iterable<JsonElement> apply(Map.Entry<String, JsonElement> e)
                {
                    String name = e.getKey();
                    JsonElement n0 = e.getValue();
                    if (!n0.isJsonNull())
                    {
                        JsonElement n1 = jUpdated.get(name);
                        if (n1 == null || n1.isJsonNull())
                        {
                            JsonObject op = new JsonObject();
                            op.addProperty("op", "remove");
                            op.addProperty("path", basePath + "/" + name);
                            return ImmutableList.<JsonElement>of(op);
                        }
                    }
                    return ImmutableList.of();
                }
            });
        return concat(addsAndReplaces, removes);
    }

    // We need a TypeAdapterFactory, in addition to the TypeAdapters we previously registered,
    // because we need to be able to override the default reflective serialization for generic
    // objects with our ObjectTypeAdapter, and we also have special handling for enums.
    static class BaseTypeAdapterFactory implements TypeAdapterFactory
    {
        private static final Set<Class<?>> TYPES_WITH_DEFAULT_SERIALIZATION = ImmutableSet.<Class<?>>of(
            String.class,
            Boolean.class,
            Integer.class,
            Long.class,
            Float.class,
            Double.class
        );
     
        private final boolean includeReadOnlyFields;
        
        BaseTypeAdapterFactory(boolean includeReadOnlyFields)
        {
            this.includeReadOnlyFields = includeReadOnlyFields;
        }
        
        @SuppressWarnings({ "unchecked" })
        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken)
        {
            Class<?> rawType = typeToken.getRawType();
            if (rawType.isPrimitive() || TYPES_WITH_DEFAULT_SERIALIZATION.contains(rawType) ||
                Map.class.isAssignableFrom(rawType) || Collection.class.isAssignableFrom(rawType) ||
                TypeAdapters.all().keySet().contains(rawType))
            {
                // Returning null here just means Gson will use one of the TypeAdapters we previously
                // registered if appropriate, or else its default serialization, rather than
                // EnumTypeAdapter or ObjectTypeAdapter.
                return null;
            }
            if (rawType.isEnum())
            {
                return (TypeAdapter<T>) TypeAdapters.enumTypeAdapter(rawType);
            }
            return TypeAdapters.objectTypeAdapter(gson, typeToken, includeReadOnlyFields);
        }   
    }
}
