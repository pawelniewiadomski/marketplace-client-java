package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.encoding.SchemaViolation;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

/**
 * Used internally to wrap {@link SchemaViolation}s in an unchecked exception.
 * 
 * @since 2.0.0
 */
@SuppressWarnings("serial")
public class SchemaViolationException extends RuntimeException
{
    private final ImmutableList<SchemaViolation> schemaViolations;

    public SchemaViolationException(Iterable<SchemaViolation> schemaViolations)
    {
        this.schemaViolations = ImmutableList.copyOf(schemaViolations);
    }
    
    public SchemaViolationException(SchemaViolation schemaViolation)
    {
        this(ImmutableList.of(schemaViolation));
    }

    public Iterable<SchemaViolation> getSchemaViolations()
    {
        return schemaViolations;
    }
    
    @Override
    public String getMessage()
    {
        return Joiner.on(", ").join(schemaViolations);
    }
}
