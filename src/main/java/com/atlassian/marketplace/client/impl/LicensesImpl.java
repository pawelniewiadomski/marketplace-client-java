package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.*;
import com.atlassian.marketplace.client.model.License;
import com.atlassian.marketplace.client.model.VendorBase;
import com.atlassian.marketplace.client.util.UriBuilder;

final class LicensesImpl extends ApiImplBase implements Licenses
{
    LicensesImpl(ApiHelper apiHelper, VendorBase vendor) throws MpacException
    {
        super(apiHelper, vendor.getId().getUri());
    }

    @Override
    public Page<License> find(LicenseQuery query) throws MpacException
    {
        UriBuilder uri = fromApiRoot().path("reporting/licenses");

        ApiHelper.addLicenseQueryParams(query, uri);
        return apiHelper.getMore(new PageReference<>(
                uri.build(), query.getBounds(), pageReader(InternalModel.Licenses.class)));
    }
}
