/**
 * Internal implementation of Marketplace client APIs.
 * Rather than referring directly to this package, use {@link com.atlassian.marketplace.client.MarketplaceClientFactory}
 * to create an implementation of the client.
 */
package com.atlassian.marketplace.client.impl;
